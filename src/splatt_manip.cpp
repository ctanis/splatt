#include <splatter.h>
#include <queue>
#include <utility>

using namespace splatter;

class N2NBuilder
{
public:
    N2NBuilder(const ownerdb& odb, std::vector<intset>& hash,
               const bool local_only=true) :
        _hash(hash), _odb(odb), _local_only(local_only)
    {
        _hash.resize(_local_only ? _odb.nlocal() : _odb.nlocal_p());
    }
    

    void operator()(splatter::index* idx, int item, int etype,
                    int width, int* nodes) const
    {
        for (int n1=0; n1<width; n1++)
        {
            if (!_local_only || _odb.owns(nodes[n1]))
            {
                int l = _odb.g2l_p(nodes[n1]);

                for (int n2=0; n2<width; n2++)
                {
                    if (n1 != n2)
                    {
                        _hash[l].insert(nodes[n2]);
                    }
                }
            }
        }
    }
    



private:
    std::vector<intset> &_hash;
    const ownerdb& _odb;
    const bool _local_only;
};



// got RCM algorithm from here: 
// http://ciprian-zavoianu.blogspot.com/2009/01/project-bandwidth-reduction.html


// reverse cuthill-mckee
status part_mgr::cuthill_mckee()
{
    std::vector<intset> hash;

    // new global id for locally owned nodes
    std::vector<int> order;

    typedef std::pair<int,int> pair;
    typedef std::priority_queue< pair, std::vector<pair>, std::greater<pair> > pqueue;

    std::queue<int> working;
    unsigned int processed=0;

    if (_odb.nlocal() == 0)     // no nodes here to sort
    {
        return SPLATT_OK;
    }

    N2NBuilder n2n(_odb, hash);
    order.resize(_odb.nlocal(), -1);

    // generate node-to-node adjacency for entire local mesh
    for (unsigned int i=0; i < _indices.size(); i++)
    {
        LLOG(0, "integrating adjacency of index " << _indices[i]->name() );
        _indices[i]->apply(n2n);
    }

    int iter=0;
    while (processed < hash.size())
    {
        LLOG(0, "RCM cluster " << iter);
        
        // find a good starting node
        int start=0;
        unsigned int deg=hash.size();
    
        for (unsigned int n=start; n<hash.size(); n++)
        {
            // unvisited node
            if (order[n] == -1)
            {
                if (hash[n].size() < deg)
                {
                    start = n;
                    deg = hash[n].size();
                }
            }
        }

        working.push( start );
    
        while (! working.empty() )
        {
            int next = working.front();
            // DEBUG("in working loop: " << next << " | " << hash[next].size() );
            working.pop();

            if (order[next] == -1)
            {
                // reverse ordering
                order[next]=_odb.nlocal()-(processed+1) + _odb.low();
                processed++;

                pqueue nbrs;

                for (unsigned int n=0; n<hash[next].size(); n++)
                {
                    int nbr = hash[next][n];

                    if (_odb.owns(nbr))
                    {
                        int l = _odb.g2l(nbr);

                        nbrs.push( { hash[l].size(), l } );
                    }
                }
             
                while (! nbrs.empty() )
                {
                    working.push( nbrs.top().second );
                    nbrs.pop();
                }
            }
        }

        LLOG(0, "RCM iter " << iter << " -- sizes: "
             << processed <<  " / " << hash.size());
        iter++;

    }

    LLOG(0, "cuthill sizes: " << processed <<  " / " << hash.size());
    assert(processed == hash.size() );

    if (processed != hash.size() )
    {
        ERROR("oops, RCM failed: " << processed << " | " << hash.size());
        return SPLATT_FAIL;
    }

    // renumber nodes
    renumber(order);
    LLOG(0, "finished renumbering for RCM");


    return SPLATT_OK;
}
