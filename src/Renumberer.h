//  -*- Mode: C++; -*- 

// a helper class for renumbering all faces in an exp_index


class Renumberer
{
public:
    Renumberer(const ownerdb& old_odb,
	       const ownerdb& new_odb,
	       const std::vector<int>& newgids,
	       migrate_args& m_args,
	       std::vector<std::set<int> >& remote_needed,
	       std::deque<int>& revisit,
	       std::map<int,int>& global_map,
	       bool live_hash,
	       fast_index& fi,
               bool purge,	// delete old ones
	       bool deliver
	)
	: old_odb(old_odb),
	  new_odb(new_odb),
	  args(m_args),
	  remote_needed(remote_needed),
	  revisit(revisit),
	  global_map(global_map),
	  _live_hash(live_hash),
	  _hash(fi),
	newgids(newgids),
	purge(purge),
        deliver(deliver)
    {
    }

    inline void operator()(splatter::index* idx,
			   int item,
			   int etype,
			   int width,
			   int* nodes) const
    {
	int needed[SPLATT_MAX_NODES];
	int numneeded=0;
	
	int smallest = nodes[0];

	for (int n=0; n<width; n++)
	{
	    assert(nodes[n] != -1);
	    

	    // figure out smallest node in this face
	    if (nodes[n] < smallest)
	    {
		smallest = nodes[n];
	    }

	    // figure out if i need to request some global ids --
	    // TODO: rewrite this to save map lookups
	    if (! old_odb.owns(nodes[n]) && global_map.find(nodes[n])==global_map.end())
	    {
		needed[numneeded++]=nodes[n];
	    }
	}


	if (old_odb.owns(smallest) || !purge)
	{
	    // i am the old owner (or i need to keep everything), and thus
	    // responsible for not losing this data

	    if (numneeded > 0)
	    {
		// there are some faces TBD before I can delete this
		for (int n=0; n<numneeded; n++)
		{
		    remote_needed[old_odb.owner(needed[n])].insert(needed[n]);
		}

		revisit.push_back(item);
	    }
	    else
	    {
		// i am the old owner and know all new ids
		if (_live_hash)
		{
		    for (int n=0; n<width; n++)
		    {
			_hash[nodes[n]].remove(item);
		    }
		}
		

		std::set<int> owners;
		for (int n=0; n<width; n++)
		{
		    if (old_odb.owns(nodes[n]))
		    {
			nodes[n] = newgids[nodes[n]-old_odb.low()];
		    }
		    else
		    {
#ifdef SPLATT_DEBUG
			nodes[n] = global_map.at(nodes[n]);
#else
			nodes[n] = global_map[nodes[n]];
#endif // SPLATT_DEBUG
		    }

#ifdef SPLATT_DEBUG
		    
		    int newowner = new_odb.owner(nodes[n]);

		    if (newowner == -1)
		    {
			DEBUG("whaaa? could not find owner of " << nodes[n]);
		    }
#endif // SPLATT_DEBUG

		    owners.insert(new_odb.owner(nodes[n]));
		}

		if (_live_hash)
		{
		    for (int n=0; n<width; n++)
		    {
			_hash[nodes[n]].insert(item);
		    }
		}
		



		{
		    bool keepit=false;
		    for (std::set<int>::iterator c = owners.begin();
			 c != owners.end();
			 c++)
		    {
			if (*c != new_odb.me() && deliver)
			{
			    // these guys need a copy...
			    args.to_move[*c].push_back(item);
			}
			else
			{
			    keepit= true;
			}
		    }

		    if (!keepit && purge)
		    {
			args.to_delete.push_back(item);
		    }
		}
	    }
	}
	else
	{
	    // delete anything i didn't originally own, assuming that
	    // remote owner will send me what i need
            args.to_delete.push_back(item);
	    
	}	

    }


    // this will be called only on faces that we needed more info to
    // process (new global ids from remote ranks)
    inline void do_revisit(splatter::index* idx,
			   int eltno,
			   int etype,
			   int nnodes,
			   int* nodes) const
    {
	std::set<int> owners;

	if (_live_hash)
	{
	    for (int n=0; n<nnodes; n++)
	    {
		_hash[nodes[n]].remove(eltno);
	    }
	}
        
	for (int n=0; n<nnodes; n++)
	{

	    if (old_odb.owns(nodes[n]))
	    {
		nodes[n] = newgids[old_odb.g2l(nodes[n])];
	    }
	    else
	    {
#ifdef SPLATT_DEBUG
		nodes[n] = global_map.at(nodes[n]);
#else
		nodes[n] = global_map[nodes[n]];
#endif // SPLATT_DEBUG


	    }

	    owners.insert(new_odb.owner(nodes[n]));
	}
        

	if (_live_hash)
	{
	    for (int n=0; n<nnodes; n++)
	    {
		_hash[nodes[n]].insert(eltno);
	    }
	}



	{
	    bool keepit=false;

	    for (std::set<int>::iterator c = owners.begin();
		 c != owners.end();
		 c++)
	    {
		if (*c != new_odb.me() && deliver)
		{
		    // these guys need a copy...
		    args.to_move[*c].push_back(eltno);
		}
		else
		{
		    keepit=true;
		}
	    }

	    if (!keepit && purge)
	    {
		args.to_delete.push_back(eltno);
	    }
	}
    }



    const ownerdb&	old_odb;
    const ownerdb&	new_odb;
    migrate_args&	args;

    std::vector< std::set<int> >& remote_needed;
    std::deque<int>& revisit;	// faces to renumber after getting remote ids
    std::map<int,int>& global_map;

    const bool _live_hash;
    fast_index& _hash;
   

private:

    const std::vector<int>& newgids;
    const bool purge;
    const bool deliver;
};
