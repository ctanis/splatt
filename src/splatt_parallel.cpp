//  -*- Mode: C++; -*- 
#include <splatter.h>
#include <list>
#include <string.h>
using namespace splatter;


parallel_ctx::parallel_ctx() :  _root(0)
{
}

parallel_ctx::parallel_ctx(MPI_Comm comm) :  _root(0)
{
    init(comm);
}

parallel_ctx::parallel_ctx(const parallel_ctx& other)
{
    *this = other;
}

void parallel_ctx::init(MPI_Comm comm)
{
    set_communicator(comm);
}


void parallel_ctx::barrier() const
{
    MPI_Barrier(_comm);
}



parallel_ctx& parallel_ctx::operator=(const parallel_ctx& other)
{
    if (&other == this)
    {
	return *this;
    }

    _np = other._np;
    _rank = other._rank;
//    _max_threads = other._max_threads;
    _comm = other._comm;
    _root = other._root;
    strncpy(_macname, other._macname, MPI_MAX_PROCESSOR_NAME);
    return *this;
}



void parallel_ctx::set_communicator(MPI_Comm comm)
{
    int namelen = MPI_MAX_PROCESSOR_NAME;
    _comm = comm;
    MPI_Comm_size(_comm, &_np);
    MPI_Comm_rank(_comm, &_rank);
    MPI_Get_processor_name(_macname, &namelen);
}


// void parallel_ctx::set_max_threads(int mt)
// {
// #ifdef SPLATT_FOMP
//     int max = omp_get_max_threads();
// #else
//     int max = 1;
// #endif

//     // if we supply an invalid maxthreads, use the system max
//     if ((mt < 1) || (mt > max))
//     {
// 	_max_threads = max;
//     }
//     else
//     {
// 	_max_threads = mt;
//     }
// }


void parallel_ctx::rebind_stdio(int ignore_rank, std::string file_prefix) const
{
//    if ((_rank != ignore_rank) && (_np > 1))
    if (_rank != ignore_rank)
    {
	std::string fname=file_prefix+"stderr." + stringify(_rank);
	freopen(fname.c_str(), "w", stderr);
	fname=file_prefix+"stdout." + stringify(_rank);
	freopen(fname.c_str(), "w", stdout);
	std::cout << "<<rebind_stdio>> -*- mode: splatt-log; -*-" << std::endl;
    }
}


static std::list<MPI_Datatype> created_types;


MPI_Datatype& splatter::new_mpi_type()
{
    created_types.push_back(MPI_Datatype());
    return created_types.back();
}


void splatter::clear_mpi_types()
{
    std::list<MPI_Datatype>::iterator it;
    
    for (it=created_types.begin(); it != created_types.end(); it++)
    {
        MPI_Type_free(&(*it));
    }

    created_types.clear();
}
