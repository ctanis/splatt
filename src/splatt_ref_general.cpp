#include <splatter.h>
#include <splatt_query.h>
using namespace splatter;


// indices is a list of explicit_index* contaning entities that have a say in
// whether a refinement is valid -- if you are refining tets, you don't need
// to include the boundary triangles

status refine_propogate_edges( part_mgr& mgr,
                               std::list<explicit_index*>& indices,
                               temp_index& edgemap )
{
    bool propogating=true;

    while ( mgr.pctx().reduce((int) propogating, MPI_LOR) )
    {
        temp_index new_edges(&mgr, topo::EDGE, false);
        deliver_args new_edge_delivery(mgr.pctx());

        for ( std::list<explicit_index*>::iterator idx_it = indices.begin();
              idx_it != indices.end();
              idx_it++ )
        {
            using namespace splatter::query;

            mgr.query( for_each(edgemap) >>=
                       faces_in(*idx_it) >>=

                       // where(not(ref_status))...

                       unique() >>=
                       store(all_needers(true), "needers") >>=

                       // ref_validator(*idx_it) >=

                       save_temp_uniq(new_edges) >>=

                       sort(fetch("needers"), face_id(), new_edge_delivery)
                );
        }

        new_edge_delivery.prepare();
        temp_index new_remote_edges(&mgr, topo::EDGE, false);

        if (new_edges.deliver(new_edge_delivery, new_remote_edges) != SPLATT_OK)
        {
            ERROR("delivery of new_edges failed");
            return SPLATT_FAIL;
        }

        int original = edgemap.size();
        edgemap.add_all(new_edges, true);
        edgemap.add_all(new_remote_edges, true);

        if (edgemap.size() != original)
        {
            propogating = true;
        }
        else
        {
            propogating = false;
        }

    }



    return SPLATT_OK;
}




status mark_edges(temp_index& edgemap)
{
    


    return SPLATT_OK;
}
