//  -*- Mode: C++; -*-
#include <splatter.h>
using namespace splatter;

const int num_entities=5;

const entity_cfg topo::std_entities[]=
{
/* NODE */
    { "node", 0, 1, 0},
/* EDGE */
    { "edge", 1, 2, 2,
      {
	  { topo::NODE, { 0 } },
	  { topo::NODE, { 1 } }
      }
    },
/* FACE_TRI */
    { "tri", 2, 3, 3,
      {
	  { topo::EDGE, { 0, 1 } },
	  { topo::EDGE, { 1, 2 } },
	  { topo::EDGE, { 2, 0 } }
      }
    },
/* FACE_QUAD */
    { "quad", 2, 4, 4,
      {
	  { topo::EDGE, { 0, 1 } },
	  { topo::EDGE, { 1, 2 } },
	  { topo::EDGE, { 2, 3 } },
	  { topo::EDGE, { 3, 0 } }
      }
    },
/* CELL_TET */
    { "tet", 3, 4, 4,
      {
	  // { topo::TRI, { 0, 1, 2 } },
	  // { topo::TRI, { 1, 3, 2 } },
	  // { topo::TRI, { 0, 3, 1 } },
	  // { topo::TRI, { 0, 2, 3 } }
	  { topo::TRI, { 0, 2, 1 } },
	  { topo::TRI, { 1, 2, 3 } },
	  { topo::TRI, { 0, 1, 3 } },
	  { topo::TRI, { 0, 3, 2 } }
      }
    },
/* CELL_PYR */
    { "pyramid", 3, 5, 5,
      {
	  { topo::QUAD, { 0, 3, 2, 1 } },
	  { topo::TRI, { 0, 1, 4 } },
	  { topo::TRI, { 1, 2, 4 } },
	  { topo::TRI, { 2, 3, 4 } },
	  { topo::TRI, { 3, 0, 4 } }
      }
    },    

    SPLATT_ENTITY_END
};



// status etype_graph::add_type(int id, const entity_cfg& e)
// {
//     if (_type_map.find(id) != _type_map.end())
//     {
// 	ERROR("duplicate id in etype_graph");
// 	return SPLATT_FAIL;
//     }


//     node* newnode = new node();
//     newnode->cfg = e;
//     _type_map[id]=newnode;


//     // make inter-node connections based on etype subfaces

//     for (int sf=0; sf<e.numsubfaces; sf++)
//     {
// 	int subtype = e.subfaces[sf].subface_type;
// 	newnode->down[subtype]=NULL;
//     }

//     return SPLATT_OK;
// }



// status etype_graph::finalize()
// {
//     //
//     std::map<int,node*>::iterator curs, inner;

//     for (curs=_type_map.begin(); curs != _type_map.end(); curs++)
//     {
// 	node* n = curs->second;

// 	for (inner = n->down.begin(); inner != n->down.end(); inner++)
// 	{
// 	    int subtype = inner->first;

// 	    node* subnode = _type_map[subtype];

// 	    if (subnode != NULL)
// 	    {
// 		inner->second = subnode;
// //		DEBUG("parent type of " << subtype << "is" << curs->first);
// 		subnode->up[curs->first]=n;
// 	    }
// 	    else
// 	    {
// 		ERROR("missing id in entity_cfg: " << subtype);
// 		return SPLATT_FAIL;
// 	    }
// 	}
//     }

//     return SPLATT_OK;
// }

		
	    
// const entity_cfg& etype_graph::operator[](int type) const
// {
//     std::map<int,node*>::const_iterator match = _type_map.find(type);

//     if (match == _type_map.end())
//     {
// 	ERROR("unknown etype: "<<type);
//     }
    
//     return (match->second->cfg);
// }
