//  -*- mode: c++; -*-

// tet & triangle refinement details ...


#include <splatter.h>
#include <splatt_query.h>

using namespace splatter;


const int MAX_TET_EDGES=6;

// do these edges form a closed path...
bool edgeloop(const int* e1, const int* e2, const int* e3)
{
    if (e1[0] == e2[0])
    {
	return
	    (e3[0] == e1[1] && e3[1] == e2[1]) ||
	    (e3[1] == e1[1] && e3[0] == e2[1]);
    }
    if (e1[0] == e2[1])
    {
	return
	    (e3[0] == e1[1] && e3[1] == e2[0]) ||
	    (e3[1] == e1[1] && e3[0] == e2[0]);
	
    }
    if (e1[1] == e2[0])
    {
	return
	    (e3[0] == e1[0] && e3[1] == e2[1]) ||
	    (e3[1] == e1[0] && e3[0] == e2[1]);
    }
    if (e1[1] == e2[1])
    {
	return
	    (e3[0] == e1[0] && e3[1] == e2[0]) ||
	    (e3[1] == e1[0] && e3[0] == e2[0]);
	
    }

    return false;
}


// this class goes through every partially refined tet and extracts needed
// edges to get that tet into a supported state (for example, having only 2
// refined edges is not supported -- this operator finds the 3rd cofacial face
// and marks that for refinement -- propogating outward)
class validate_tet : public splatter::query::query_op
{
public:
    validate_tet(std::vector<int>& refstat, const temp_index& edges) :
	refstat(refstat), edges(edges)
    {}


    SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
    {
	int edge[] = { nodes[0], nodes[1],
		       nodes[0], nodes[2],
		       nodes[0], nodes[3],
		       nodes[1], nodes[2],
		       nodes[1], nodes[3],
		       nodes[2], nodes[3] };

	// 1 or 2 refined edge2, or exactly 3 co-facial edges is ok
	// --------------------------------------------------


	// edge list partitioned into known/unknown
	std::vector<int> rpart(6);
	int rcount=0; int end=5;
	for (int e=0; e< 12; e+= 2)
	{
	    if (edges.find(&edge[e]) == -1)
	    {
		// unrefined
		rpart[end--]=e;
	    }
	    else
	    {
		// refined
		rpart[rcount++]=e;
	    }
	}

	assert(rcount==end+1);
	refstat[eltno] = rcount;

	// THIS SHOULD NOT BE NECESSARY 
	// make sure everybody has these..
	for (int e=0; e<rcount; e++)
	{
	    o(NULL, -1, topo::EDGE, 2, &edge[rpart[e]]);
	}


	if (rcount == 1)
	{
	    return;
	}


	if (rcount == 2)
	{
	    /* add 3rd cofacial edge if possible */
	    int lastedge[2];

	    if (edge[rpart[0]] == edge[rpart[1]])
	    {
		lastedge[0] = edge[rpart[0]+1];
		lastedge[1] = edge[rpart[1]+1];
		o(NULL, -1, topo::EDGE,2, lastedge);
		refstat[eltno]=3;
		return;
	    }
	    else if (edge[rpart[0]] == edge[rpart[1]+1])
	    {
		lastedge[0] = edge[rpart[0]+1];
		lastedge[1] = edge[rpart[1]];
		o(NULL, -1, topo::EDGE,2, lastedge);
		refstat[eltno]=3;
		return;
	    }
	    else if (edge[rpart[0]+1] == edge[rpart[1]])
	    {
		lastedge[0] = edge[rpart[0]];
		lastedge[1] = edge[rpart[1]+1];
		o(NULL, -1, topo::EDGE,2, lastedge);
		refstat[eltno]=3;
		return;
	    }
	    else if (edge[rpart[0]+1] == edge[rpart[1]+1])
	    {
		lastedge[0] = edge[rpart[0]];
		lastedge[1] = edge[rpart[1]];
		o(NULL, -1, topo::EDGE,2, lastedge);
		refstat[eltno]=3;
		return;
	    } else
	    {
		// no cofacial edge, so we leave it at 2
//		TRACE(11, "not a cofacial edge...");
		return;
	    }
	}


	if (rcount == 3)
	{
	    /* ok if  they are cofacial */
	    if (edgeloop(&edge[rpart[0]], &edge[rpart[1]], &edge[rpart[2]]))
	    {
		return;
	    }
	    else
	    {
//		TRACE(11, "3 edges not cofacial -- refining whole thing");
	    }
	    
	    // otherwise we refine the whole tet
	}
	
	// refine the whole tet
	// TRACE(11, "need to refine the whole tet! " << edgecount);

	refstat[eltno] = MAX_TET_EDGES;

	for (unsigned int e=rcount; e<6; e++)
	{
	    o(NULL, -1, topo::EDGE, 2, &edge[rpart[e]]);
	}

	return;
    }

private:
    std::vector<int>& refstat;
    const temp_index& edges;
};


#define N(a) nodes[tet_or[rot][(a)]]
#define E(a) edge[edge_or[rot][(a)]][2]

#define NEWTET(a,b,c,d) \
    newtet[0]=(a); \
    newtet[1]=(b); \
    newtet[2]=(c); \
    newtet[3]=(d); \
    newtets[numnewtets++] = static_cast<explicit_index*>(idx)->add_face(newtet);
    


// with these orientations enumerated, we can choose the one that is
// appropriate for the incoming tet and write ONE refinement algorithm,
// assuming the edges to be modified are face-forward
    
// all possible orientations of a single tet
// static const int tet_or[][4] = 
// {
//     { 0, 1, 2, 3 },		// 0
//     { 1, 2, 0, 3 },
//     { 2, 0, 1, 3 },

//     { 0, 2, 3, 1 },
//     { 2, 3, 0, 1 },
//     { 3, 0, 2, 1 },

//     { 0, 3, 1, 2 },
//     { 3, 1, 0, 2 },
//     { 1, 0, 3, 2 },

//     { 1, 3, 2, 0 },
//     { 3, 2, 1, 0 },
//     { 2, 1, 3, 0 }
// };

// static const int edge_or[][6] =
// {
//     { 0, 1, 2, 3, 4, 5 },
//     { 1, 2, 0, 4, 5, 3 },
//     { 2, 0, 1, 5, 3, 4 },

//     { 2, 5, 3, 0, 1, 4 },
//     { 5, 3, 2, 1, 4, 0 },
//     { 3, 2, 5, 4, 0, 1 },

//     { 3, 4, 0, 2, 5, 1 },
//     { 4, 0, 3, 5, 1, 2 },
//     { 0, 3, 4, 2, 5, 1 },

//     { 4, 5, 1, 0, 3, 2 },
//     { 5, 1, 4, 3, 2, 0 },
//     { 1, 4, 5, 2, 0, 3 }
// };


// permuted to match up with edges from refine_tet
static const int tet_or[][4] = 
{
    { 0, 1, 2, 3 },		// 0
    { 1, 2, 0, 3 },
    { 2, 0, 1, 3 },

    { 3, 0, 2, 1 },
    { 3, 1, 0, 2 },
    { 2, 3, 0, 1 },

    { 1, 0, 3, 2 },
    { 2, 1, 3, 0 },
    { 0, 2, 3, 1 },

    { 0, 3, 1, 2 },
    { 1, 3, 2, 0 },
    { 3, 2, 1, 0 },

};

// corresponding edge orientations for tet orientations
static const int edge_or[][6] =
{
    { 0, 1, 2, 3, 4, 5 },
    { 1, 2, 0, 4, 5, 3 },
    { 2, 0, 1, 5, 3, 4 },

    { 3, 2, 5, 4, 0, 1 },
    { 4, 0, 3, 5, 1, 2 },
    { 5, 3, 2, 1, 4, 0 },


    { 0, 3, 4, 2, 5, 1 },
    { 1, 4, 5, 2, 0, 3 },
    { 2, 5, 3, 0, 1, 4 },
    
    { 3, 4, 0, 2, 5, 1 },
    { 4, 5, 1, 0, 3, 2 },
    { 5, 1, 4, 3, 2, 0 }

};



class refine_tet : public splatter::query::query_op
{
public:
    refine_tet(temp_index& refine_map, std::vector<int>& refstat, std::list<app_hook*>& hooks)
	: refine_map(refine_map), refstat(refstat),
	  new_node_map(refine_map.data(refine_id_tag, std::vector<int>())),
	  hooks(hooks)
    {
//	TRACE(11, "new_node_map size: " << new_node_map.size() << "|" << refine_map.size());
    }

    // look up the cnt refined edges -- returns a good orientation index
    int get(int cnt, int (&edge)[6][3]) const
    {
	for (int e=0; e<6 && cnt>0; e++)
	{
	    int c = refine_map.find(edge[e]);
	    if (c != -1)
	    {
		edge[e][2]=new_node_map[c];
		cnt--;
		matched[cnt]=e;
	    }
	}
	
	assert(cnt==0);
	return matched[0];
    }


//   tet node ordering
//
//           3
//          /\ .
//         /  \  .
//        /    \   . 2
//       /      \ .
//      +--------+
//     0          1
//
//


    SQ_TERM(idx, eltno, etype, nnodes, inodes)
    {
	int nodes[4];
	for (int n=0; n<4; n++)
	{
	    nodes[n] = inodes[n];
	}


	// in order of the tet orientations listed above

	int edge[][3] =
	    {						// old edge node
		{ nodes[0], nodes[1], -1 }, // 0	-- 1
		{ nodes[1], nodes[2], -1 }, // 3	-- 2
		{ nodes[2], nodes[0], -1 }, // 1	-- 0
		{ nodes[0], nodes[3], -1 }, // 2	-- 3
		{ nodes[1], nodes[3], -1 }, // 4	-- 4
		{ nodes[2], nodes[3], -1 }  // 5	-- 5
	    };
	
	
#ifdef SPLATT_DEBUG
	int refcount=0;


	for (int e=0; e<6; e++)
	{
	    if (refine_map.find(edge[e]) != -1)
	    {
		refcount++;
	    }
	}

	// if (refstat[eltno] != refcount)
	// {
	//     TRACE(11, eltno << " # " << refstat[eltno] << " /= " << refcount);
	// }
	
	assert(refstat[eltno] == refcount);
#endif // SPLATT_DEBUG

	int newtet[4];
	int newtets[8];
	int numnewtets=0;
	
	int rot=0;
	// TRACE(11, "check: " << eltno << "/" << refstat.size());
	// TRACE(11, "doing refinement of element " <<
	//       eltno << " -- " << refstat[eltno]);
	switch(refstat[eltno])
	{
	 case 1:			// single edge
	     rot = get(1,edge);

	     NEWTET(N(0), E(0), N(2), N(3));
	     NEWTET(E(0), N(1), N(2), N(3));

	     break;

	 case 2:			// 2 edges -- not cofacial
//	     TRACE(11, "doing non-cofacial 2 edge");
	     rot = get(2,edge);
	     // we know they're not cofacial!

	     NEWTET(N(0), E(0), E(5), N(3));
	     NEWTET(N(0), E(0), N(2), E(5));
	     NEWTET(E(0), N(1), N(2), E(5));
	     NEWTET(E(0), N(1), E(5), N(3));
    
	     break;
	 case 3:			// cofacial
	     rot = get(3,edge);

	     // if edge_2 from current rotation is not refined...
	     if (E(3) == -1)
	     {
		 rot += 6;
	     }

	     assert(E(3) != -1);

//	     NEWTET(N(0), E(0), E(3), N(2));
	     NEWTET(N(0), E(3), E(0), N(2));
	     NEWTET(E(0), E(4), N(1), N(2));
	     NEWTET(E(4), E(3), N(3),N(2));
	     NEWTET(E(0), E(3), E(4), N(2));

	     break;
	 case 6:			// full ref
	     get(6,edge);		// 1->2, 2->3, 3->1
	     rot=0;

	     NEWTET(N(0), E(0), E(2), E(3));
	     NEWTET(N(2), E(2), E(1), E(5));
	     NEWTET(N(1), E(1), E(0), E(4));
	     NEWTET(E(3), E(4), E(5), N(3));
	     NEWTET(E(1), E(2), E(4), E(5));
	     NEWTET(E(2), E(3), E(4), E(5));
	     NEWTET(E(1), E(4), E(2), E(0));
	     NEWTET(E(2), E(4), E(3), E(0));
	     
	     break;

	 default:
	     ERROR("impossible state in refine_tet: " << refstat[eltno]);
	}

	//TRACE(11, "refinement complete");

	// this is kinda awkward and probably slow due to the pointers

	// these values are set in the NEWTET macro above
	refine_arg rarg = { eltno, numnewtets, newtets, &refine_map };
	

	// for the callback to set volumes
	// general entity hook takes refine_arg*
	for (std::list<app_hook*>::iterator hit = hooks.begin();
	     hit != hooks.end(); hit++)
	{
	    (*hit)->run(mgr, &rarg);
	}
    }


private:
    temp_index& refine_map;
    std::vector<int>& refstat;
    const std::vector<int>& new_node_map;
    std::list<app_hook*>& hooks;

    mutable int matched[6];
};





// debug-only validation routine to determine that all tets are being dealt
// with consistently across processors
#ifdef SPLATT_DEBUG
class test_refstat : public splatter::query::query_op
{
public:
    test_refstat(temp_index& refine_map, std::vector<int>& refstat)
	: refine_map(refine_map), refstat(refstat)
    {
    }

    SQ_TERM(idx, eltno, etype, nnodes, nodes)
    {
	int refcount=0;

	// in order of the tet orientations listed above

	int edge[][3] =
	    {						// old edge node
		{ nodes[0], nodes[1], -1 }, // 0	-- 1
		{ nodes[0], nodes[2], -1 }, // 1	-- 0
		{ nodes[0], nodes[3], -1 }, // 2	-- 3
		{ nodes[1], nodes[2], -1 }, // 3	-- 2
		{ nodes[1], nodes[3], -1 }, // 4	-- 4
		{ nodes[2], nodes[3], -1 }  // 5	-- 5
	    };
	
	for (int e=0; e<6; e++)
	{
	    if (refine_map.find(edge[e]) != -1)
	    {
		refcount++;
	    }
	}


	if (refstat[eltno] != refcount)
	{
//	    TRACE(11, eltno << " # " << refstat[eltno] << " /= " << refcount);
	    for (int n=0; n<nnodes; n++) 
	    {
		std::cout << nodes[n] << " ";
	    }
	    std::cout << std::endl;

	    for (int e=0; e<6; e++)
	    {
		std::cout << edge[e][0] << " " << edge[e][1] <<
		    " -> " << refine_map.find(edge[e]) << std::endl;
	    }
	     
	    
	}
	
	assert(refstat[eltno] == refcount);
    }
    

private:
    temp_index& refine_map;
    std::vector<int>& refstat;
};
#endif // SPLATT_DEBUG


status refine_triangles(part_mgr* mgr, explicit_index* idx, temp_index& edgemap,
			std::list<app_hook*>& hooks);


using namespace splatter::query;

// a better -- general -- approach would be to take a starting temp_index of
// entities to refine, such as the initial edge list.  that moves at least one
// entity-specific job into the driver code.

status part_mgr::refine_marked(std::string index_name, std::deque<int> faces, explicit_index* next, temp_index* edges_init)
{
    /*
      topo-aware, mixed-element recursive refinement

      A given refinement (corresponding to a configured callback) can do some
      combination of the following:

      1. request an additional node
      2. recursively process subentities
      3. the entity itself will not be processed until all its subentities are
      complete


      Coming across an entity owned by another process.  We need to agree with
      other process that refinement is necessary.  The owner is responsible
      for driving new node creation that may result from the remotely owned
      entity.

      We need to short-circuit the development of this for tet refinement and
      boundary faces.  In this case, the edge->tet lookup is trivial.

    */

    explicit_index* root_idx = get_index(index_name);

    if (root_idx == NULL)
    {
	ERROR("unknown index in refine_marked: " << index_name);
	return SPLATT_FAIL;
    }

    // for now...
    assert(root_idx->etype() == topo::TET);

    // this vector counts how many edges to refine in each entity
    std::vector<int> ref_status(root_idx->size(), 0);
    DEBUG("ref_status size: " << ref_status.size());

    // temporarily attach ref_status so it will survive
    // deletions/get_more_nodes
    root_idx->attach_data("ref_status", proxy(ref_status));

    // index of locally refined edges
    temp_index refine_edges(this, topo::EDGE, true);

    if (edges_init != NULL)
    {
        ERROR("edges_init is broken");
        refine_edges.add_all(*edges_init);
    }

    {
	deliver_args initial_delivery(pctx());
	
	// // initial entirely-local refine edges
	// query( for_each(root_idx, faces.begin(), faces.end())
	//        >>= store(all_needers(true),"tet_needers")
	//        >>= subfaces(/*tris*/) >>= subfaces(/*edge*/)
	//        >>= save_temp_uniq(refine_edges)
	//        >>= sort(fetch("tet_needers"), face_id(), initial_delivery)
	//     );

	// initial entirely-local refine edges
	query( for_each(root_idx, faces.begin(), faces.end())
	       >>= subfaces(/*tris*/) >>= subfaces(/*edge*/)
	       >>= save_temp_uniq(refine_edges) >>= end()
	    );

	// determine who needs to know about this edge
	query ( for_each(refine_edges) >>= faces_in(root_idx)
		>>= unique()
		>>= store(all_needers(true), "tet_needers")
		>>= subfaces() >>= subfaces() >>= faces_in(refine_edges)
		>>= sort(fetch("tet_needers"), face_id(), initial_delivery)
	    );

	initial_delivery.prepare();

	temp_index incoming(this, topo::EDGE, false);
	if (refine_edges.deliver(initial_delivery, incoming) != SPLATT_OK)
	{
	    ERROR("initial delivery of refine_edges failed");
	    return SPLATT_FAIL;
	}

	refine_edges.add_all(incoming, true);
    }
    
    LOG("initial number of edges to refine: " << refine_edges.size());
    
    
    // propogate changes from edges, ensuring that higher-dimension entities
    // are OK
    bool propogating=true;
    int propogate_cycle=0;

    // while *anybody* needs to keep propogating ...
    while (pctx().reduce((int)propogating, MPI_LOR))
    {
//	TRACE(11, "propogate_cycle: " << propogate_cycle);


	temp_index new_edges(this, topo::EDGE, false);
	deliver_args new_edge_delivery(pctx());

	query( for_each(refine_edges) >>= faces_in(root_idx)
	       >>= where(query::negate(flag(ref_status, MAX_TET_EDGES)))
	       // not yet fully refined tets...
	       >>= unique()
	       >>= store(all_needers(true), "tet_needers")
	       >>= validate_tet(ref_status, refine_edges)
	       >>= save_temp_uniq(new_edges)
	       >>= sort(fetch("tet_needers"), face_id(), new_edge_delivery)
	    );
//	TRACE(11, "finished prop query");
	

	new_edge_delivery.prepare();
	temp_index new_remote_edges(this, topo::EDGE, false);

	if (new_edges.deliver(new_edge_delivery, new_remote_edges) != SPLATT_OK)
	{
	    ERROR("delivery of new_edges failed");
	    return SPLATT_FAIL;
	}

	int original = refine_edges.size();
	refine_edges.add_all(new_edges, true);
	refine_edges.add_all(new_remote_edges, true);

	if (refine_edges.size() != original)
	{
	    propogating=true;
	}
	else
	{
	    propogating=false;
	}
	
	propogate_cycle++;
    }

    
#ifdef SPLATT_DEBUG
    TRACE(11, "validating refstat1");
    query(for_each(root_idx) >>= test_refstat(refine_edges, ref_status));
    TRACE(11, "done validating refstat1");
#endif // SPLATT_DEBUG


//    TRACE(11, "original number of refined edges: " << refine_edges.size());

    
    // // make sure everybody agrees about remote edges before purge
    // {
    // 	deliver_args last_edges(pctx());

    // 	query (for_each(refine_edges) >>=
    // 	       sort(all_needers(true), face_id(), last_edges));
    // 	last_edges.prepare();
    // 	temp_index incoming(this, topo::EDGE, false);

    // 	if (refine_edges.deliver(last_edges, incoming) != SPLATT_OK)
    // 	{
    // 	    ERROR("deliver of last edges failed");
    // 	    return SPLATT_FAIL;
    // 	}

    // 	refine_edges.add_all(incoming, true);
    // }
    // TRACE(11, "post-reconcile refined edges: " << refine_edges.size());
    
    LOG("local + phantom number of edges to refine: " << refine_edges.size());


    // purge remote edges
    {
    	migrate_args re_purge(pctx());

    	query( for_each(refine_edges) >>=
    	       where(query::negate(owns())) >>=
    	       save_ids(re_purge.to_delete) >>=
    	       end() );

    	refine_edges.do_migrate(re_purge);
    }
    

    LOG("propogated number of edges to refine: " << refine_edges.size());

    // get more nodes for thes local refine_edges
    std::vector<int> new_ids(refine_edges.size());

    int newstart;

    {
	std::list<temp_index*> extras;
	extras.push_back(&refine_edges);
//	TRACE(11,"initial tet index size: " << root_idx->size());

	if (get_more_nodes(refine_edges.size(), &newstart, extras) != SPLATT_OK)
	{
	    ERROR("get_more_nodes failed");
	    return SPLATT_FAIL;
	}

//	TRACE(11,"post gmn tet index size: " << root_idx->size());
#ifdef SPLATT_DEBUG
	root_idx->validate_hash();
#endif // SPLATT_DEBUG
    }
    

    // assign new ids
    for (int n=0; n<refine_edges.size(); n++)
    {
	new_ids[n] = _odb.l2g(newstart++);
    }
    
    // attach node ids so they will follow the edges around
    refine_edges.attach_data(refine_id_tag, proxy(new_ids));


    deliver_args edges_for_others(pctx());
    
    // for all refined tets, send refined edges
    query( for_each("tets") >>= where(query::negate(flag(ref_status, 0)))
	   >>= store(all_needers(true), "tet_needers")
	   >>= subfaces() >>=subfaces() >>= faces_in(refine_edges)
//	   >>=  unique()
//	   >>= save_temp_uniq(dummy)
	   >>= sort(fetch("tet_needers"), face_id(), edges_for_others)
	);
    edges_for_others.prepare();
    
    {
	temp_index remote_edges(this, topo::EDGE, false);
	
	if (refine_edges.deliver(edges_for_others, remote_edges) != SPLATT_OK)
	{
	    ERROR("delivery[2] of refined edges failed");
	    return SPLATT_FAIL;
	}

	unsigned int oldsize= new_ids.size();
	refine_edges.add_all(remote_edges);

	// data too...
	// get node ids from remote_edges
	const std::vector<int>& remote_ids =
	    remote_edges.data(refine_id_tag, new_ids);

	// copy remote_ids  into new refine_edges map
	std::copy(remote_ids.begin(), remote_ids.end(),
		  new_ids.begin()+oldsize);


    }

//    TRACE(11, "post-rebuild number of refined edges: " << refine_edges.size());


#ifdef SPLATT_DEBUG
    TRACE(11, "validating refstat");
    refine_edges.validate_hash();
    query(for_each(root_idx) >>= test_refstat(refine_edges, ref_status));
    TRACE(11, "done validating refstat");
#endif // SPLATT_DEBUG
    
    // now we have all refined edges and their corresponding new ids
    //

    // augment_nodes();
    // call application hooks before renumbering

    // node hook (type 0) takes edge list
    for (std::list<app_hook*>::iterator it = _refine_hooks[0].begin();
	 it != _refine_hooks[0].end(); it++)
    {
	(*it)->run(this, &refine_edges);
    }
    

#ifdef SPLATT_DEBUG
    TRACE(11, "validating refstat");
    query(for_each(root_idx) >>= test_refstat(refine_edges, ref_status));
    TRACE(11, "done validating refstat");
#endif // SPLATT_DEBUG    

    std::deque<int> to_delete;
    

    // do the renumbering !
    DEBUG("replacing entities: " << root_idx->size() << "; " << ref_status.size());
    query(for_each(root_idx) >>= where(query::negate(flag(ref_status, 0)))
	  >>= where(owns())
	  >>= save_ids(to_delete)
	  >>= refine_tet(refine_edges, ref_status, _refine_hooks[root_idx->etype()])
	);
    DEBUG("done replacing entities");
    
    root_idx->delete_faces(to_delete.begin(), to_delete.end());
    root_idx->detach_data("ref_status");
    root_idx->sync_phantom_layer(true);
    
    if (next != NULL)
    {
	refine_triangles(this, next, refine_edges, _refine_hooks[topo::TRI]);

#ifdef SPLATT_DEBUG
	int fcount=0;
	TRACE(11000,"looking for bum faces in tets");
	query( for_each(refine_edges) >>= faces_in(root_idx) >>= count(fcount) >>= end());
	TRACE(11000, "found " << fcount);
	assert(fcount == 0);
	TRACE(11000, "no faces left in tets -- checking boundaries");
	query( for_each(refine_edges) >>= faces_in(next) >>= count(fcount) >>= end());
	TRACE(11000, "found " << fcount);
	assert(fcount == 0);
	TRACE(11000, "boundaries are OK");
#endif
	
    }

    augment_nodes();

    return SPLATT_OK;
}


class refine_tri : public splatter::query::query_op
{
public:
    refine_tri(temp_index& refine_map, std::list<app_hook*>& hooks) :
	refine_map(refine_map),
	new_node_map(refine_map.data(refine_id_tag, std::vector<int>())),
	hooks(hooks)
    {
    }

    SQ_TERM(idx,eltno,etype,nnodes,inodes)
    {
	int nodes[3];
	for (int n=0; n<3; n++)
	{
	    nodes[n] = inodes[n];
	}

	assert(etype == topo::TRI);
	int edge[][3] =
	    {
		{ nodes[0], nodes[1], -1 },
		{ nodes[1], nodes[2], -1 },
		{ nodes[2], nodes[0], -1 }
	    };

	int refcount=0;
	int theedge=0;
	for (int e=0; e<3; e++)
	{
	    int c;
	    c = refine_map.find(edge[e]);
	    if (c != -1)
	    {
		edge[e][2]=new_node_map[c];
		theedge=e;
		refcount++;
	    }
	}

	assert(refcount == 1 || refcount == 3);

	int numnew=0;
	int newtri[3];
	int newtris[4];


	if (refcount == 1)
	{
	    newtri[0] = nodes[theedge];
	    newtri[1] = edge[theedge][2];
	    newtri[2] = nodes[(theedge+2) % 3];
	    newtris[numnew++]=static_cast<explicit_index*>(idx)->add_face(newtri);

	    newtri[0] = edge[theedge][2];
	    newtri[1] = nodes[(theedge+1) % 3];
	    newtri[2] = nodes[(theedge+2) % 3];
	    newtris[numnew++]=static_cast<explicit_index*>(idx)->add_face(newtri);
	}
	else
	{
	    // full split
	    newtri[0] = nodes[0];
	    newtri[1] = edge[0][2];
	    newtri[2] = edge[2][2];
	    newtris[numnew++]=static_cast<explicit_index*>(idx)->add_face(newtri);

	    newtri[0] = edge[0][2];
	    newtri[1] = nodes[1];
	    newtri[2] = edge[1][2];
	    newtris[numnew++]=static_cast<explicit_index*>(idx)->add_face(newtri);

	    newtri[0] = edge[1][2];
	    newtri[1] = nodes[2];
	    newtri[2] = edge[2][2];
	    newtris[numnew++]=static_cast<explicit_index*>(idx)->add_face(newtri);

	    newtri[0] = edge[0][2];
	    newtri[1] = edge[1][2];
	    newtri[2] = edge[2][2];
	    newtris[numnew++]=static_cast<explicit_index*>(idx)->add_face(newtri);
	}


	refine_arg rarg = { eltno, numnew, newtris, &refine_map };

	// etype hook takes refine_arg*
	for (std::list<app_hook*>::iterator it = hooks.begin();
	     it != hooks.end(); it++)
	    {
		(*it)->run(mgr, &rarg);
	    }

    }


private:
    temp_index& refine_map;
    const std::vector<int>& new_node_map;
    std::list<app_hook*>& hooks;
};


// assumes no propogation will be required, as it was handled by tet
// refinement process
status refine_triangles(part_mgr* mgr, explicit_index* idx, temp_index& edgemap,
			std::list<app_hook*>& hooks)
{
// #ifdef SPLATT_DEBUG
//     TRACE(11000, "validating bnd hash: pre refinement");
//     idx->validate_hash();
//     TRACE(11000, "done validating bnd hash: pre refinement");
// #endif // SPLATT_DEBUG

    TRACE(626, "initial bnd size: " << idx->size());

    std::deque<int> to_delete;
    mgr->query(
	all_faces(edgemap) >>= faces_in(idx)  >>= unique()
	>>= save_ids(to_delete) >>= where(owns()) >>= refine_tri(edgemap, hooks)
	);
//    TRACE(11000, "deleting " << to_delete.size() << " triangles");
    idx->delete_faces(to_delete.begin(), to_delete.end());

// #ifdef SPLATT_DEBUG
//     TRACE(11000, "checking for proper deletions right here");
//     int fcount;
//     mgr->query( for_each(edgemap) >>= dump() >>= faces_in(idx) >>= dump() >>= count(fcount) >>= end());
//     TRACE(11000, "found " << fcount);
//     assert(fcount == 0);
// #endif // SPLATT_DEBUG

    idx->sync_phantom_layer();
// #ifdef SPLATT_DEBUG
//     TRACE(11000, "validating bnd hash post refinement");
//     idx->validate_hash();
//     TRACE(11000, "done validating bnd hash post refinement");


// #endif // SPLATT_DEBUG
    return SPLATT_OK;
}

