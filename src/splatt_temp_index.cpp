#include <splatter.h>
using namespace splatter;

std::vector<int> temp_index::dummy_nodes;

temp_index::temp_index (part_mgr* mgr, int entity_type, bool update_hash) :
    explicit_index(mgr->get_ownerdb(), "temp",
		   mgr->get_etypes()[entity_type].nnodes,
		   entity_type, dummy_nodes, SPLATT_STEAL_DATA)
    {
//	    DEBUG("temp_index width: " << _width);
        _deliver_during_renumber=false;
	_purge_during_renumber=false;
	_live_hash = true;
	if (update_hash)
	{
	    _node2faces.update(_odb.low(), _odb.high());
	}
    }


temp_index::temp_index (explicit_index& ei, bool update_hash) :
    explicit_index(ei._odb, "temp", ei._width, ei._etype, dummy_nodes, SPLATT_STEAL_DATA)
    {
        _deliver_during_renumber=false;
	_purge_during_renumber=false;
	_live_hash = true;
	if (update_hash)
	{
	    _node2faces.update(_odb.low(), _odb.high());
	}
    }



// this is static
temp_index temp_index::no_type(part_mgr* mgr, int width, bool update_hash)
{
    return temp_index(mgr, true, width, update_hash);
}


// this is static
temp_index temp_index::vec_wrapper(part_mgr* mgr, std::vector<int>& data)
{
    return temp_index(mgr, true, data);
}



// no_type constructor
temp_index::temp_index(part_mgr* mgr, bool ignored, int width, bool update_hash) :
    explicit_index(mgr->get_ownerdb(), "temp/notype", width,
                   -1, dummy_nodes, SPLATT_STEAL_DATA)
{
    _purge_during_renumber = false;
    _live_hash = true;
    if (update_hash)
    {
        _node2faces.update(_odb.low(), _odb.high());
    }
}


// vec_wrapper constructor
temp_index::temp_index(part_mgr* mgr, bool x, std::vector<int>& data) :
    explicit_index(mgr->get_ownerdb(), "temp/vecwrapper", 1, -1, data)
{
    _purge_during_renumber = false;
    _live_hash=false;
}




status temp_index::renumber(const ownerdb& new_odb,
			    const std::vector<int>& newgids,
			    std::map<int,int>& global_map)
{
    LLOG(1, _odb << new_odb);
    std::vector< std::set<int> > remote_needed(_odb.pctx().np());

    for (unsigned int n=0; n<_nodes.size(); n++)
    {
        if (! _odb.owns(_nodes[n]))
        {
            if (global_map.find(_nodes[n]) == global_map.end())
            {
                remote_needed[_odb.owner(_nodes[n])].insert(_nodes[n]);
            }	    
        }
    }


    // exchange requirements
    
    // communicate with other ranks to determine needed global ids
    std::vector<int> old_sizes(_odb.np());
    std::vector<int> new_sizes(_odb.np());
    std::vector<int> old_ids;
    std::vector<int> old_displs(_odb.np());
    std::vector<int> new_ids;
    std::vector<int> new_displs(_odb.np());

    old_displs[0]=0;
    for (int r=0; r<_odb.np(); r++)
    {
	old_sizes[r] = remote_needed[r].size();

	if (r>0)
	{
	    old_displs[r] = old_displs[r-1] + old_sizes[r-1];
	}
    }
    old_ids.resize(old_displs.back() + old_sizes.back());

    // copy ids from set to buffer
    std::vector<int>::iterator buffit=old_ids.begin();

    int accum=0;
    for (int r=0; r<_odb.np(); r++)
    {
	accum += remote_needed[r].size();
	buffit = std::copy(remote_needed[r].begin(),
			   remote_needed[r].end(),
			   buffit);
    }


    // exchange message sizes
    MPI_Alltoall(&old_sizes[0], 1, MPI_INT,
		 &new_sizes[0], 1, MPI_INT, _odb.pctx().comm());


    // calculate incoming displacements
    new_displs[0] = 0;
    for (unsigned int r=1; r<new_sizes.size(); r++)
    {
	new_displs[r] = new_displs[r-1] + new_sizes[r-1];
    }
    new_ids.resize(new_displs.back() + new_sizes.back());


    int fake[]= {0};
    
    // exchange list of required ids
    MPI_Alltoallv((old_ids.size() == 0 ? fake : &old_ids[0]),
		  &old_sizes[0],
		  &old_displs[0],
		  MPI_INT,
		  (new_ids.size() == 0 ? fake : &new_ids[0]),
		  &new_sizes[0],
		  &new_displs[0],
		  MPI_INT,
		  _odb.pctx().comm());



    // look up all the requested ids
    for (unsigned int n=0; n<new_ids.size(); n++)
    {
	new_ids[n] = newgids[_odb.g2l(new_ids[n])];
    }



        // send/retrieve remote ids from all other ranks
    MPI_Alltoallv((new_ids.size() == 0 ? fake : &new_ids[0]),
		  &new_sizes[0],
		  &new_displs[0],
		  MPI_INT,
		  (old_ids.size() == 0 ? fake : &old_ids[0]),
		  &old_sizes[0],
		  &old_displs[0],
		  MPI_INT,
		  _odb.pctx().comm());


    // old_ids has the new globals for the needed nodes in numeric order from
    // remote_needed

    new_ids.clear();
    new_sizes.clear();
    new_displs.clear();

    // build up global renumber map
    


    int curs=0;
    for (int r=0; r<_odb.np(); r++)
    {
	for (std::set<int>::const_iterator ci=remote_needed[r].begin();
	     ci != remote_needed[r].end();
	     ci++)
	{
	    assert(global_map.find(*ci) == global_map.end());
	    global_map[*ci] = old_ids[curs++];
	}
    }


    remote_needed.clear();
    old_ids.clear();


    for (unsigned int n=0; n<_nodes.size(); n++)
    {
        if (_nodes[n] != -1)
        {
            if (_odb.owns(_nodes[n]))
            {
                _nodes[n] = newgids[_odb.g2l(_nodes[n])];
            }
            else
            {
                _nodes[n] = global_map[_nodes[n]];
            }
        }
    }

    
    LLOG(2, "building hash after renumber");
    build_hash();
    LLOG(2, "renumber complete");

    return SPLATT_OK;
}


status temp_index::clear()
{
    _node2faces.clear();
    _nodeproxy.clear();
    return SPLATT_OK;
}
