//  -*- Mode: C++; -*-
#include <splatter.h>
#include <cassert>
using namespace splatter;

//static const int line_length=1024;


status part_mgr::save_start(std::string path_prefix)
{
    std::string filename;
    
    if (save_stream.is_open())
    {
	ERROR("stream is already open");
	return SPLATT_FAIL;
    }

    filename = path_prefix + "." + stringify(_pctx.rank());
    save_stream.open( filename.c_str() ) ;

    if (! save_stream.is_open())
    {
	ERROR("could not open " << filename << " for writing");
	return SPLATT_FAIL;
    }
    save_stream << std::scientific << std::setprecision(std::numeric_limits<double>::digits10);

    save_stream << _odb.low() << " " << _odb.high() << std::endl;

    for (unsigned int idx=0; idx<_indices.size(); idx++)
    {
	save_stream << _indices[idx]->name() << std::endl;
	_indices[idx]->save(save_stream);
    }
    
    return SPLATT_OK;
}

status part_mgr::save_node_data(std::string tag)
{
    save_stream << tag << std::endl;
    return _node_index.dproxy(tag)->save(save_stream, _odb.nlocal());
}


status part_mgr::save_data(std::string idxname, std::string tag)
{
    save_stream << tag << " " << idxname << std::endl;
    return get_index(idxname)->dproxy(tag)->save(save_stream);
}


int part_mgr::load(std::string path_prefix)
{
    std::string filename;
    std::vector<char>buffer(1024);

    filename = path_prefix + "." + stringify(_pctx.rank());
    load_stream.open( filename.c_str() ) ;

    if (load_stream.is_open())
    {
        load_stream.getline(&buffer[0], buffer.size());

        int low_node, high_node;
        {
            std::stringstream ss(&buffer[0]);

            ss >> low_node;
            ss >> high_node;
        }
    
//    config_odb(low_node, high_node);
        LLOG(1, "restarting with nodes " << low_node << " - " << high_node);
        config_node_index(high_node - low_node);
        assert(_odb.low() == low_node);
        assert(_odb.high() == high_node);



        LLOG(1, "loading restart indices");
        for (unsigned int idx=0; idx<_indices.size(); idx++)
        {
            std::string index_name;
            index_name.reserve(buffer.size());
            TRACE(626, "cap: " << index_name.capacity());
            load_stream.getline(&buffer[0], buffer.size());
            std::stringstream ss(&buffer[0]);
            TRACE(626, "str: " << ss.str());
            ss >> index_name;
            LLOG(1, "loading nodes for index " << index_name);
            get_index(index_name)->load(load_stream);
        }

        while (load_stream.getline(&buffer[0], buffer.size()))
        {
            std::stringstream ss(&buffer[0]);
	
            std::string data_tag;
            data_tag.reserve(buffer.size());
            std::string index_name;
            index_name.reserve(buffer.size());

            ss >> data_tag;
            index* idx=NULL;
	

            if (ss >> index_name)
            {
                // explicit index
                idx= get_index(index_name);
                LLOG(1, "loading data for index " << index_name << " - " << data_tag);
            }
            else
            {
                idx = &_node_index;
                LLOG(1, "loading data for node index - " << data_tag);
            }

            // node data
            data_proxy* dp = idx->dproxy(data_tag);

            if (dp == NULL)
            {
                ERROR("missing data " << data_tag);
                return SPLATT_FAIL;
            }

            dp->load(load_stream);
	
        }

        load_stream.close();

        return _pctx.reduce(1, MPI_SUM);
    }
    else
    {
        // set all local sizes to 0?
        LOG("not taking part in load");
        config_node_index(0);

        return _pctx.reduce(0, MPI_SUM);
    }
}



status part_mgr::save_end()
{
    if (! save_stream.is_open())
    {
	ERROR("no save in progress");
	return SPLATT_FAIL;
    }

    save_stream.close();
    return SPLATT_OK;
}



