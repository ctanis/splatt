//  -*- Mode: C++; -*-

#ifdef SPLATT_CGNS 

#include <splatter.h>
#include <cassert>
#include <limits>
using namespace splatter;

#define cgnsErrchk(ans) { if (CGNS_Errorcheck((ans), __FILE__, __LINE__) == SPLATT_FAIL) {return SPLATT_FAIL;} }
#define MAX_LENGTH 256

static status CGNS_Errorcheck(int ier, const char* file, int line)
{
    char *error_message;
    if (ier != 0)
    {
        
        error_message = (char *)cg_get_error();
        ERROR_NO_ABORT("CGNS Error detected. Error ("<<ier<<") "<< error_message << " - " << file << "("<<line<<")");
        return SPLATT_FAIL;
    }

    return SPLATT_OK;
}



// node orderings taken from
// http://cgns.github.io/CGNS_docs_current/sids/conv.html#unstructgrid
const entity_cfg cgns::cgns_entities[] =
{
    /* NODE */
    { "NODE", 0, 1, 0 },
    /* EDGE */
    { "EDGE2", 1, 2, 2,
      {
          { cgns::NODE, { 0 } },
          { cgns::NODE, { 1 } }        
      }
    },
    { "EDGE3", 1, 3, 3,
      {
          { cgns::NODE, { 0 } },
          { cgns::NODE, { 2 } },
          { cgns::NODE, { 1 } }
      }
    },
    { "TRI3", 2, 3, 3,
      {
          { cgns::EDGE2, { 0, 1} },
          { cgns::EDGE2, { 1, 2} },
          { cgns::EDGE2, { 2, 0} },
      }        
    },
    { "TRI6", 2, 6, 3 ,
      {
          { cgns::EDGE3, { 0, 3, 1 } },
          { cgns::EDGE3, { 1, 4, 2 } },
          { cgns::EDGE3, { 2, 5, 0 } }
      }
    },
    { "QUAD4", 2, 4, 4,
      {
          { cgns::EDGE2, { 0, 1 } },
          { cgns::EDGE2, { 1, 2 } },
          { cgns::EDGE2, { 2, 3 } },
          { cgns::EDGE2, { 3, 0 } }          
      }
    },
    { "QUAD9", 2, 9, 4,        // should the face-node be a standalone subface?
      {
          { cgns::EDGE3, { 0, 4, 1 } },
          { cgns::EDGE3, { 1, 5, 2 } },
          { cgns::EDGE3, { 2, 6, 3 } },
          { cgns::EDGE3, { 3, 7, 0 } }
      }
    },
    { "TET4", 3, 4, 4,
      {
          { cgns::TRI3, { 0, 2, 1 } },
          { cgns::TRI3, { 1, 2, 3 } },
          { cgns::TRI3, { 0, 1, 3 } },
          { cgns::TRI3, { 0, 3, 2 } }
      }
    },
    { "TET10", 3, 10, 4,
      {
          { cgns::TRI6, { 0, 6, 2, 5, 1, 4 } },
          { cgns::TRI6, { 1, 5, 2, 9, 3, 8 } },
          { cgns::TRI6, { 0, 4, 1, 8, 3, 7 } },
          { cgns::TRI6, { 0, 7, 3, 9, 2, 6 } }
      }
    },
    { "PYR5", 3, 5, 5,
      {
          { cgns::QUAD4, { 0, 3, 2, 1 } },
          { cgns::TRI3, { 0, 4, 3 } },
          { cgns::TRI3, { 0, 1, 4 } },
          { cgns::TRI3, { 1, 2, 4 } },
          { cgns::TRI3, { 2, 3, 4 } }
      }
    },
    { "PYR14", 3, 14, 5,
      {
          { cgns::QUAD9, { 0, 8, 3, 7, 2, 6, 1, 5 } },
          { cgns::TRI6, { 0, 9, 4, 12, 3, 8 } },
          { cgns::TRI6, { 0, 5, 1, 10, 4, 9 } },
          { cgns::TRI6, { 1, 6, 2, 11, 4, 10 } },
          { cgns::TRI6, { 2, 7, 3, 12, 4, 11 } }
      }
    },
    { "PRISM6", 3, 6, 5,
      {
          { cgns::TRI3, { 0, 2, 1 } },
          { cgns::TRI3, { 3, 4, 5 } },
          { cgns::QUAD4, { 0, 1, 4, 3 } },
          { cgns::QUAD4, { 1, 2, 5, 4 } },
          { cgns::QUAD4, { 2, 0, 3, 5 } }
      }
    },
    { "PRISM18", 3, 18, 5,
      {
          { cgns::TRI6, { 0, 8, 2, 7, 1, 6 } },
          { cgns::TRI6, { 3, 12, 4, 13, 5, 14 } },
          { cgns::QUAD9, { 0, 6, 1, 10, 4, 12, 3, 9, 15 } },
          { cgns::QUAD9, { 1, 7, 2, 11, 5, 13, 4, 10, 16 } },
          { cgns::QUAD9, { 2, 8, 0, 9, 3, 14, 5, 11, 17 } }
      }
    },
    { "HEX8", 3, 8, 6,
      {
          { cgns::QUAD4, { 3, 0, 4, 7 } },
          { cgns::QUAD4, { 0, 1, 5, 4 } },
          { cgns::QUAD4, { 1, 2, 6, 5 } },
          { cgns::QUAD4, { 2, 3, 7, 6 } },
          { cgns::QUAD4, { 3, 2, 1, 0 } },
          { cgns::QUAD4, { 4, 5, 6, 7 } }
      }        
    },
    { "HEX27", 3, 27, 6,
      {
          { cgns::QUAD9, { 3, 11, 0, 12, 4, 19, 7, 15, 24 } },
          { cgns::QUAD9, { 0, 8, 1, 13, 5, 16, 4, 12, 21 } },
          { cgns::QUAD9, { 1, 9, 2, 14, 6, 17, 5, 13, 22 } },
          { cgns::QUAD9, { 2, 10, 3, 15, 7, 18, 6, 14, 23 } },
          { cgns::QUAD9, { 3, 10, 2, 9, 1, 8, 0, 11, 20 } },
          { cgns::QUAD9, { 4, 16, 5, 17, 6, 18, 7, 19, 25 } }
      }        
    },

    SPLATT_ENTITY_END
    
};




// look at the parts of the designated CGNS file
status cgns::cgns_analyze(std::string filename)
{
    LLOG(0, "in cgns_analyze: " << filename);

    int cg, nbases;
    float version;

    LOG("filename: " << filename);
    cgnsErrchk( cg_open(filename.c_str(), CG_MODE_READ, &cg) );

    LOG("cg: " << cg);

    cgnsErrchk( cg_version(cg, &version) );
    
    LOG("CGNS file version " << version);

    cgnsErrchk( cg_nbases(cg, &nbases) );

    LOG("num bases: " << nbases);
    
    for (int base=1; base <= nbases; base++)
    {
        int celldim, physdim, nzones;
        char basename[MAX_LENGTH];

        cgnsErrchk( cg_base_read(cg, base, basename, &celldim, &physdim) );

        LOG("base " << base << "("<<basename<<"): " << celldim << "; " << physdim);

        cgnsErrchk ( cg_nzones(cg, base, &nzones) );
        LOG("nzones: " << nzones);

        for (int zone=1; zone <= nzones; zone++)
        {
            char zonename[MAX_LENGTH];
            ZoneType_t zonetype;
            cgsize_t isize[3];       // ndim*3
            int ncoords;

            cgnsErrchk( cg_zone_read(cg, base, zone, zonename, isize) );
            cgnsErrchk( cg_zone_type(cg, base, zone, &zonetype) );
            cgnsErrchk( cg_ncoords(cg, base, zone, &ncoords) );

            LOG("Zone " << zone << "("<<zonename<<"): "<< (int)(zonetype) << "|" << (zonetype == Unstructured) << "|" << ncoords);

            assert(zonetype == Unstructured);
            
            LOG("num vertices: " << isize[0] << "; num elements: " << isize[1] << "; num bdry verts: " << isize[2]);

            int nsections;
            cgnsErrchk( cg_nsections(cg, base, zone, &nsections) );
            LOG("num sections: " << nsections);

            for (int sect = 1; sect <= nsections; sect++)
            {
                char nodename[MAX_LENGTH];
                ElementType_t type;
                cgsize_t start, end;
                int nbdry, parent_flag, npe;
                
                cgnsErrchk( cg_section_read(cg, base, zone, sect,
                                            nodename, &type, &start, &end,
                                            &nbdry, &parent_flag) );
                cgnsErrchk( cg_npe(type, &npe) );

                LOG("section " << sect << "("<< nodename <<"): "
                    << (int)(type) << "("<<npe<<")," << start << "," << end <<
                    "," << nbdry << "," << parent_flag);
                
            }

        }

    }

    return SPLATT_OK;
}


status cgns::cgns_init(std::string filename, parallel_ctx ctx, cgns_handle* hp)
{
    cgnsErrchk( cg_open(filename.c_str(), CG_MODE_READ, &(hp->cg)) );
    
    hp->pctx = ctx;

    return SPLATT_OK;
}

status cgns::cgns_close(cgns_handle* hp)
{
    cgnsErrchk( cg_close(hp->cg) );
    return SPLATT_OK;
}


#define NDIM 3                  // 3-d coordinates
#define MAX_STR 256

status cgns::cgns_load_coords( cgns_handle* hp, int base, int zone,
                               std::vector<node_coords>& coords)
{
    cgsize_t isize[3];
    char zonename[MAX_STR];
    // cgnsErrchk( cg_ncoords(hp->cg, base, zone, &ncoords) );
    cgnsErrchk( cg_zone_read(hp->cg, base, zone, zonename, isize) );

    // determine coordinate count per rank in hp->pctx
    cgsize_t ncoords = isize[0];
    LLOG(0, "number of vertices in CGNS zone: " << ncoords);

    cgsize_t start, end, count, rem;

    count = ncoords / hp->pctx.np();
    rem = ncoords % hp->pctx.np();
    start = count * hp->pctx.rank();

    if (hp->pctx.rank() < rem)
    {
        count++;
        start += hp->pctx.rank();
    }
    else
    {
        start += rem;
    }
    end = start+count;

    start++;                    // 1-indexed

    std::vector<double> x, y, z;

    LLOG(0, "getting coords from " << start << " to " << end << " of " << ncoords);

    x.resize(count);
    y.resize(count);
    z.resize(count);
    coords.resize(count);

    cgnsErrchk( cg_coord_read(hp->cg, base, zone, "CoordinateX", RealDouble,
                              &start, &end, x.data()) );
    cgnsErrchk( cg_coord_read(hp->cg, base, zone, "CoordinateY", RealDouble,
                              &start, &end, y.data()) );
    cgnsErrchk( cg_coord_read(hp->cg, base, zone, "CoordinateZ", RealDouble,
                              &start, &end, z.data()) );
    
    for (int i=0; i<count; i++)
    {
        coords[i][0] = x[i];
        coords[i][1] = y[i];
        coords[i][2] = z[i];
    }

    return SPLATT_OK;
}

status cgns::cgns_load_elements(cgns_handle* hp, int base, int zone, int section,
                                std::vector<int>& c2n,
                                cgns::cgns_entity_type& etype,
                                std::string& name)
{
    char nodename[MAX_LENGTH];
    cgsize_t sec_start, sec_end;
    int nbdry, parent_flag, npe;
    ElementType_t type;

    cgnsErrchk( cg_section_read(hp->cg, base, zone, section,
                                nodename, &type, &sec_start, &sec_end,
                                &nbdry, &parent_flag) );
    cgnsErrchk( cg_npe(type, &npe) );

    switch (type)
    {
     case TRI_3: etype = cgns::TRI3; assert(npe==3); break;
     case TRI_6: etype = cgns::TRI6; assert(npe==6); break;
     case QUAD_4: etype = cgns::QUAD4; assert(npe==4); break;
     case QUAD_9: etype = cgns::QUAD9; assert(npe==9); break;
     case TETRA_4: etype = cgns::TET4; assert(npe==4); break;
     case TETRA_10: etype = cgns::TET10; assert(npe==10); break;
     case PYRA_5: etype = cgns::PYR5; assert(npe==5); break;
     case PYRA_14: etype = cgns::PYR14; assert(npe==14); break;
     case PENTA_6: etype = cgns::PRISM6; assert(npe==6); break;
     case PENTA_18: etype = cgns::PRISM18; assert(npe==18); break;
     case HEXA_8: etype = cgns::HEX8; assert(npe==8); break;
     case HEXA_27: etype = cgns::HEX27; assert(npe==27); break;
     default:
         ERROR("unsupport element type: " << (int)(type));                
    }

    cgsize_t start, count, rem;

    cgsize_t  num_elements = (sec_end - sec_start)+1;
    
    count = num_elements / hp->pctx.np();
    rem = num_elements % hp->pctx.np();
    start = count * hp->pctx.rank();

    if (hp->pctx.rank() < rem)
    {
        count++;
        start += hp->pctx.rank();
    }
    else
    {
        start += rem;
    }

    c2n.resize(npe*count);

    std::vector<cgsize_t> raw;
    raw.resize(npe*count, 0);
    
    LLOG(0, "getting elements from " << sec_start+start << " to " << sec_start+start+count-1 << " of " << num_elements << " -- " << raw.size() << " / " << npe << " of type " << type << "|"<<nodename);

    cgnsErrchk( cg_elements_partial_read(hp->cg, base, zone, section,
                                         sec_start+start,
                                         sec_start+start+count-1,
                                         raw.data(), NULL) );

    // cgnsErrchk( cg_elements_read(hp->cg, base, zone, section,
    //                              raw.data(), NULL) );
    
    for (unsigned int i=0; i<c2n.size(); i++)
    {
        // c2n[i]--; // make 0-indexed eventually we will use longs for node
        // ids (or at least parameterize it)
        assert(raw[i] <= std::numeric_limits<int>::max());
        c2n[i] = raw[i]-1;
        assert(c2n[i] >= 0);
    }

    name = std::string(nodename);
    return SPLATT_OK;
}

#define CHK(s) { if ((s) == SPLATT_FAIL) return SPLATT_FAIL; }

// load all sections from base/zone as explicit indices in mgr
// currently requires all ranks to be involved
// to change this, specify max_ranks for loading, but have everyone access the
// file to set up the indices.
status cgns::cgns_load_all(part_mgr& mgr, std::string filename, int base, int zone)
{
    cgns_handle hp;

    CHK( cgns_init(filename, mgr.pctx(), &hp) );

    std::vector<node_coords> coords;
    cgns_load_coords(&hp, base, zone, coords);
    int size =  coords.size();

    mgr.config_node_index(size)->
        attach_data("xyz", proxy(coords, 1, SPLATT_STEAL_DATA));


    int nsections;
    cgnsErrchk( cg_nsections(hp.cg, base, zone, &nsections) );

    for (int sect=1; sect <= nsections; sect++ )
    {
        std::string name;
        cgns::cgns_entity_type etype;
        std::vector<int> nodes;

        CHK( cgns_load_elements(&hp, base, zone, sect,
                                nodes, etype, name) );

        // assumes name is unique
        mgr.add_explicit_index(name, etype, nodes,
                               SPLATT_PHANTOM_DEPS |
                               SPLATT_FAST_INDEX |
                               SPLATT_STEAL_DATA);
    }
    
    CHK( cgns_close(&hp) );

    mgr.finalize_load();
    return SPLATT_OK;
}

static ElementType_t cgns_type(int etype)
{
    switch (etype)
    {
     case cgns::NODE: return NODE;
     case cgns::EDGE2: return BAR_2;
     case cgns::EDGE3: return BAR_3;
     case cgns::TRI3: return TRI_3;
     case cgns::TRI6: return TRI_6;
     case cgns::QUAD4: return QUAD_4;
     case cgns::QUAD9: return QUAD_9;
     case cgns::TET4: return TETRA_4;
     case cgns::TET10: return TETRA_10;
     case cgns::PYR5: return PYRA_5;
     case cgns::PYR14: return PYRA_14;
     case cgns::PRISM6: return PENTA_6;
     case cgns::PRISM18: return PENTA_18;
     case cgns::HEX8: return HEXA_8;
     case cgns::HEX27: return HEXA_27;
     default: ERROR("unsupported element type: " << etype);
    };

    return NODE;
}


status cgns::cgns_write_all(part_mgr& mgr, std::string file_prefix)
{
    int cg;
    cgsize_t isize[3];
    mgr.augment_nodes();
    
    cgnsErrchk (cg_open( (file_prefix + "-" + stringify(mgr.pctx().rank()) +".cgns").c_str(),
                         CG_MODE_WRITE, &cg));


    int num_cells = 0;
    std::vector<explicit_index*> indices = mgr.indices();

    for (unsigned int i=0; i<indices.size(); i++)
    {
        // count volume elements
        if (cgns_entities[indices[i]->etype()].dim == 3)
        {
            num_cells += indices[i]->size(); // *indices[i]->width();
        }
    }
    

    int base=1;

    cgnsErrchk ( cg_base_write(cg, ("base "+stringify(mgr.pctx().rank())).c_str(),
                               3, 3, &base) );
    isize[0] = mgr.get_ownerdb().nlocal_p();
    isize[1] = num_cells;
    isize[2] = 0;

    int zone=1;
    cgnsErrchk( cg_zone_write(cg, 1, "zone", isize, Unstructured, &zone) );

    std::vector<double> x, y, z;
    x.resize(mgr.get_ownerdb().nlocal_p());
    y.resize(mgr.get_ownerdb().nlocal_p());
    z.resize(mgr.get_ownerdb().nlocal_p());

    std::vector<node_coords> coords =
        mgr.get_node_index()->data("xyz", coords);

    for (unsigned int i=0; i<coords.size(); i++)
    {
        x[i] = coords[i][0];
        y[i] = coords[i][1];
        z[i] = coords[i][2];
    }
    
    int c=1;
    cgnsErrchk(cg_coord_write(cg, 1, 1, RealDouble, "CoordinateX", x.data(), &c));
    c=2;
    cgnsErrchk(cg_coord_write(cg, 1, 1, RealDouble, "CoordinateY", y.data(), &c));
    c=3;
    cgnsErrchk(cg_coord_write(cg, 1, 1, RealDouble, "CoordinateZ", z.data(), &c));

    cgsize_t start=1;
    for (unsigned int idx=0; idx<indices.size(); idx++)
    {
        int sect=idx+1;
        int size = indices[idx]->size()*indices[idx]->width();
        const int* orig = indices[idx]->local_nodes();

        std::vector<cgsize_t> elements(size);

        for (int i=0; i<size; i++)
        {
            elements[i] = orig[i]+1; // 1-index
        }
        
        LOG("writing " << start << " to " << start+indices[idx]->size());

        cg_section_write(cg, 1, 1, indices[idx]->name().c_str(),
                         cgns_type(indices[idx]->etype()),
                         start, start+indices[idx]->size()-1, 0, elements.data(), &sect);
        start += indices[idx]->size();
    }

    cgnsErrchk( cg_close(cg) );
    return SPLATT_OK;
}
#endif
