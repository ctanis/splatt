#include <splatter.h>
using namespace splatter;


// only part_mgr should be able to do this..
ownerdb& ownerdb::operator=(ownerdb& other)
{
    if (this != &other)
    {
//	_topo_cfg = other._topo_cfg;
	_pctx = other._pctx;
	_node_dist.swap(other._node_dist);
	_rank = other._rank;
	_np = other._np;
	_low = other._low;
	_high = other._high;

	_glob2loc.swap(other._glob2loc);
	_loc2glob.swap(other._loc2glob);
    }

    return *this;
}


status ownerdb::init(const parallel_ctx& pctx, std::vector<int>& node_dist)
{
    _pctx = pctx;
    _node_dist.swap(node_dist);
    _rank = _pctx.rank();
    _np = _pctx.np();
    _low = _node_dist[_rank];
    _high = _node_dist[_rank+1];
    return SPLATT_OK;
}



void ownerdb::debug_phantoms() const
{
#ifdef SPLATT_DEBUG

    std::cout  << "phantoms: ";
    for (unsigned int n=0; n<_loc2glob.size(); n++)
    {
	std::cout << _loc2glob[n] << " ";
    }
    std::cout << std::endl;
#endif // SPLATT_DEBUG
}
