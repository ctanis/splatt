//  -*- Mode: C++; -*- 
#include <splatter.h>
#include <splatt_query.h>

using namespace splatter::query;


int splatter::count_duplicates(part_mgr* mgr, std::string name)
{
    int total, uniq;
    mgr->query(
	all_faces(name) >>= count(total) >>= unique() >>= count(uniq) >>= end()
	);

    return total-uniq;
}


int splatter::count_unneeded(part_mgr* mgr, std::string name)
{
    int total, need;
    mgr->query(
	all_faces(name) >>= count(total) >>= where(needs())
	>>= count(need) >>= end()
	);

    return total-need;
}


int splatter::count_duplicates(part_mgr* mgr, temp_index& ti)
{
    int total, uniq;
    mgr->query(
	all_faces(ti) >>= count(total) >>= unique() >>= count(uniq) >>= end()
	);

    return total-uniq;
}


int splatter::count_unneeded(part_mgr* mgr, temp_index& ti)
{
    int total, need;
    mgr->query(
	all_faces(ti) >>= count(total) >>= where(needs())
	>>= count(need) >>= end()
	);

    return total-need;
}
