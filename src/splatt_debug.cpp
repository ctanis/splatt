//  -*- Mode: C++; -*-
#include <splatter.h>
#include "splatt_debug.h"
#include "splatt_query.h"

void splatter::validate_phantom_layer(part_mgr& mgr, std::string index_name)
{
    LOG("validating phantom layer of " << index_name);
    temp_index local_phantom(&mgr, topo::TET, true);
    temp_index remote_phantom(&mgr, topo::TET, true);

    //deliver_args phantom_tet_ids(mgr.pctx());
    deliver_args phantom_tet_ids(mgr.pctx());
    using namespace splatter::query;

    mgr.query(
	all_faces(index_name) >>= where(needs()) >>= unique() >>= split(
	    // don't save any for me..
	    sort(all_needers(true), face_id(), phantom_tet_ids),
	    where(phantom()) >>= save_temp(local_phantom) >>=end())
	);


    // int total_faces, local_faces;
    // mgr.query(
    //     all_faces("tets") >>= count(total_faces) >>=
    //     save_temp(local_phantom) >>= count(local_faces) >>= end()
    //     );
    // DEBUG("total_faces: " << total_faces << ", local_faces: " << local_faces);
    // static_cast<explicit_index*>(mgr.get_index("tets"))->validate_hash();
    // local_phantom.validate_hash();


    int suddenly_missing;
    mgr.query(
	all_faces(local_phantom) >>= where(negate(faces_in(&mgr, index_name))) >>=
	count(suddenly_missing) >>= end()
	);


    assert(suddenly_missing == 0);

    local_phantom.validate_hash();

    // for (int n=0; n<mgr.pctx().np(); n++)
    // {
    // 	LOG(phantom_tet_ids[n].size() << " for " << n);
    // 	LOG(phantom_tet_ids.in_sizes[n] << " from " << n);
    // }

    // explicit_index* tets = static_cast<explicit_index*>(mgr.get_index("tets"));
    // DEBUG("local_tets:");
    // for (int p=0; p<tets->size(); p++)
    // {
    //     for (int n=0; n<4; n++)
    //     {
    // 	std::cout << *(tets->nodes()+p*4+n) << " ";
    //     }
    //     std::cout << std::endl;
    // }


    // DEBUG("local_phantom:");
    // for (int p=0; p<local_phantom.size(); p++)
    // {
    //     for (int n=0; n<4; n++)
    //     {
    // 	std::cout << local_phantom[p][n] << " ";
    //     }
    //     std::cout << std::endl;
    // }


    DEBUG("delivering");
    phantom_tet_ids.prepare();
    mgr.get_index(index_name)->deliver(phantom_tet_ids, remote_phantom, false);
    DEBUG("done delivering");

    local_phantom.validate_hash();

	
	
    // DEBUG(mgr.get_ownerdb());

    // for (int f=0; f<remote_phantom.size(); f++)
    // {
    //     intset matches = remote_phantom.findall(remote_phantom.width(), remote_phantom[f]);

    //     if (matches.size() > 1)
    //     {
    // 	DEBUG("multiple face:");
    // 	for (unsigned int m=0; m<matches.size(); m++)
    // 	{
    // 	    for (int w=0; w<remote_phantom.width(); w++)
    // 	    {
    // 		std::cout << remote_phantom[matches[m]][w] << " ";
    // 	    }
    // 	    std::cout << std::endl;
    // 	}
		
    //     }

    //     // std::cout << f << ":";
    //     // for (int c=0; c<4; c++)
    //     // {
    //     // 	std::cout << remote_phantom[f][c] << " ";
    //     // }
    //     // std::cout << std::endl;
    // }

    // TRACE(123, "building local_phantom hash");
    // local_phantom.build_hash();



    // remote phantom can have duplicates
    // DEBUG("testing remote_phantom:");
    // remote_phantom.validate_hash();
    LOG("done");
	
	
    local_phantom.validate_hash();




    // DEBUG("remote_phantom:");
    // for (int p=0; p<remote_phantom.size(); p++)
    // {
    //     for (int n=0; n<4; n++)
    //     {
    // 	std::cout << remote_phantom[p][n] << " ";
    //     }
    //     std::cout << std::endl;
    // }



    int unknown_locally, unknown_globally;
    
    local_phantom.validate_hash();


    LOG("unknown locally:");
    mgr.query(all_faces(remote_phantom) >>= 
//		  where(negate(faces_in(&mgr, "tets")))
	      where(negate(faces_in(local_phantom)))
	      >>=dump() >>= count(unknown_locally) >>= end());

    LOG("unknown globally:");
    mgr.query(all_faces(local_phantom) >>=
	      where(negate(faces_in(remote_phantom)))
	      >>= dump() >>= count(unknown_globally) >>= end());
    


    // LOG("start local");
    // mgr.query(all_faces(local_phantom) >>= dump() >>= end());
    // LOG("end local/start phantom");
    // mgr.query(all_faces(remote_phantom) >>= dump() >>= end());
    // LOG("end phantom");

    
    LOG("validating local_phantom hash");
    local_phantom.validate_hash();
    // LOG("validating remote_phantom hash");
    // remote_phantom.validate_hash();

    LOG("phantom validation: " << unknown_locally << "/" << remote_phantom.size() << ", " << unknown_globally << "/" << local_phantom.size());
    assert(unknown_locally == 0);
    assert(unknown_globally == 0);

    LOG("done validating phantom layer");
    // END VALIDATE PHANTOM LAYER ------------------------------------
}
