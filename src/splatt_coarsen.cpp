//  -*- mode: c++; -*- 

#include <splatter.h>
#include <splatt_query.h>
#include <splatt_coarsen.h>

// add coarsening hooks and data for tets

using namespace splatter;
using namespace splatter::query;

// verify that a and b really are the same tet
bool tetmatch(const int* a, const int* b)
{
    picky_matcher pm(4,a);
    return  pm.match(b);
}

class actual_replacements : public query::query_op
{
public:
    actual_replacements(explicit_index& replacements) : replacements(replacements) {}

    SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
    {
        // get the faces in replacements that match nodes, and then only list
        // the ones that have original in the 'parent' position

        intset matches = replacements.findall(4, nodes);

        for (unsigned int m=0; m < matches.size(); m++)
        {
            if (tetmatch(&(replacements[matches[m]][4]), nodes))
            {
                o(&replacements, matches[m], topo::TET, 4, replacements[matches[m]]);
            }
        }
    }

private:
    explicit_index& replacements;
};


class actual_parent : public query::query_op
{
public:
    actual_parent(explicit_index& replacements) : replacements(replacements) {}

    SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
    {
        // get the faces in replacements that match nodes, and then only list
        // the ones that have original in the 'child' position

        intset matches = replacements.findall(4, nodes);

        for (unsigned int m=0; m < matches.size(); m++)
        {
            if (tetmatch(replacements[matches[m]], nodes))
            {
                o(&replacements, matches[m], topo::TET, 4, &(replacements[matches[m]][4]));
            }
        }
    }

private:
    explicit_index& replacements;
};


explicit_index& splatter::coarsen_candidates(coarsen_kernel* kernel)
{
    return *(kernel->origp); 
}

static void node_coarsen_hook(part_mgr* mgr, void* edgesp, void* udata)
{
    temp_index& edge_map=*((temp_index*)(edgesp));
    const std::vector<int>& nmap = edge_map.data(refine_id_tag, std::vector<int>());
    coarsen_kernel* kernel = (coarsen_kernel*) udata;

    for (int e=0; e<edge_map.size(); e++)
    {
        int x[3];
        x[0] = nmap[e];
        x[1] = edge_map[e][0];
        x[2] = edge_map[e][1];
        kernel->esplitp->add_face(x);
    }
}


static void tri_coarsen_hook(part_mgr* mgr, void* cbdata, void* udata)
{
    coarsen_kernel* kernel = (coarsen_kernel*) udata;
    refine_arg* arg = (refine_arg*) cbdata;

    // save original triangle
    kernel->orig_trip->add_face((*kernel->bnd_idx)[arg->oldent]);
}


static void tet_coarsen_hook(part_mgr* mgr, void* cbdata, void* udata)
{
    coarsen_kernel* kernel = (coarsen_kernel*) udata;
    refine_arg* arg = (refine_arg*) cbdata;

    explicit_index& originals = *(kernel->origp);
    explicit_index& replacements = *(kernel->replp);
    explicit_index& tet_idx = *(kernel->tet_idx);

#ifdef SPLATT_DEBUG
    std::vector<int>& orig_repl_count =
        originals.data(bk_repl_count, std::vector<int>());
#endif // SPLATT_DEBUG
    
    int numnew = arg->numnew;
    const int* newtets = arg->newent;
    const int* orignodes = tet_idx[arg->oldent];

    intset match = replacements.findall(4, orignodes);

    // update bookkeeping if this tet was the result of a previous refinement
    if (match)
    {
        // need to actually look at the matches, since the hash might find
        // something inaccuarate..

        bool found=false;
        int realmatch=-1;
        for (unsigned int m=0; m < match.size(); m++)
        {
            if (tetmatch(replacements[match[m]], orignodes))
            { 
                found = true;
                assert(realmatch==-1);
                realmatch=match[m];
            }
        }

        if (found)              // replacing a replacement
        {
            // remove this guy's parent from the coarsening candidates, temporarily
            intset ps = originals.findall(4, &(replacements[realmatch][4]));

            if (ps)
            {
                for (unsigned int i=0; i<ps.size(); i++)
                {
                    kernel->otl.push_back(ps[i]);
                }
            }
            else
            {
                // remove original even though we don't have it
                int p = originals.add_face(&(replacements[realmatch][4]));
                kernel->otl.push_back(p);

#ifdef SPLATT_DEBUG
                orig_repl_count[p]=-1; // should never be seen since we're deleting                
#endif // SPLATT_DEBUG
            }
        }
    }

    // store all replacements
    for (int n=0; n<numnew; n++)
    {
        int repnodes[8];
        const int* nnodes = tet_idx[newtets[n]];

        for (int i=0; i<4; i++)
        {
            repnodes[i] = nnodes[i];
        }

        for (int i=0; i<4; i++)
        {
            repnodes[i+4] = orignodes[i];
        }

        replacements.add_face(repnodes);
    }

    // store original for future coarsening

    int p = originals.add_face(orignodes);

    // save data associated with this tet, for when we restore it
    for (unsigned int proxy=0; proxy<kernel->tet_data_proxies.size(); proxy++)
    {
        kernel->tet_data_proxies[proxy][1]->store_raw(p,
                                                      arg->oldent,
                                                      kernel->tet_data_proxies[proxy][0]);
    }
    
#ifdef SPLATT_DEBUG
    orig_repl_count[p] = numnew;
#endif
}


coarsen_kernel* splatter::register_coarsening_handlers(part_mgr* mgr,
                                                       explicit_index* tet_idx,
                                                       explicit_index* bnd_idx)
{
    coarsen_kernel* rval = new coarsen_kernel(mgr);
    explicit_index* tmp;

    rval->mgr = mgr;
    

    // originals
    tmp = mgr->add_explicit_index(coarsen_idx, topo::TET,
                                  SPLATT_PHANTOM_DEPS | SPLATT_FAST_INDEX);
    rval->origp = tmp;

#ifdef SPLATT_DEBUG
    std::vector<int> _tmp_data;
    // SPLATT_STEAL_DATA -> proxy uses internal array for data
    tmp->attach_data(bk_repl_count, proxy(_tmp_data, 1, SPLATT_STEAL_DATA));
#endif // SPLATT_DEBUG

    // original triangles
    tmp = mgr->add_explicit_index(coarsen_tri_idx, topo::TRI, SPLATT_FAST_INDEX);
    rval->orig_trip = tmp;
    

    // replacements
    tmp = mgr->add_explicit_index_notype(repl_idx, 8,
                                         SPLATT_PHANTOM_DEPS | SPLATT_FAST_INDEX);
    rval->replp = tmp;
    
    // edgesplits
    tmp = mgr->add_explicit_index_notype(edgespl_idx, 3, SPLATT_FAST_INDEX);
    tmp->set_purge(false);
    rval->esplitp = tmp;

    // originals_pending
    tmp = mgr->add_explicit_index_notype(coarsen_pend_idx, topo::TET,
                                         SPLATT_PHANTOM_DEPS | SPLATT_FAST_INDEX);
    rval->orig_pendp = tmp;
    

#ifdef SPLATT_DEBUG
    tmp->attach_data(bk_repl_count, proxy(_tmp_data, 1, SPLATT_STEAL_DATA));
#endif // SPLATT_DEBUG

    rval->tet_idx = tet_idx;
    rval->bnd_idx = bnd_idx;
    

    mgr->register_hook(SPLATT_REFINE_HOOK, topo::NODE, hookify(node_coarsen_hook, rval));
    mgr->register_hook(SPLATT_REFINE_HOOK, topo::TRI, hookify(tri_coarsen_hook, rval));
    mgr->register_hook(SPLATT_REFINE_HOOK, topo::TET, hookify(tet_coarsen_hook, rval));
    return rval;
}


status splatter::do_coarsen(coarsen_kernel* kernel, std::deque<int>& ids)
{
    part_mgr& mgr = *(kernel->mgr);

    // bookkeeping cleaner uppers
    std::deque<int> repls_cleanup;
    std::deque<int> edgesplits_cleanup;

    // detected hanging nodes
    temp_index hanging_nodes = temp_index::no_type(&mgr, 3, false);

    std::deque<int> coarsen_candidates;
    std::deque<int> tets_to_delete;

    kernel->unused_nodes.clear();
    kernel->curr_edge_map.clear();
    kernel->new_ids.clear();

    temp_index& curr_edge_map = kernel->curr_edge_map;
    std::vector<int>& new_ids = kernel->new_ids;
    std::vector<int>& unused_nodes = kernel->unused_nodes;
    explicit_index* tet_idx = kernel->tet_idx;
    

#ifdef SPLATT_DEBUG
    int replamt;
    int tetamt;
#endif // SPLATT_DEBUG

    kernel->replp->sync_phantom_layer(true);
    kernel->orig_pendp->sync_phantom_layer(true);

    
    mgr.query(
        for_each(kernel->origp, ids.begin(), ids.end()) >>=

        split (

            /* check for hanging nodes */
            subfaces(/*triangles */) >>=

            subfaces(/*edges*/) >>=
            faces_in(kernel->esplitp) >>=
            save_ids(edgesplits_cleanup) >>=
            save_temp_uniq(hanging_nodes) >>= end()

            ,
            /* delete replacements */
            actual_replacements(*(kernel->replp)) >>=
            save_ids(repls_cleanup) >>=
#ifdef SPLATT_DEBUG
            count (replamt) >>=
#endif // SPLATT_DEBUG
            castnodes(topo::TET, 4) >>=
            insert_if_needed("tets") >>=
#ifdef SPLATT_DEBUG
            count (tetamt) >>=
#endif // SPLATT_DEBUG
            save_ids(tets_to_delete) >>=

            end()

            ,

            /* check for coarsening candidates */
            actual_parent(*(kernel->replp)) >>=
            faces_in(kernel->orig_pendp) >>=
            unique() >>=
            save_ids(coarsen_candidates) >>= end()
            )
        );

#ifdef SPLATT_DEBUG
    assert(replamt == tetamt);
#endif // SPLATT_DEBUG

    // delete little tets
    kernel->tet_idx->delete_faces(tets_to_delete.begin(), tets_to_delete.end());
        
    // put big tets back
    explicit_index& originals = *(kernel->origp);
    // add big tets
    for (std::deque<int>::iterator nit=ids.begin();
         nit!= ids.end(); nit++)
    {
        assert(! kernel->tet_idx->findall(4, originals[*nit]));
        int p =kernel->tet_idx->add_face(originals[*nit]);

        // copy data from originals back into tet index
        for (unsigned int proxy=0; proxy<kernel->tet_data_proxies.size(); proxy++)
        {
            kernel->tet_data_proxies[proxy][0]->store_raw(p,
                                                          *nit,
                                                          kernel->tet_data_proxies[proxy][1]);
        }

        // tet_ref_status[p]=TET_REF_UNTOUCHED;
        // TODO: other tet data??
    }
    kernel->tet_idx->sync_phantom_layer(true);
    
    LLOG(2, "deleting tris");
    // delete little tris
    std::deque<int> tri_list;
    mgr.query(for_each(kernel->bnd_idx) >>= // for each boundary
              where(negate(faces_in(kernel->tet_idx))) >>= // not in a tet
              unique() >>=
              save_ids(tri_list) >>= end());
    kernel->bnd_idx->delete_faces(tri_list.begin(), tri_list.end());
    tri_list.clear();
    
    LLOG(2, "adding tris");
    // add big tris
    mgr.query(for_each(kernel->orig_trip) >>=
              where (faces_in(kernel->tet_idx)) >>=
              unique() >>=
              save_ids(tri_list) >>= end());
    LLOG(2,"query done");
    for (unsigned int t=0; t<tri_list.size(); t++)
    {
        kernel->bnd_idx->add_face((*kernel->orig_trip)[tri_list[t]]);
    }
    kernel->orig_trip->delete_faces(tri_list.begin(), tri_list.end());
    kernel->bnd_idx->sync_phantom_layer(true);
    LLOG(2,"done with tris");
    


    // clean up bookkeeping stuff
    kernel->origp->delete_faces(ids.begin(),
                                ids.end());
    kernel->replp->delete_faces(repls_cleanup.begin(),
                                repls_cleanup.end());
    kernel->esplitp->delete_faces(edgesplits_cleanup.begin(),
                                  edgesplits_cleanup.end());

    
    curr_edge_map.attach_data("x", proxy(new_ids));
    const ownerdb& odb = mgr.get_ownerdb();
        
    // check for hanging nodes to pass to follow-up refine call
    for (int n=0; n<hanging_nodes.size(); n++)
    {
        int* parts = hanging_nodes[n];

        // new node is still present
        if (tet_idx->findall(1, &parts[0]))
        {
            // as is the refined edge...
            if (tet_idx->findall(2, &parts[1]))
            {
                int p = curr_edge_map.add_face(&parts[1]);
                new_ids[p] = parts[0];
            }
            
            kernel->esplitp->add_face(hanging_nodes[n]);
        }
        else
        {
            // delete from edgesplits
            if (odb.owns(parts[0]))
            {
                unused_nodes.push_back(parts[0]);
            }
        }
    }
    kernel->esplitp->sync_phantom_layer(true);
    curr_edge_map.detach_data("x");

    LLOG(0, "coarsened " << tets_to_delete.size() << " tets");
    LLOG(0, "reusing " << unused_nodes.size() << " nodes");

    std::deque<int> unpending;
    explicit_index& originals_pending = *(kernel->orig_pendp);

#ifdef SPLATT_DEBUG
    std::vector<int>& orig_repl_count = originals.data(bk_repl_count, std::vector<int>());
    std::vector<int>& orig_pend_repl_count =
        kernel->orig_pendp->data(bk_repl_count, std::vector<int>());
#endif // SPLATT_DEBUG


    for (unsigned int c=0; c<coarsen_candidates.size(); c++)
    {
        int o = coarsen_candidates[c];
        int mincount=0;
        int majcount=0;
        int subcount=0;
        mgr.query(one_face(*(kernel->orig_pendp), o) >>= 
                  actual_replacements(*(kernel->replp)) >>=

                  // only allow this when we own all the children...
                  count(mincount) >>= where(owns()) >>= count(majcount) >>=
                  actual_replacements(*(kernel->replp)) >>=
                  count(subcount) >>= end()
            );

        if (mincount == majcount && subcount == 0)
        {
            unpending.push_back(o);

            int p = kernel->origp->add_face(originals_pending[o]);

            // copy data from pending into orig
            for (unsigned int proxy=0; proxy<kernel->tet_data_proxies.size(); proxy++)
            {
                kernel->tet_data_proxies[proxy][1]->store_raw(p,
                                                              o,
                                                              kernel->tet_data_proxies[proxy][2]);
            }

#ifdef SPLATT_DEBUG
            orig_repl_count[p] = orig_pend_repl_count[o];            
#endif // SPLATT_DEBUG
        }
    }


    // tell owners about secondary coarsening candidates and have them check
        

    LLOG(0, "putting back " << unpending.size() << " coarsening candidates");
    kernel->orig_pendp->delete_faces(unpending.begin(), unpending.end());
    kernel->origp->sync_phantom_layer(true);


    return SPLATT_OK;
}



status splatter::coarsen_update(coarsen_kernel* kernel)
{
    std::deque<int>& orig_to_lock = kernel->otl;
    explicit_index& originals = *(kernel->origp);
    
    std::sort(orig_to_lock.begin(), orig_to_lock.end());
    orig_to_lock.erase(std::unique(orig_to_lock.begin(), orig_to_lock.end()),
                       orig_to_lock.end());


#ifdef SPLATT_DEBUG
    std::vector<int>& orig_repl_count = originals.data(bk_repl_count, std::vector<int>());
    std::vector<int>& orig_pend_repl_count =
        kernel->orig_pendp->data(bk_repl_count, std::vector<int>());
#endif

    // flush refinement changes
    kernel->esplitp->sync_phantom_layer(true);
    kernel->replp->sync_phantom_layer(true);
    kernel->orig_trip->sync_phantom_layer(true);

    

    for (unsigned int t=0; t<orig_to_lock.size(); t++)
    {

        int p=kernel->orig_pendp->add_face(originals[orig_to_lock[t]]);

#ifdef SPLATT_DEBUG
        orig_pend_repl_count[p] = orig_repl_count[orig_to_lock[t]];
#endif // SPLATT_DEBUG
            
        // copy from originals to orig_pending
        for (unsigned int proxy=0; proxy<kernel->tet_data_proxies.size(); proxy++)
        {
            kernel->tet_data_proxies[proxy][2]->store_raw(p,
                                                          orig_to_lock[t],
                                                          kernel->tet_data_proxies[proxy][1]);
        }



    }
    originals.delete_faces(orig_to_lock.begin(), orig_to_lock.end());
    kernel->origp->sync_phantom_layer(true);
    kernel->orig_pendp->sync_phantom_layer(true);

#ifdef SPLATT_DEBUG
    TRACE(4576, "sizes: " << kernel->origp->size() << " .. " << kernel->replp->size());
    for (int o=0; o<originals.size(); o++)
    {
        int repcount=0;
        int actcount=0;
        kernel->mgr->query(one_face(*(kernel->origp), o) >>=
                           actual_replacements(*(kernel->replp)) >>=
                           count(repcount) >>=
                           faces_in(kernel->tet_idx) >>=
                           count(actcount) >>= end() );
                    
        LOG(orig_repl_count[o] << "=?=" << repcount);
        assert(orig_repl_count[o] == repcount);
    }
#endif // SPLATT_DEBUG

    orig_to_lock.clear();
    return SPLATT_OK;
}


status splatter::coarsen_track_tet(coarsen_kernel* kernel, std::string data_name)
{
    // get data from tet_idx and clone proxy, attaching to originals and originals_pending
    array<data_proxy*,3> this_set;

    data_proxy* tet_data = kernel->tet_idx->dproxy(data_name);
    if (tet_data == NULL)
    {
        ERROR("tet index has no data tagged " << data_name);
    }
    this_set[0] = tet_data;

    data_proxy* clone = tet_data->type_copy();
    this_set[1] = clone;
    kernel->origp->attach_data(data_name, clone);

    clone = tet_data->type_copy();
    this_set[2] = clone;
    kernel->orig_pendp->attach_data(data_name, clone);

    kernel->tet_data_proxies.push_back(this_set);
    return SPLATT_OK;
}
