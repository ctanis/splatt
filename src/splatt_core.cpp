//  -*- Mode: C++; -*-
#include <splatter.h>
#include <parmetis.h>
#include <cassert>
using namespace splatter;

// CONSTRUCTOR
part_mgr::part_mgr(int num_ent, const entity_cfg* cfg, bool doinit) :
    _node_index(_odb), _initted(false), _run_renumber_hooks(true)
{
    load_topo_cfg(num_ent, cfg);
    

    // contents moved to explicit init() call to support integration
    // into existing codes
    if (doinit)
    {
	init(parallel_ctx(MPI_COMM_WORLD));
    }
 
}

// DESTRUCTOR
part_mgr::~part_mgr()
{
    
    for (unsigned int i=0; i<_indices.size(); i++)
    {
	delete _indices[i];	
	_indices[i]=NULL;
    }


    for (std::list<app_hook*>::iterator it = _renumber_hooks.begin();
	 it != _renumber_hooks.end(); it++)
    {
	delete *it;
	*it = NULL;
    }

    for (unsigned int v=0; v<_refine_hooks.size(); v++)
    {
	for (std::list<app_hook*>::iterator it = _refine_hooks[v].begin();
	     it != _refine_hooks[v].end(); it++)
	{
	    if (*it != NULL)
	    {
		delete *it;
		*it = NULL;
	    }
	}
    }
   
}


// part_mgr initialization goes here
status part_mgr::init(parallel_ctx pctx)
{
    _pctx = pctx;
    _initted = true;

    return SPLATT_OK;
}


// node stuff

// status part_mgr::divvy_global_node_count(int nglobal)
// {
//     _global_node_count = nglobal;


//     // make sure everyone agrees about what we're divvying up
//     MPI_Bcast(&_global_node_count, 1, MPI_INT, _pctx.root(), _pctx.comm());

//     int ok=1;
//     if (_global_node_count != nglobal)
//     {
// 	ok=0;
//     }

//     if (_pctx.reduce(ok, MPI_LAND) != 1)
//     {
// 	ERROR("mismatch in divvied global node count");
// 	return SPLATT_FAIL;
//     }
    
	
//     const int np = _pctx.np();
//     const int rank = _pctx.rank();
    

//     int nodes_per = _global_node_count / np;
//     int extra_cutoff = _global_node_count % np;


//     int node_low, node_high;

//     // parmetis style vector for finding part containing node
//     _node_dist.resize(np+1);

//     node_high = -1;
//     for (int p=0; p <= rank; p++)
//     {
// 	// calculate node bounds for part p
// 	node_low = node_high+1;
// 	node_high = node_low + nodes_per-1;

// 	// is this one of the parts that requires an extra node to
// 	// even out?
// 	if (p < extra_cutoff)
// 	{
// 	    node_high++;
// 	}
//     }

//     //gather local ranges
//     MPI_Allgather(&node_low, 1, MPI_INT,
// 		  &(_node_dist[0]), 1, MPI_INT,
// 		  _pctx.comm());

//     // gather global high
//     _node_dist[np]=node_high+1;
//     MPI_Bcast(&(_node_dist[np]), 1, MPI_INT,
// 	      np-1, _pctx.comm());

//     _local_node_count = _node_dist[rank+1] - _node_dist[rank];

//     return SPLATT_OK;
// }



// figure out global node stats assuming nlocal distinct nodes per
// rank
status part_mgr::build_global_node_count(int nlocal)
{
    int np = _pctx.np();
    std::vector<int> node_dist(np+1);
    std::vector<int> temp(np);
    
    LLOG(1, "building global node count: " << nlocal);
    
    MPI_Allgather(&nlocal, 1, MPI_INT,
		  &temp[0], 1, MPI_INT,
		  _pctx.comm());

    node_dist[0]=0;
    for (int r=1; r<=np; r++)
    {
	node_dist[r] = node_dist[r-1] + temp[r-1];
    }
    _odb.init(_pctx, node_dist);

    LLOG(2, "my node range: " << _odb);
    return SPLATT_OK;
}

// status part_mgr::config_odb(int low, int high)
// {
//     build_global_node_count(high-low);
//     assert(_odb.low() == low);
//     assert(_odb.high() == high);
//     return SPLATT_OK;
// }


status part_mgr::load_topo_cfg(int num, const entity_cfg* cfg)
{
//     int id=0;
//     while (cfg->name.length() > 0)
//     {
// //	DEBUG("adding entity type " << id);
// 	if (_entity_adj.add_type(id, *cfg) == SPLATT_FAIL)
// 	{
// 	    ERROR("problem with topology configuration");
// 	    return SPLATT_FAIL;
// 	}
	
// 	id++;
// 	cfg++;
//     }

//     DEBUG("finalizing topological config");
//     return _entity_adj.finalize();

    _num_entities = num;
    _refine_hooks.resize(_num_entities);
    _entities = cfg;
    return SPLATT_OK;
}


status part_mgr::track(temp_index* tidx)
{
    _tracked_indices.insert(tidx);
    return SPLATT_OK;
}





status part_mgr::untrack(temp_index* tidx)
{
    if (_tracked_indices.erase(tidx))
    {
	return SPLATT_OK;
    }
    else
    {
	return SPLATT_FAIL;
    }
}



splatter::explicit_index* part_mgr::get_index(std::string name)
{
    for (unsigned int i=0; i<_indices.size(); i++)
    {
	if (_indices[i]->name() == name)
	{
	    return _indices[i];
	}
    }

    //ERROR("part_mgr::get_index -- could not find index named " << name);
    LOG("part_mgr::get_index -- could not find index named " << name);
    return NULL;
}


// splatter::index* part_mgr::add_implicit_index(std::string name,
// 					      int size,
// 					      int node_type,
// 					      int flags)
// {
//     implicit_index* newimp;

//     newimp = new implicit_index(this, name, node_type, size, flags);

//     // newimp->_low    = _node_dist[_pctx.rank()];
//     // newimp->_nlocal  = _node_dist[_pctx.rank()+1]-newimp->_low;

//     DEBUG("index " << name << " added to position " << _indices.size());
//     _indices.push_back(newimp);


//     if (flags & SPLATT_NODE_MASTER)
//     {
// 	// set up node_dist for mgr

// 	if (_node_dist.size() > 0)
// 	{
// 	    ERROR("SPLATT_NODE_MASTER already specified");
// 	    return NULL;
// 	}

// 	build_global_node_count(newimp->_nlocal);
// 	newimp->_low = _node_dist[_pctx.rank()];
//     }


//     return newimp;
// }

splatter::implicit_index* part_mgr::config_node_index(int size, int node_type)
{
//     _node_index = new implicit_index(_odb, "nodes", node_type, flags);
    _node_index._etype = node_type;

    build_global_node_count(size);

    for (unsigned int d=0; d<_node_index._data.size(); d++)
    {
	_node_index._data[d]->resize(size);
    }


    LLOG(2, "node index configured");
    return &_node_index;    
}




splatter::explicit_index* part_mgr::add_explicit_index(std::string name,
						       int entity_type,
						       std::vector<int>& nodes,
						       int flags)
{
    explicit_index* newexp = new explicit_index(_odb,
						name,
						_entities[entity_type].nnodes,
						entity_type,
						nodes,
						flags);

    _indices.push_back(newexp);
    LLOG(1, "registered explicit_index: " << name);

    return newexp;
}


splatter::explicit_index* part_mgr::add_explicit_index(std::string name, int entity_type,
						       int flags)
{
    explicit_index* newxp = new explicit_index(_odb, name, _entities[entity_type].nnodes,
					       entity_type, flags);
    _indices.push_back(newxp);
    LLOG(1, "registered explicit index: " << name);
    return newxp;
}

splatter::explicit_index* part_mgr::add_explicit_index_notype(std::string name,
                                                              int width, int flags)
{
    explicit_index* newxp = new explicit_index(_odb, name, width, -1, flags);
    _indices.push_back(newxp);
    LLOG(1, "registered notype explicit index: " << name);
    return newxp;
}


// this is for calling immediately after loading -- we can eliminate
// this with more selective file reading
status part_mgr::finalize_load()
{
    //assumes that _odb (to which all indices refer) is set up properly now --
    //aka, build_node_count() has been added

    LLOG(0, _odb);

    for (unsigned int i=0; i<_indices.size(); i++)
    {
	// don't bother clobber duplicates at this point
	if (_indices[i]->sync_phantom_layer(false) == SPLATT_FAIL)
	{
	    ERROR("initial_distribution failed for index " << i);
	    return SPLATT_FAIL;
	}
    }

    return SPLATT_OK;
}


// // augment implicit indices with SPLATT_AUTO_AUGMENT set
// status part_mgr::update_index_deps()
// {
//     for (unsigned int i=0; i<_indices.size(); i++)
//     {
// 	if ((_indices[i]->type() == SPLATT_IMPLICIT) &&
// 	    (_indices[i]->_flags & SPLATT_AUTO_AUGMENT))
// 	{
// 	    implicit_index* idx = static_cast<implicit_index*>(_indices[i]);
// 	    idx->add_globals(_globals);
// 	}
//     }

//     DEBUG("global dependencies registered");
//     return SPLATT_OK;
// }



/*
  IMPLICIT: send to new owner/recv from old owner
  RA: same as IMPLICIT. redo add_global_deps at the end
  EXPLICIT: straight up renumber/migrate
  HYBRID: iterate over members
  CSR:ugh
*/
status part_mgr::renumber(const std::vector<int>& newgids,
			  std::list<splatter::temp_index*> externals)
{
    return renumber(newgids, _odb._node_dist, externals);
}

status part_mgr::renumber(const std::vector<int>& newgids,
			  const std::vector<int>& newdist,
			  std::list<splatter::temp_index*> externals)
{
    LLOG(2, "renumbering all indices");

    // this won't work if there is junk at the end of implicit data
    assert(_odb.nphantom() == 0);
    if (_odb.nphantom() != 0)
    {
	ERROR("can only be called with no phantom data");
	return SPLATT_FAIL;
    }

    std::map<int,int> _last_renum_map;
//    _last_renum_map.clear();
    

#ifdef SPLATT_DEBUG
    for (unsigned int n=0; n<newdist.size(); n++)
    {
	DEBUG("newdist["<<n<<"]="<<newdist[n]);
    }
#endif // SPLATT_DEBUG

    // renumber explicit indices
    ownerdb newodb = make_ownerdb(newdist);
    DEBUG("old odb: " << _odb);
    DEBUG("new odb: " << newodb);

    for (unsigned int idx=0; idx < _indices.size(); idx++)
    {
	LLOG(2, "renumbering " << _indices[idx]->name());
	if (_indices[idx]->renumber(newodb, newgids, _last_renum_map) != SPLATT_OK)
	{
	    ERROR("renumber of " << _indices[idx]->name() << " failed");
	    return SPLATT_FAIL;
	}
    }

    TRACE(876, "finished renumbering explicits");

    // renumber tracked indices
    for (std::set<temp_index*>::iterator it = _tracked_indices.begin();
	 it != _tracked_indices.end();
	 it++)
    {
	if ((*it)->renumber(newodb, newgids, _last_renum_map) != SPLATT_OK)
	{
	    ERROR("renumber of tracked index failed");
	    return SPLATT_FAIL;
	}
    }

    TRACE(876, "finished renumbering tracked");
    
   
    // renumber temps
    // renumber "external" lists -- such as the temp_index used in queries
    for (std::list<splatter::temp_index*>::iterator it = externals.begin();
	 it != externals.end();
	 it++)
    {
	LLOG(2, "renumbering a temp_index");
	(*it)->renumber(newodb,newgids, _last_renum_map);
    }

    TRACE(876, "finished renumbering externals");


    // renumber nodes
    LLOG(2, "renumbering the node index");
    if (_node_index.renumber(newodb,newgids, _last_renum_map) != SPLATT_OK)
    {
        ERROR("renumber of nodes failed");
        return SPLATT_FAIL;
    }
    TRACE(876, "finished renumbering nodes");
    
 
    // make newodb the official odb
   _odb = newodb;

    
    if (_run_renumber_hooks)
    {

	for (std::list<app_hook*>::iterator it = _renumber_hooks.begin();
	     it != _renumber_hooks.end(); it++)
	{
	    LLOG(1, "running renumber hook");
	    (*it)->run(this, NULL);
	}
    }
    
    TRACE(876, "finished renumber hooks");
    
    
    return SPLATT_OK;
}


void part_mgr::call_hooks()
{
    for (std::list<app_hook*>::iterator it = _renumber_hooks.begin();
	 it != _renumber_hooks.end(); it++)
    {
	LLOG(1, "running renumber hook");
	(*it)->run(this, NULL);
    }
}



status part_mgr::build_fast_indices()
{
    LLOG(2, "building search hash for all indices");
    for (unsigned int idx=0; idx < _indices.size(); idx++)
    {
	if (_indices[idx]->flags() & SPLATT_FAST_INDEX)
	{
	    if (_indices[idx]->build_hash() == SPLATT_FAIL)
	    {
		ERROR("failed");
		return SPLATT_FAIL;
	    }
	}
    }
    
    return SPLATT_OK;
}



class GlobalExtractor
{
public:
    GlobalExtractor(const ownerdb& odb, std::map<int,int>& globals) :
	_globals(globals), _odb(odb)
    {
    }

    void operator()(splatter::index* idx, int id, int etype,
		    int width, int* nodes) const
    {
	for (int n=0; n<width; n++)
	{
            if (! _odb.owns(nodes[n]))
            {
                std::map<int,int>::iterator match = _globals.find(nodes[n]);

                if (match == _globals.end()) // new phantoms get -1
                {
                    _globals[nodes[n]]=-1;
                }
            }
	}
    }
    
private:
    std::map<int,int>&	_globals;
    const ownerdb&	_odb;
};



status part_mgr::build_phantom_map(std::list<temp_index*> extras)
{
    LLOG(2, "building phantom map");
    clear_phantom_data();
    
    GlobalExtractor ge(_odb, _odb._glob2loc);

    // officially attached indices
    for (unsigned int i=0; i<_indices.size(); i++)
    {
	if (_indices[i]->flags() & SPLATT_PHANTOM_DEPS)
	{
	    _indices[i]->apply(ge);
	}
    }

    // temporarily tracked indices
    for (std::set<temp_index*>::iterator it = _tracked_indices.begin();
	 it != _tracked_indices.end();
	 it++)
    {
	(*it)->apply(ge);
    }

    // explicitly passed indices
    for (std::list<temp_index*>::iterator it = extras.begin();
	 it != extras.end(); it++)
    {
	(*it)->apply(ge);
    }

    _odb._loc2glob.resize(_odb._glob2loc.size());
    std::map<int,int>::iterator gi=_odb._glob2loc.begin();
    
    int p=0;
    for (; gi != _odb._glob2loc.end(); gi++)
    {
	_odb._loc2glob[p] = gi->first;
	gi->second = p;
	p++;
    }

    return SPLATT_OK;
}



status part_mgr::build_node_sync_map()
{
    LLOG(2, "building node sync map");
    std::map<int,int>::iterator gmap;
    _sync_args = sync_args(_pctx.np());

    // auto-sorted!
    int curs=0;
    int last_owner=-1;
    for (gmap = _odb._glob2loc.begin(); gmap != _odb._glob2loc.end(); gmap++)
    {
	int owner = _odb.owner(gmap->first);

	if (owner > last_owner)	// we may have skipped some ranks
	{
	    for (int i=last_owner+1; i<=owner; i++)
	    {
		// displacement for rank i -- starting at end of local data
		// update when we get to new rank's data
		_sync_args.in_displs[i]=curs;
	    }
	}
	last_owner = owner;
	gmap->second = curs++;	// store position of this gid
    }


    // set remaining input displacements
    for (unsigned int i=last_owner+1; i<_sync_args.in_displs.size(); i++)
    {
	_sync_args.in_displs[i]=curs;
    }
    

    // TELL EVERYONE ELSE WHAT WE NEED

    // build sizes out of displacements
    for (int r=0; r<_pctx.np(); r++)
    {
	_sync_args.in_sizes[r] = _sync_args.in_displs[r+1]-_sync_args.in_displs[r];
    }

    TRACE(876, "gonna barrier");
    MPI_Barrier(MPI_COMM_WORLD);

    // ALSO FIGURE OUT WHAT EVERYONE ELSE NEEDS FROM ME
    MPI_Alltoall(&(_sync_args.in_sizes[0]), 1, MPI_INT,
		 &(_sync_args.out_sizes[0]), 1, MPI_INT,
		 _pctx.comm());

#if 2 <= SPLATT_LOG_LEVEL
    LOG("phantom breakdown. num phantoms =" << _odb.nphantom() );
    for (int r=0; r<_pctx.np(); r++)
    {
        LOG("rank " << r << " -- in " << _sync_args.in_sizes[r] << "; out " << _sync_args.out_sizes[r]);
    }
#endif


    // now, outamts is size of buffer for each outgoing rank

    int total_out_size=0;
    for (int r=0; r<_pctx.np(); r++)
    {
	_sync_args.out_displs[r]=total_out_size;
	total_out_size += _sync_args.out_sizes[r];
    }

    _sync_args.out_ids.resize(total_out_size);

    DEBUG("odb.loc2glob.size() " << _odb._loc2glob.size());
    int fake[]=	{0};


    // it's weird that some processors end up here w/ no phantom component...
    
    // read actual index needs from each remote rank
    MPI_Alltoallv(//&(_odb._loc2glob[0]),	// send my global id needs
	(_odb._loc2glob.size() == 0 ? fake : &(_odb._loc2glob[0])),
	&(_sync_args.in_sizes[0]),		// sizes of global id needs per rank
	&(_sync_args.in_displs[0]),	// displacement of global id needs per rank
	MPI_INT,
//	&(_sync_args.out_ids[0]),	// read remote global id needs
	(_sync_args.out_ids.size() == 0 ? fake : &(_sync_args.out_ids[0])),
	&(_sync_args.out_sizes[0]),	// number of values for each rank
	&(_sync_args.out_displs[0]),	// displacement of ranks in _outgoing
	MPI_INT,
	_pctx.comm());

    // convert outids to local numbering
    for (unsigned int i=0; i<_sync_args.out_ids.size(); i++)
    {
	_sync_args.out_ids[i] -= _odb.low();
    }


    // rewrite in_displs to account for local nodes
    for (unsigned int n=0; n<_sync_args.in_displs.size(); n++)
    {
	_sync_args.in_displs[n] += _odb.nlocal();
    }
    
    return SPLATT_OK;
}


status part_mgr::sync_all_node_data()
{
    LLOG(2, "syncing all node data");
    return _node_index.sync(_pctx, _sync_args);
}

status part_mgr::sync_node_data(std::string name)
{
    return _node_index.sync_one(_pctx, _sync_args, name);
}



status part_mgr::get_more_nodes(int amt, int* newstart,
				std::list<splatter::temp_index*> externals)
{
    LLOG(1, "requesting "<<amt<<"more nodes");
    bool augment=false;

    // if we are starting with phantom data, let's get more at the end.
    if (_pctx.reduce(_odb.nphantom(), MPI_SUM) > 0)
    {
        // assert(_odb.nphantom() > 0);
        TRACE(876, "somehow my nphantom is 0");
        
        
	augment=true;
	clear_phantom_data();
    }


    *newstart = _odb.nlocal();	// start of newly allocated

    // figure out how many nodes everybody wants, to calculate a new node_dist
    std::vector<int> sizes(_pctx.np());
    std::vector<int> new_dist(_pctx.np()+1);

    MPI_Allgather(&amt, 1, MPI_INT, &sizes[0], 1, MPI_INT, _pctx.comm());

    new_dist[0]=0;
    int shift=0;
    for (unsigned int r=1; r<new_dist.size(); r++)
    {
	shift += sizes[r-1];
	new_dist[r] = _odb._node_dist[r] + shift;
    }


    // int new_local_size = new_dist[_odb.me()+1] - new_dist[_odb.me()];
    // _node_index->revert_to_local(new_local_size);
    

    // renumber
    std::vector<int> newgids(_odb.nlocal());
    for (int n=0; n<_odb.nlocal(); n++)
    {
	newgids[n] = n+new_dist[_odb.me()];
    }

    renumber(newgids, new_dist, externals);
    DEBUG("asserting that " << _node_index.size() << " == " << _odb.nlocal());
    assert(_node_index.size() == _odb.nlocal());
    

    if (augment)
    {
	augment_nodes(externals);
    }
    

    return SPLATT_OK;
}


status part_mgr::clear_phantom_data()
{
    LLOG(1, "clearing phantom data");
    _odb._glob2loc.clear();
    _odb._loc2glob.clear();
    // TODO: do this for all implicit_indices??
    _node_index.revert_to_local(_odb.nlocal());

    return SPLATT_OK;
}


status part_mgr::augment_nodes(std::list<temp_index*> extras)
{
    LLOG(1, "augmenting nodes");
    build_phantom_map(extras);	// make node data syncable
    build_node_sync_map();
    return sync_all_node_data();
}


status part_mgr::set_query_data(std::string name, std::deque<int>* datap)
{
    query_data[name]=datap;

    return SPLATT_OK;
}


std::deque<int>* part_mgr::get_query_data(std::string name)
{
    std::map<std::string, std::deque<int>* >::iterator it = query_data.find(name);

    if (it == query_data.end())
    {
	ERROR("unknown query data: " << name);
	return NULL;
    }

    else return it->second;
}

status part_mgr::register_hook(hook_entry point, int etype, app_hook* hook)
{
    if (point == SPLATT_RENUMBER_HOOK) // etype is ignroed
    {
	_renumber_hooks.push_back(hook);
    }
    else if (point == SPLATT_REFINE_HOOK)
    {
	if (etype >=0 && etype < _num_entities)
	{
	    _refine_hooks[etype].push_back(hook);	    
	}
	else
	{
	    return SPLATT_FAIL;
	}

    }
    else
    {
	ERROR("unknown hook point: " << point);
	return SPLATT_FAIL;
    }
    return SPLATT_OK;
}



void part_mgr::dump()
{
    LOG("part_mgr dump");
    //LOG(_odb);
    LOG(_odb.me() << " of " << _odb.np());
    LOG("nlocal: " << _odb.nlocal() << "; nlocal_p: " << _odb.nlocal_p() << "; nglobal: " << _odb.nglobal());
    LOG(_odb);
    

    LOG("node data:");
    for (unsigned int idx=0; idx < _node_index._data.size(); idx++)
    {
	LOG("-- " << _node_index._data_names[idx] << ": " << _node_index._data[idx]->dump());
    }
    

    for (unsigned int idx=0; idx<_indices.size(); idx++)
    {
	LOG("index " << _indices[idx]->name() << ";("<< _entities[_indices[idx]->etype() ].name <<") " << _indices[idx]->size() << "/" << _pctx.reduce(_indices[idx]->size(), MPI_SUM));
	for (unsigned int d=0; d< _indices[idx]->_data.size(); d++)
	{
	    LOG("-- attached data " << _indices[idx]->_data_names[d] << ": " << _indices[idx]->_data[d]->dump());
	}
    }
    LOG("tracking " << _tracked_indices.size() << " indices");
    LOG("number of renumber hooks: " << _renumber_hooks.size() << " -- " <<
	(_run_renumber_hooks ? "enabled" : "inhibited"));
}



// ---------------------------------------------------------------------------
// EXPERIMENTAL remove_unused_nodes & related code
// experimental Sun Sep  1 10:16:39 2013


status part_mgr::remove_unused_nodes(std::list<temp_index*> externals)
{
    bool augment=false;

    // if we are starting with phantom data, let's get more at the end.
    if (_pctx.reduce(_odb.nphantom(), MPI_SUM) > 0)
    {
        // assert(_odb.nphantom() > 0);
        TRACE(876, "somehow my nphantom is 0");
        
        
	augment=true;
	clear_phantom_data();
    }



    // identify unused nodes
    std::vector<int> new_glob(_odb.nlocal());
    std::vector<int> node_dist(_pctx.np()+1);
    int used_count=0;
    
    clear_phantom_data();
    for (int n=0; n<_odb.nlocal(); n++)
    {
        bool foundit=false;
        int g = _odb.l2g(n);
        
        // check default explicits
        for (unsigned int i=0; i<_indices.size() && !foundit; i++)
        {
            if (_indices[i]->findall(1, &g))
            {
                foundit=true;
            }
        }

        // check tracked if needed
        for (std::set<temp_index*>::iterator it = _tracked_indices.begin();
             !foundit && it != _tracked_indices.end();
             it++)
        {
            if ((*it)->findall(1, &g))
            {
                foundit=true;
            }
        }
        
        // check externals if needed
        for (std::list<temp_index*>::iterator it = externals.begin();
             !foundit && it != externals.end(); it++)
        {
            if ((*it)->findall(1, &g))
            {
                foundit=true;
            }
        }

        if (foundit)
        {
            new_glob[n] = used_count++;
        }
        else
        {
            new_glob[n] = -1;
        }
    }

    if (_pctx.reduce(_odb.nlocal()-used_count, MPI_SUM) == 0)
    {
        LLOG(1,"no nodes to remove");
        return SPLATT_OK;
    }

    LLOG(1, "removing " << (_odb.nlocal()-used_count) << " of " << _odb.nlocal());
    

    // "build_count" using used_count
    {
        int np = _pctx.np();
        int me = _pctx.rank();
        std::vector<int> temp(np);

        MPI_Allgather(&used_count, 1, MPI_INT,
                      &temp[0], 1, MPI_INT,
                      _pctx.comm());

        node_dist[0]=0;
        for (int r=1; r<=np; r++)
        {
            node_dist[r] = node_dist[r-1] + temp[r-1];
        }
        int start = node_dist[me];

        for (int n=0; n<_odb.nlocal(); n++)
        {
            if (new_glob[n] != -1)
            {
                new_glob[n] += start;
            }
        }

        // newglob and new_dist contain the appropriate info for renumber
    }

    if (renumber(new_glob, node_dist, externals) == SPLATT_FAIL)
    {
        ERROR("renumber failed!");
        return SPLATT_FAIL;
    }

    if (augment)
    {
        augment_nodes(externals);
    }

    return SPLATT_OK;
}
