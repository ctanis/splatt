//  -*- Mode: C++; -*-
#include <splatter.h>
#include <parmetis.h>
#include <cassert>
#include <fstream>
using namespace splatter;


// this is specifically for building a parmetis adjidx where nodes CANNOT
// connect to themselves, and it assumes we have no relevant phantom stuff
// (see splatt_solver.hpp)
class CSRBuilder
{
public:
    CSRBuilder(const ownerdb& odb, std::vector<intset>& hash) :
	_hash(hash), _odb(odb)
    {
	_hash.resize(_odb.nlocal());
    }


    // remember dependency between nodes
    void operator()(splatter::index* idx, int item, int etype,
		    int width, int* nodes) const
    {
	for (int n1=0; n1<width; n1++)
	{
	    if (_odb.owns(nodes[n1]))
	    {
		int l = _odb.g2l(nodes[n1]);

		for (int n2=0; n2<width; n2++)
		{
		    if (n1 != n2)
		    {
			_hash[l].insert(nodes[n2]);
		    }
		}
	    }
	    
	}
    }


    void getCSR(std::vector<int>& ia, std::vector<int>& ja)
    {

	// preallocate ia,ja
	ia.reserve(_hash.size()+1);
	// std::map<int, std::set<int> >::iterator ss;

	// int total=0;
	// for (ss=_hash.begin(); ss != _hash.end(); ss++)
	// {
	//     total += ss->second.size();
	// }
	int total=0;
	for (unsigned int n=0; n<_hash.size(); n++)
	{
	    total += _hash[n].size();
	}
	ja.reserve(total);
	


	for (unsigned int n=0; n<_hash.size(); n++)
	{
	    // std::set<int>::iterator nbrs = (_hash _AT(n).begin());
	    // std::set<int>::iterator nbrs = _hash[n].begin();

	    ia.push_back(ja.size());
	    //while (nbrs != _hash[n].end())
	    for (unsigned int v=0; v<_hash[n].size(); v++)
	    {
		// ja.push_back(*nbrs);
		// nbrs++;
		ja.push_back(_hash[n][v]);
	    }
	}
	ia.push_back(ja.size());
	_hash.clear();		// i should not need this
    }
    
    

private:
    //std::map<int, std::set<int> > _hash; // TODO replace this with a vector
    //std::vector<std::set<int> > _hash;
    std::vector<uniqvec<int> >& _hash;
    const ownerdb& _odb;
};



// decomp the mesh based on adjacencies in index[index_name]
// caution: clear phantom data prior to calling this!
status decomp_int(MPI_Comm comm, std::vector<int>& ia,
                  std::vector<int>& ja,
                  int nparts, int* edgecutp, ownerdb odb,
                  std::vector<int>& new_gids,
                  std::vector<int>& new_node_dist, bool adapt, int flag)
{
    LLOG(0, (adapt ? " adapting" : "full decomp"));

    int rank, np;
    
    MPI_Comm_size(comm, &np);
    MPI_Comm_rank(comm, &rank);

//     {
// 	std::vector<intset> hash;

// //	DEBUG("my low/high: " << odb.low() << "," << odb.high());
// 	CSRBuilder csrb(odb, hash);
// //	DEBUG("before CSRBuilderapply:"); LOG_MEM;
        
//         idx->apply(csrb);

// //	DEBUG("after CSRBuilderapply:"); LOG_MEM;
// 	csrb.getCSR(ia,ja);
//     }
//     // ia,ja has the parallel topology to decomp
//     LLOG(1, "build CSR for parmetis: "<< ia.size() << ";" << ja.size());
// //    LOG_MEM;
    

//    LLOG(2, "decomp/ia.size(): " << ia.size() << "; ia.capacity(): " << ia.capacity());
//    LLOG(2, "decomp/ja.size(): " << ja.size() << "; ja.capacity(): " << ja.capacity());

    // parmetis flags
    int zero=0;
    int options[3]={0,0,0};
    int one=1;
    real_t one05=1.02;

    std::vector<real_t> wghts(nparts);
    for (unsigned int i=0; i<wghts.size(); i++)
    {
	wghts[i] = 1.0/nparts;
    }


    // destination for new partition info
    std::vector<int> new_parts(odb.nlocal());
    //new_parts.resize(mgr.local_node_count());
    
    int* nd = const_cast<int*>(odb.node_dist());

    // for (int r=0; r<np+1; r++)
    // {
    // 	DEBUG("inner_node_dist["<<r<<"]="<<nd[r]);
    // }


    LLOG(1, "calling parmetis");
    timer parmetis_timer;

    if (adapt)
    {
	real_t itr=1000.0;	// recommended value

	ParMETIS_V3_AdaptiveRepart(
	    nd,
	    &ia[0],
	    &ja[0],
	    NULL,
	    NULL,
	    NULL,
	    &zero,
	    &zero,
	    &one,
	    &nparts,
	    &wghts[0],
	    &one05,
	    &itr,
	    options,
	    edgecutp,
	    &new_parts[0],
	    &comm
	    );
	
    }
    else
    {
	// full decomp
	ParMETIS_V3_PartKway(
	    nd,
	    &ia[0],
	    &ja[0],
	    NULL,
	    NULL,
	    &zero,
	    &zero,
	    &one,
	    &nparts,
	    &wghts[0],
	    &one05,
	    options,
	    edgecutp,
	    &new_parts[0],
	    &comm
	    );
    }
    

    TBL(flag, "*parmetis", parmetis_timer.time());
    LLOG(0, "TIME: parmetis call took " << parmetis_timer.time());

//    LOG_MEM;
    //
    


    // got new parts .. build renumber map
    std::vector<int> parcnts_l(nparts); // count of part nos locally
    std::vector<int> parcnts_g(nparts * np); // total part breakdown



    // count local partition breakdown
    for (unsigned int n=0; n<new_parts.size(); n++)
    {
	parcnts_l[new_parts[n]]++;
    }

    // get global breakdown of new parts
    MPI_Allgather(&parcnts_l[0], nparts, MPI_INT,
    		  &parcnts_g[0], nparts, MPI_INT, comm);
    



    std::vector<int> part_sizes(nparts);
    std::vector<int> part_offsets(nparts);

    int tally=0;
    for (int p=0; p<nparts; p++)
    {
	// for each rank
	for (int r=0; r<np; r++)
	{
	    // determine starting id for local nodes going to partition p
	    if (r == rank)
	    {
		part_offsets[p]=tally;
	    }

	    tally += parcnts_g[r*nparts + p];
	    part_sizes[p] += parcnts_g[r*nparts + p];
	}
    }

    new_node_dist.resize(nparts+1);

//    DEBUG("part_sizes / new node_dist:");

    int total=0;
    for (unsigned int n=0; n<part_sizes.size(); n++)
    {
//    	DEBUG(n << ":" << part_sizes[n] << ":" << total);
    	new_node_dist[n]=total;
    	total += part_sizes[n];
    }
    new_node_dist.back() = total;
//    DEBUG ("	:" << total);



    // now determine local numbering
    new_gids.resize(odb.nlocal());

    for (unsigned int n=0; n < new_gids.size(); n++)
    {
	new_gids[n] = part_offsets[new_parts[n]]++;
    }
    
    

// #ifdef SPLATT_DEBUG
//     for (unsigned int n=0; n<new_gids.size(); n++)
//     {
// 	if (! odb.owns(new_gids[n]))
// 	{
// 	    ERROR("invalid id after decomp: " << new_gids[n]);
// 	}
//     }
// #endif	// SPLATT_DEBUG


    // // renumber according to new_gids
    // DEBUG("renumbering");
    // mgr.renumber(new_gids);
    // DEBUG("decomp > renumber finished");


    // // TODO rebuild phantom dependencies & sync

    LLOG(1, "end of splatt decomp()");
//    LOG_MEM;
    

    return SPLATT_OK;
}


status splatter::decomp(MPI_Comm comm, splatter::index* idx, int nparts, int* edgecut,
                        ownerdb odb,
                        std::vector<int>& new_gids, std::vector<int>& new_dist,
                        bool adapt, int flag)
{
    std::vector<int> ia;
    std::vector<int> ja;
    

    {
	std::vector<intset> hash;

//	DEBUG("my low/high: " << odb.low() << "," << odb.high());
	CSRBuilder csrb(odb, hash);
//	DEBUG("before CSRBuilderapply:"); LOG_MEM;
        
        idx->apply(csrb);

//	DEBUG("after CSRBuilderapply:"); LOG_MEM;
	csrb.getCSR(ia,ja);
    }
    // ia,ja has the parallel topology to decomp
    LLOG(1, "build CSR for parmetis: "<< ia.size() << ";" << ja.size());
//    LOG_MEM;

    return decomp_int(comm, ia, ja, nparts, edgecut, odb,
                      new_gids, new_dist, adapt, flag);

}

status splatter::decomp(part_mgr& mgr, bool adapt, MPI_Comm comm, int nparts)
{
    int edgecut;
    std::vector<int> new_gids;
    std::vector<int> new_dist;

    bool has_phantom = mgr.get_ownerdb().nphantom() > 0;

    if (nparts <= 0)
    {
        nparts = mgr.pctx().np();
    }


    if (has_phantom)
    {
        mgr.clear_phantom_data();
    }


    std::vector<int> ia;
    std::vector<int> ja;
    std::vector<explicit_index*> indices = mgr.indices();

    {
        std::vector<intset> hash;
        CSRBuilder csrb(mgr.get_ownerdb(), hash);

        for (unsigned int i=0; i<indices.size(); i++)
        {
            indices[i]->apply(csrb);
        }

        csrb.getCSR(ia,ja);
    }

    if (decomp_int(comm, ia, ja, nparts, &edgecut, mgr.get_ownerdb(),
                   new_gids, new_dist, adapt, 0) != SPLATT_OK)
    {
        ERROR("decomp_int failed");
    }

    mgr.renumber(new_gids, new_dist);

    if (has_phantom)
    {
        mgr.augment_nodes();
    }

    return SPLATT_OK;
}



