//  -*- mode: c++; -*- 
#include <splatter.h>
#include <splatt_geom.h>

namespace splatter
{
    namespace geom
    {
	void tet_refine_single();
	void tet_refine_double();
	void tet_refine_face();
	void tet_refine_full();
    }
}


/*
  tet node ordering

	     3
  	    /\ .
	   /  \	 .
	  /    \   . 2
	 /    	\ .
        +--------+
       0       	  1

 */

	int edge[] = { nodes[0], nodes[1], -1,
		       nodes[0], nodes[2], -1,
		       nodes[0], nodes[3], -1,
		       nodes[1], nodes[2], -1,
		       nodes[1], nodes[3], -1,
		       nodes[2], nodes[3], -1};




// LEGAL ROTATIONS

ABCD -> BCAD
	CBAD
	ACDB
	BDCA



void splatter::geom::tet_refine_single()
{
    // find the edge

    


}


void splatter::geom::tet_refine_double()
{
    // find the edges
}


void splatter::geom::tet_refine_face()
{
    // find the face
}


void splatter::geom::tet_refine_full()
{
    // boom!
    // 5
    tmp_edge[0] = face_nodes[0];
    tmp_edge[1] = face_nodes[2];
    assert(refine_edges.find(tmp_edge) != -1);
    edge_nodes[0] = new_node_ids[ refine_edges.find(tmp_edge) ];

    //6
    tmp_edge[0] = face_nodes[0];
    tmp_edge[1] = face_nodes[1];
    assert(refine_edges.find(tmp_edge) != -1);
    edge_nodes[1] = new_node_ids[ refine_edges.find(tmp_edge) ];

    //7
    tmp_edge[0] = face_nodes[1];
    tmp_edge[1] = face_nodes[2];
    assert(refine_edges.find(tmp_edge) != -1);
    edge_nodes[2] = new_node_ids[ refine_edges.find(tmp_edge) ];

    //8
    tmp_edge[0] = face_nodes[0];
    tmp_edge[1] = face_nodes[3];
    assert(refine_edges.find(tmp_edge) != -1);
    edge_nodes[3] = new_node_ids[ refine_edges.find(tmp_edge) ];

    //9
    tmp_edge[0] = face_nodes[1];
    tmp_edge[1] = face_nodes[3];
    assert(refine_edges.find(tmp_edge) != -1);
    edge_nodes[4] = new_node_ids[ refine_edges.find(tmp_edge) ];
	
    //10
    tmp_edge[0] = face_nodes[2];
    tmp_edge[1] = face_nodes[3];
    assert(refine_edges.find(tmp_edge) != -1);
    edge_nodes[5] = new_node_ids[ refine_edges.find(tmp_edge) ];
	
    int scratch_tet[4];

    // tet 2 -- 3 5 7 10
    face_nodes = &tet_nodes[f*4];
    scratch_tet[0] = face_nodes[2];
    scratch_tet[1] = edge_nodes[0];
    scratch_tet[2] = edge_nodes[2];
    scratch_tet[3] = edge_nodes[5];

    // add it to tet_idx and also replacement_tets
    x = tet_idx->add_face(scratch_tet);
    vcond[x]=vcond[f];
    tet_ref_status[x]=TET_LOCKED;
    replacement_tets.add_face(scratch_tet);
    newfaces++;

    // tet 3 -- 2 7 6 9
    face_nodes = &tet_nodes[f*4];
    scratch_tet[0] = face_nodes[1];
    scratch_tet[1] = edge_nodes[2];
    scratch_tet[2] = edge_nodes[1];
    scratch_tet[3] = edge_nodes[4];

    // add it
    x = tet_idx->add_face(scratch_tet);
    vcond[x]=vcond[f];
    tet_ref_status[x]=TET_LOCKED;
    replacement_tets.add_face(scratch_tet);
    newfaces++;

    // tet 4 -- 8 9 10 4
    face_nodes = &tet_nodes[f*4];
    scratch_tet[0] = edge_nodes[3];
    scratch_tet[1] = edge_nodes[4];
    scratch_tet[2] = edge_nodes[5];
    scratch_tet[3] = face_nodes[3];

    // add it
    x=tet_idx->add_face(scratch_tet);
    vcond[x]=vcond[f];
    tet_ref_status[x]=TET_LOCKED;
    replacement_tets.add_face(scratch_tet);
    newfaces++;

    // tet 5 -- 7 5 9 10
    scratch_tet[0] = edge_nodes[2];
    scratch_tet[1] = edge_nodes[0];
    scratch_tet[2] = edge_nodes[4];
    scratch_tet[3] = edge_nodes[5];

    // add it
    x=tet_idx->add_face(scratch_tet);
    vcond[x]=vcond[f];
    tet_ref_status[x]=TET_LOCKED;
    replacement_tets.add_face(scratch_tet);
    newfaces++;

    // tet 6 -- 5 8 9 10
    scratch_tet[0] = edge_nodes[0];
    scratch_tet[1] = edge_nodes[3];
    scratch_tet[2] = edge_nodes[4];
    scratch_tet[3] = edge_nodes[5];
	    
    // add it
    x=tet_idx->add_face(scratch_tet);
    vcond[x]=vcond[f];
    tet_ref_status[x]=TET_LOCKED;
    replacement_tets.add_face(scratch_tet);
    newfaces++;

    // tet 7 -- 7 9 5 6
    scratch_tet[0] = edge_nodes[2];
    scratch_tet[1] = edge_nodes[4];
    scratch_tet[2] = edge_nodes[0];
    scratch_tet[3] = edge_nodes[1];

    // add it
    x=tet_idx->add_face(scratch_tet);
    vcond[x]=vcond[f];
    tet_ref_status[x]=TET_LOCKED;
    replacement_tets.add_face(scratch_tet);
    newfaces++;

    // tet 8 -- 5 9 8 6
    scratch_tet[0] = edge_nodes[0];
    scratch_tet[1] = edge_nodes[4];
    scratch_tet[2] = edge_nodes[3];
    scratch_tet[3] = edge_nodes[1];

    // add it
    x=tet_idx->add_face(scratch_tet);
    vcond[x]=vcond[f];
    tet_ref_status[x]=TET_LOCKED;
    replacement_tets.add_face(scratch_tet);
    newfaces++;

    // tet 1 -- 1 6 5 8
    face_nodes = &tet_nodes[f*4];
    scratch_tet[0] = face_nodes[0];
    scratch_tet[1] = edge_nodes[1];
    scratch_tet[2] = edge_nodes[0];
    scratch_tet[3] = edge_nodes[3];

    // add it	    
    x=tet_idx->add_face(scratch_tet);
    vcond[x] = vcond[f];
    tet_ref_status[x] = TET_LOCKED;
    replacement_tets.add_face(scratch_tet);
    newfaces++;
		
}


