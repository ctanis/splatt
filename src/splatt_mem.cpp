//  -*- Mode: C++; -*- 
// http://stackoverflow.com/questions/669438/how-to-get-memory-usage-at-run-time-in-c

#include <unistd.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <splatt_log.h>
#include <mpi.h>

namespace splatter
{
    void get_mem_usage(double* vm_usage, double* resident_set);

    void log_sys_mem();
    
};


//////////////////////////////////////////////////////////////////////////////
//
// process_mem_usage(double &, double &) - takes two doubles by reference,
// attempts to read the system-dependent data for a process' virtual memory
// size and resident set size, and return the results in KB.
//
// On failure, returns 0.0, 0.0

void splatter::get_mem_usage(double* vm_usage, double* resident_set)
{
    using std::ios_base;
    using std::ifstream;
    using std::string;

    *vm_usage     = 0.0;
    *resident_set = 0.0;

    // 'file' stat seems to give the most reliable results
    //
    ifstream stat_stream("/proc/self/stat",ios_base::in);

    if (! stat_stream.is_open())
    {
	ERROR("couldn't open /proc/self/stat for memory diag");
	return;
    }

    // dummy vars for leading entries in stat that we don't care about
    //
    string pid, comm, state, ppid, pgrp, session, tty_nr;
    string tpgid, flags, minflt, cminflt, majflt, cmajflt;
    string utime, stime, cutime, cstime, priority, nice;
    string O, itrealvalue, starttime;

    // the two fields we want
    //
    unsigned long vsize;
    long rss;

    stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
		>> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
		>> utime >> stime >> cutime >> cstime >> priority >> nice
		>> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

    stat_stream.close();

    long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
    *vm_usage     = vsize / 1024.0;
    *resident_set = rss * page_size_kb;
}

// int main()
// {
//     using std::cout;
//     using std::endl;

//     double vm, rss;
//     process_mem_usage(vm, rss);
//     cout << "VM: " << vm << "; RSS: " << rss << endl;
// }


#define LINESIZE 1024
void splatter::log_sys_mem()
{
    using std::ios_base;
    using std::ifstream;
    using std::string;

    ifstream mem_stream("/proc/meminfo", ios_base::in);

    if (! mem_stream.is_open())
    {
	ERROR("couldn't open /proc/meminfo for memory diag");
	return;
    }


    char line[LINESIZE];
    int lineno=1;
    
    
    // terrible hackfest
    while (mem_stream.getline(line, LINESIZE))
    {
	// if ( (! strncasecmp(line, "mem", 3 )) ||
	//      (! strncasecmp(line, "swap", 4 )) )
	{
	    LOG(line);
	}

	lineno++;
    }


    return;
}
