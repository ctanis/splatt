//  -*- Mode: C++; -*- 
#include <splatter.h>
#include <fstream>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <cmath>

using namespace splatter;


#define LINESIZE 1024
#define INDEX_OFFSET 1	// subtract one from all node ids in data file

/*! open file and figure out bounds for the current rank, expecting
 *  one line per data item */
static long int init_parallel_read(std::string filename,
				   std::ifstream& in,
				   parallel_ctx mpi)
{
    char linebuf[LINESIZE];

    struct stat statbuf;
    if (stat(filename.c_str(), &statbuf) != 0)
    {
	ERROR("error statting " << filename);
	return -1;
    }

    unsigned long int total_size = statbuf.st_size;
    unsigned long int chunk = total_size  / mpi.np();

    LLOG(1, filename << " file size: " << total_size << "; my piece: " << chunk);
    
    in.open(filename.c_str());
    if (! in.is_open() )
    {
	ERROR("could not open " << filename);
	return -1;
    }
    LLOG(1, "successfully opened " << filename);


    // everybody jump to their rank*chunk position and slurp to end of
    // line, then we Allgather byte positions
    in.seekg(mpi.rank()*chunk);

    if (mpi.rank() > 0)
    {
	// slurp to the end
	in.getline(linebuf, LINESIZE);
    }

    long int currpos = in.tellg();

    // currpos < 0 when file is empty?
    if (currpos < 0)
    {
	currpos=0;
    }
    std::vector<long int> starting_points(mpi.np());

    LLOG(2, "my starting point: " << currpos);

    MPI_Allgather(&currpos, 1, MPI_LONG,
		  &(starting_points[0]), 1, MPI_UNSIGNED_LONG, mpi.comm());

    long int stop_at=0;
    if (mpi.rank() < mpi.np()-1)
    {
	stop_at = starting_points[mpi.rank()+1];
    }
    else
    {
	stop_at = total_size;
    }

    LLOG(2, "bytes for me: " << currpos << " to " << stop_at);
    return stop_at;
}


bool try_memguide(std::string filename, int& vc, int& tc, int& bc)
{
    std::ifstream in;

    in.open(filename.c_str());
    if (in.is_open())
    {
	in >> vc >> tc >> bc;	
	in.close();
	return true;
    }

    return false;
}




status starcd::load_tets(std::string filename, parallel_ctx pctx,
			 std::vector<int>& faces,
			 std::vector<tet_cond_data>& vcond)
{
    std::ifstream in;
    long int stop_at = init_parallel_read(filename, in, pctx);

    if (stop_at == -1)
    {
	ERROR("cannot open " << filename << " in parallel");
	return SPLATT_FAIL;
    }
    

    //
    char linebuf[LINESIZE];
    int data_offset = INDEX_OFFSET;
    int count = 0;
    int maxnode = 0;

    faces.clear();
    vcond.clear();

    
    // while we're not at the end of our part, and we can continue reading lines..
    while (((long int)in.tellg() < stop_at) && in.getline(linebuf, LINESIZE))
    {
        if (in.eof())
        {
            LOG("got to end of file (np="<< pctx.np()<<")");
            break;
        }

        if (in.bad())
        {
            ERROR("input stream went bad -- aborting tet read!");
            break;
        }

	if (in.fail())
	{
	    ERROR("input stream failed -- cutting tet read short!");
	    break;
	}

	int conn_scratch[8];
	std::stringstream ss(linebuf);
	int p;
	int vtag;		// volume(cell) tag
	
	ss >> p;		// cell id
	// p -= data_offset;-- thrown away

	for (int n=0; n<8; n++)
	{
	    ss >> p;
	    p-= data_offset;
	    conn_scratch[n] = p;

	    if (p > maxnode) maxnode=p;
	}


	// analyze node type based on Sagar's heuristic?
	if ( (conn_scratch[2] == conn_scratch[3]) &&
	     (conn_scratch[4] == conn_scratch[5]) )
	{
	    // tet
	    conn_scratch[3]=conn_scratch[4];

	    array<int, 4> tet_array;
	    for (int i=0; i<4; i++)
	    {
		faces.push_back(conn_scratch[i]);
	    }
	    

	    // save tet nodes in container
	    // for (int i=0; i<4; i++)
	    // {
	    // 	tet_nodes.push_back(conn_scratch[i]);
	    // }


	    // save volume condition in container
	    ss >> vtag;
	    vcond.push_back(vtag);
	}
	else
	{
            LOG(linebuf);
	    ERROR("error. only tets implemented currently, near byte " <<
		  (long int)in.tellg()<<"["<<stop_at<<"]" << "; " << faces.size()/4);
	    return SPLATT_FAIL;
	}

	count++;
    }
    if (in.eof())
    {
        LOG("got to end of file (np="<< pctx.np()<<")");
    }

    if (in.bad())
    {
        ERROR("input stream went bad -- aborting tet read!");
    }

    if (in.fail())
    {
        ERROR("input stream failed -- cutting tet read short!");
    }


    in.close();

    LLOG(1, "read in " << count << " tets");
    LLOG(1, "max_node: " << pctx.reduce(maxnode, MPI_MAX));

    return SPLATT_OK;
}




status starcd::load_nodes(std::string filename, parallel_ctx pctx,
			  std::vector<node_coords>& coords)
{
    std::ifstream in;
    char linebuf[LINESIZE];

    long int stop_at = init_parallel_read(filename, in, pctx);

    if (stop_at == -1)
    {
	ERROR("cannot open " << filename << " in parallel");
	return SPLATT_FAIL;
    }

    coords.clear();
    
    while ( (!stop_at || (long int)in.tellg() < stop_at) &&
	    in.getline(linebuf, LINESIZE))
    {
	std::stringstream ss(linebuf);

	int dummy;
	
	ss >> dummy;

	array<double,3> tmp;
	ss >> tmp;

	coords.push_back(tmp);
    }
    if (in.eof())
    {
        LOG("got to end of file (np="<< pctx.np()<<")");
    }

    if (in.bad())
    {
        ERROR("input stream went bad -- aborting bnd read!");
    }

    if (in.fail())
    {
        ERROR("input stream failed -- cutting bnd read short!");
    }

    in.close();

    return SPLATT_OK;
}



status starcd::load_boundaries(std::string filename, parallel_ctx pctx,
			       std::vector<int>& faces,
			       std::vector<bnd_cond_data>& bcond)
{
    std::ifstream in;
    long int stop_at = init_parallel_read(filename, in, pctx);

    if (stop_at == -1)
    {
	ERROR("cannot open " << filename << " in parallel");
	return SPLATT_FAIL;
    }
    
    char linebuf[LINESIZE];
    int data_offset=INDEX_OFFSET;

    faces.clear();
    bcond.clear();

    while (((long int)in.tellg() < stop_at) && in.getline(linebuf, LINESIZE))
    {
	std::stringstream ss(linebuf);

	array<int,3> face;
	int btype;
	int dummy;

	ss>>dummy;
	ss>>face;
	ss>>dummy;
	ss>>btype;

	for (int i=0; i<3; i++)
	{
	    faces.push_back(face[i] - data_offset);
	}

	bcond.push_back(btype);
	
    }

        if (in.eof())
    {
        LOG("got to end of file of bnds (np="<< pctx.np()<<")");
    }

    if (in.bad())
    {
        ERROR("input stream went bad -- aborting bnd read!");
    }

    if (in.fail())
    {
        ERROR("input stream failed -- cutting bnd read short!");
    }


    in.close();

    return SPLATT_OK;
}



status starcd::load_all(const std::string filename_prefix,
                        parallel_ctx pctx,
			std::vector< node_coords >&  coords,
			std::vector< int >&	     tets,
			std::vector< tet_cond_data >& vcond,
			std::vector< int >&	     bnds,
			std::vector< bnd_cond_data >& bcond)
{
    LLOG(0, "in load_all with np=" << pctx.np());

    int vc=0, tc=0, bc=0;
    if (try_memguide(filename_prefix + ".memguide", vc,tc,bc))
    {
	LLOG(1, "found data size estimates: " << vc << " " << tc << " " << bc);
	
	vc = ceil(vc *1.1 / pctx.np()); // average with some wiggle room for later manipulations?
	tc = ceil(tc *1.1 / pctx.np());
	bc = ceil(bc *1.1 / pctx.np());

	coords.reserve(vc);
	tets.reserve(tc*4);
	vcond.reserve(tc);
	bnds.reserve(bc*3);
	bcond.reserve(bc);

	LLOG(2, "initial capacities:");
	LLOG(2, "coords " << coords.capacity());
	LLOG(2, "tets " << tets.capacity());
	LLOG(2, "vcond " << vcond.capacity());
	LLOG(2, "bnds " << bnds.capacity());
	LLOG(2, "bcond " << bcond.capacity());

    }
    else
    {
	LLOG(1, "could not find data size estimates");
    }
    


    if (! load_nodes(filename_prefix + ".vrt", pctx, coords))
    {
	ERROR("failed to load nodes from " << filename_prefix << ".vrt");
	return SPLATT_FAIL;
    }

    if (! load_tets(filename_prefix + ".cel", pctx, tets, vcond))
    {
	ERROR("failed to load tets from " << filename_prefix << ".cel");
	return SPLATT_FAIL;
    }


    if (! load_boundaries(filename_prefix + ".bnd", pctx, bnds, bcond))
    {
	ERROR("failed to load boundaries from " << filename_prefix << ".bnd");
	return SPLATT_FAIL;
    }


    return SPLATT_OK;
}


class tet_fv_dumper
{
public:
    tet_fv_dumper(const ownerdb& odb, std::ostream& out) : odb(odb), out(out)
    {
    }

    void operator()(splatter::index* idx, int eltno, int etype,
		    int nnodes, int* nodes) const
    {
	out << "1 1 ";          // 1 1 -- tet;  2 1 -- hex

	for (int n=0; n<nnodes; n++)
	{
//	    out << nodes[n]+1 << " ";
//#ifdef SPLATT_DEBUG
	    if(odb.g2l_p(nodes[n]) == -1)
	    {
		ERROR("no local match for " << nodes[n]);
	    }
	    
//#endif // SPLATT_DEBUG
	    assert(odb.g2l_p(nodes[n]) != -1);
	    out << odb.g2l_p(nodes[n])+1 << " ";
	}

	out << std::endl;
    }

private:
    const ownerdb& odb;
    std::ostream& out;
};

class cgns_to_fv_dumper
{
public:
    cgns_to_fv_dumper(const ownerdb& odb, std::ostream& out) : odb(odb),
                                                               out(out)
    {}

    void operator()(splatter::index* idx, int eltno, int etype,
                    int nnodes, int* nodes) const
    {
        switch(etype)
        {
         case cgns::cgns_entity_type::TET4:
             out << "1 1 ";
             out << odb.g2l_p(nodes[3])+1 << " ";
             out << odb.g2l_p(nodes[0])+1 << " ";
             out << odb.g2l_p(nodes[1])+1 << " ";
             out << odb.g2l_p(nodes[2])+1 << " ";
             out << std::endl;
             break;

         case cgns::cgns_entity_type::HEX8:
             out << "2 1 ";
             out << odb.g2l_p(nodes[0])+1 << " ";
             out << odb.g2l_p(nodes[1])+1 << " ";
             out << odb.g2l_p(nodes[3])+1 << " ";
             out << odb.g2l_p(nodes[2])+1 << " ";
             out << odb.g2l_p(nodes[4])+1 << " ";
             out << odb.g2l_p(nodes[5])+1 << " ";
             out << odb.g2l_p(nodes[7])+1 << " ";
             out << odb.g2l_p(nodes[6])+1 << " ";
             out << std::endl;
             break;

         default:
             ERROR("cannot dump unknown element type " << etype << " (converting cgns to fv)");
        }
    }

private:
    const ownerdb& odb;
    std::ostream& out;
};



/*! writes a single rank's local grid to fieldview grid format */
status fvuns::write_grid(std::string filename,
			 const ownerdb& odb,
			 splatter::index* node_index,
			 splatter::index* elt_index,
			 std::vector<starcd::node_coords>& coords)
{
    std::ofstream out;

    out.open(filename.c_str());
    if (! out.is_open())
    {
	LOG("could not open " << filename << " for writing");
	return SPLATT_FAIL;
    }

    out << "FIELDVIEW_Grids 3 0" << std::endl;
    out << "GRIDS" << std::endl << 1 << std::endl;
    out << "Boundary Table" << std::endl << 0 << std::endl;

    out << "Nodes" <<std::endl;
//    out << odb.nlocal() << std::endl;
    out << odb.nlocal_p() << std::endl;
//    out << coords.size() << std::endl;

//    for (unsigned int n=0; n<coords.size(); n++)
    assert((int)coords.size() == odb.nlocal_p());
    for (int n=0; n<odb.nlocal_p(); n++)
//    for (int n=0; n<odb.nlocal(); n++)
    {
	out << "\t" << std::setprecision(15) << std::scientific << coords[n] << std::endl;
    }

    out << "Boundary Faces" << std::endl;
    out << 0 << std::endl;
    
    out << "Elements" << std::endl;

    //tet_index->apply(tet_fv_dumper(odb,out));
    elt_index->apply(cgns_to_fv_dumper(odb, out));

    out.close();
    return SPLATT_OK;
}


status fvuns::write_results(std::string filename,
			    const ownerdb& odb,
			    std::vector<std::string> vars,
			    std::vector<double>& Q)
{
    std::ofstream out;

    out.open(filename.c_str());
    if (! out.is_open())
    {
	LOG("could not open " << filename << " for writing");
	return SPLATT_FAIL;
    }

    out << "FieldView_Results 3 0" << std::endl;
    out << "Constants" << std::endl;
    out << "0." << std::endl;	// time
    out << "0." << std::endl;	// mach
    out << "0." << std::endl;	// alpha
    out << "0." << std::endl;	// re

    out << "Grids 1" << std::endl;

    out << "Variable Names" << std::endl;
    out << vars.size() << std::endl;

    for (unsigned int v=0; v<vars.size(); v++)
    {
	out << vars[v] << std::endl;
    }

    out << "Boundary Variable Names" << std::endl;
    out << "0" << std::endl;

    out << "Nodes" << std::endl;
    out << odb.nlocal_p() << std::endl;
    out << "Variables" << std::endl;
    
    assert(Q.size() == vars.size() * odb.nlocal_p());
    out << std::setprecision(15) << std::scientific;

    // unleave the Q's
    for (unsigned int v=0; v< vars.size(); v++)
    {
	for (unsigned int qi = v; qi < Q.size(); qi += vars.size())
	{
	    out << "\t" << Q[qi] << std::endl;
	}
    }

    out << "Boundary Variables" << std::endl;

    out.close();
    return SPLATT_OK;
}
