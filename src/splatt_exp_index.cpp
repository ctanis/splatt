//  -*- Mode: C++; -*- 
#include <splatter.h>
#include <functional>

using namespace splatter;
#include "Renumberer.h"

// EXPLICIT_INDEX
explicit_index::explicit_index(const ownerdb& odb,
			       std::string name, const int width, int entity_type,
			       std::vector<int>& nodes, int flags)
    : index(odb, name, SPLATT_EXPLICIT, flags),
      _etype(entity_type),
      _width(width),
      _nodes( _flags & SPLATT_STEAL_DATA ? _saved_nodes : nodes ),
      _nodeproxy(_nodes, _width), // don't propogate flogs to node proxy
      _live_hash(false),
      _deliver_during_renumber(true),
      _purge_during_renumber(true),
      _local_is_dirty(true)
{
    if (_flags & SPLATT_STEAL_DATA)
    {
	_saved_nodes.swap(nodes);
    }

}


explicit_index::explicit_index(const ownerdb& odb,
			       std::string name, const int width, int entity_type,
			       int flags)
    : index(odb, name, SPLATT_EXPLICIT, flags),
      _etype(entity_type),
      _width(width),
      _nodes( _saved_nodes ),
      _nodeproxy(_nodes, _width), // don't propogate flogs to node proxy
      _live_hash(false),
      _deliver_during_renumber(true),
      _purge_during_renumber(true),
      _local_is_dirty(true)
{
}



explicit_index::~explicit_index()
{

}
    


void
explicit_index::set_purge(bool p)
{
    _purge_during_renumber=p;
}
    

status
explicit_index::build_hash()
{
    LLOG(2, "building hash for " << _name);
    
    _node2faces.clear();
    _node2faces.update(_odb.low(),_odb.high());

    int count = _nodes.size() / _width;
    assert(_nodes.size() % _width == 0);

    for (int f=0; f<count; f++)
    {
	for (int n=0; n<_width; n++)
	{
            int node_actual = _nodes[f*_width+n];
            if (node_actual != -1) // temp_indices only
            {
                _node2faces[node_actual].insert(f);
            }
	}
    }

    _live_hash=true;

    return SPLATT_OK;
}



class remote_face_owners : public index_op
{
public:
    remote_face_owners(const ownerdb &odb,
		       std::deque<int>& to_delete,
		       std::vector<std::deque<int> >& to_move, int& counter) :
	odb(odb),
	to_delete(to_delete),
	to_move(to_move), counter(counter)
    { }


    void operator()(splatter::index* idx, int item, int etype,
		    int width, int* nodes) const
    {
	counter++;
	std::set<int> owners;
	for (int n=0; n<width; n++)
	{
	    owners.insert(odb.owner(nodes[n]));
	}


	bool keepit=false;
	for (std::set<int>::iterator c = owners.begin();
	     c != owners.end();
	     c++)
	{
	    if (*c != odb.me())
	    {
		to_move[*c].push_back(item);
	    }
	    else
	    {
		keepit=true;
	    }
	}

	if (! keepit)
	{
	    to_delete.push_back(item);
	}
    }
    
    const ownerdb			&odb;
    std::deque<int>&			to_delete;
    std::vector<std::deque<int> >&	to_move;
    int& counter;
};


// todo: fix this to update the hash incremental
status explicit_index::sync_phantom_layer(bool clobber_duplicates)
{
    LLOG(1, "syncing phantom layer for " << _name << "," <<
	 (clobber_duplicates ? "" : " NOT ") << "clobbering duplicates" );

#ifdef SPLATT_DEBUG
    for (unsigned int d=0; d<_data.size(); d++)
    {
	assert(_data[d]->size() == size());
    }
#endif // SPLATT_DEBUG

    // find new owners for each..
    migrate_args m_args(_odb.pctx());
    
    // find remote face owners
    int facecount;
    remote_face_owners rfo(_odb, m_args.to_delete, m_args.to_move, facecount);
    apply(rfo);

    // set up migrate args
    m_args.prepare();

    // turn off the hashing here, and do it explicitly after the migration
    bool dohash = _live_hash;
    _live_hash=false;
    do_migrate(m_args);
    _live_hash = dohash;
    
    if (_live_hash || clobber_duplicates)
    {
	build_hash();

	// clobber duplicates
	if (clobber_duplicates)
	{
	    LLOG(1, "clobbering duplicates");
	    migrate_args deleter(_odb.pctx());
	    int count = _nodes.size() / _width;
	    
	    std::set<int> gonna_delete;
	    for (int f=0; f<count; f++)
	    {
		if (gonna_delete.find(f) != gonna_delete.end())
		    continue;
		
		intset match = findall(_width, &_nodes[f*_width]);

		if (match.size() > 1)
		{
                    picky_matcher node_sig;
                    if (_etype == -1)
                    {
                        node_sig = picky_matcher(_width, &_nodes[f*_width]);
                    }

                    if (_etype == -1 && !node_sig.distinct())
                    {
                        for (unsigned int d=0; d<match.size(); d++)
                        {
                            // skip the one we're looking for
                            if (match[d] == f)
                                continue;

                            if (node_sig.match(&_nodes[match[d]*_width]))
                            {
                                gonna_delete.insert(match[d]);
                                deleter.to_delete.push_back(match[d]);
                            }
                        }
                    }
                    else
                    {
                        for (unsigned int d=0; d<match.size(); d++)
                        {
                            // skip the one we're looking for
                            if (match[d] == f)
                                continue;

                            gonna_delete.insert(match[d]);
                            deleter.to_delete.push_back(match[d]);
                        }
                    }
                    
		}
	    }
	    deleter.prepare();

	    do_migrate(deleter);
	    _node2faces.purge();

	}
    }
 
    LLOG(2, "complete");
    return SPLATT_OK;
}





status explicit_index::renumber(const ownerdb& new_odb, 
				const std::vector<int>& newgids,
				std::map<int,int>& global_map)
{
    LLOG(1, "renumbering an explicit index ("<<_name<<")" <<
	 (_live_hash ? " with " : " without ") << " live hash");

    DEBUG("renumbering from: " << _odb);
    DEBUG("renumbering to: " << new_odb);
    

    migrate_args rrr_margs(_odb.pctx());
    std::vector<std::set<int> > rrr_remote_needed(_odb.np());
    std::deque<int> rrr_revisit;

#ifdef SPLATT_DEBUG
    if (_live_hash)
    {
	LLOG(2, "pre-renumber validation: " << _name);
	validate_hash();
    }

#endif

    if (_live_hash)
    {
        _node2faces.update(new_odb.low(), new_odb.high());
    }
    
#ifdef SPLATT_DEBUG
    if (_live_hash)
    {
	LLOG(2, "post update validation:" << _name);
	validate_hash();

    }
#endif
    // _purge_during_renumber -> only translate

    Renumberer rrr(_odb, new_odb, newgids,
		   rrr_margs, rrr_remote_needed, rrr_revisit, global_map,
		   _live_hash, _node2faces, _purge_during_renumber,
                   _deliver_during_renumber);

    // this first pass identifies which faces we really care about,
    // and which ones must be looked up remotely before we can proceed 

    apply(rrr);

    /*
      results of applying rrr:

      remote_needed[ np ] -- old global ids to fetch per rank
      revisit -- local faces to reprocess
      args.to_delete -- no longer relevant faces

    */


    // communicate with other ranks to determine needed global ids
    std::vector<int> old_sizes(_odb.np());
    std::vector<int> new_sizes(_odb.np());
    std::vector<int> old_ids;
    std::vector<int> old_displs(_odb.np());
    std::vector<int> new_ids;
    std::vector<int> new_displs(_odb.np());

    old_displs[0]=0;
    for (unsigned int r=0; r < old_sizes.size(); r++)
    {
	old_sizes[r] = rrr.remote_needed[r].size();

	if (r > 0)
	{
	    old_displs[r] = old_displs[r-1] + old_sizes[r-1];
	}
    }
    old_ids.resize(old_displs.back() + old_sizes.back());

    // load needed ids into outbuffer
    std::vector<int>::iterator buffit=old_ids.begin();
    for (int r=0; r<_odb.np(); r++)
    {
	buffit = std::copy(rrr.remote_needed[r].begin(),
			   rrr.remote_needed[r].end(),
			   buffit);
    }


    // exchange message sizes
    MPI_Alltoall(&old_sizes[0], 1, MPI_INT,
		 &new_sizes[0], 1, MPI_INT,
		 _odb.pctx().comm());


    // calculate incoming displacements
    new_displs[0]=0;
    for (unsigned int r=1; r<new_sizes.size(); r++)
    {
	new_displs[r] = new_displs[r-1] + new_sizes[r-1];
    }
    new_ids.resize(new_displs.back() + new_sizes.back());

    int fake[]={0};

    // exchange list of required ids
    MPI_Alltoallv((old_ids.size() == 0 ? fake : &old_ids[0]),
		  &old_sizes[0],
		  &old_displs[0],
		  MPI_INT,
		  (new_ids.size() == 0 ? fake : &new_ids[0]),
		  &new_sizes[0],
		  &new_displs[0],
		  MPI_INT,
		  _odb.pctx().comm());


    // look up all the requested ids
    for (unsigned int n=0; n<new_ids.size(); n++)
    {
	new_ids[n] = newgids[_odb.g2l(new_ids[n])];
    }

    // old_ids should be the same size it was earlier, since that was the list
    //of what we needed -- now it will have the ids in it
    //old_ids.resize(new_ids.size());

    // send/retrieve remote ids from all other ranks
    MPI_Alltoallv((new_ids.size() == 0 ? fake : &new_ids[0]),
		  &new_sizes[0],
		  &new_displs[0],
		  MPI_INT,
		  (old_ids.size() == 0 ? fake : &old_ids[0]),
		  &old_sizes[0],
		  &old_displs[0],
		  MPI_INT,
		  _odb.pctx().comm());

    // old_ids has the new globals for the needed nodes in numeric order from
    // remote_needed

    new_ids.clear();
    new_sizes.clear();
    new_displs.clear();

    // build up global renumber map

    int curs=0;
    for (int r=0; r<_odb.np(); r++)
    {
	for (std::set<int>::const_iterator ci=rrr.remote_needed[r].begin();
	     ci != rrr.remote_needed[r].end();
	     ci++)
	{
	    assert(global_map.find(*ci) == global_map.end());
	    global_map[*ci] = old_ids[curs++];
	}
    }


    // get rid of some unneeded stuff...
    rrr.remote_needed.clear();
    old_ids.clear();


    // this application revisits the flagged faces, assuming we now
    // have all the global ids we need

    do_apply<Renumberer, std::deque<int>::iterator, &Renumberer::do_revisit>
	(rrr, rrr_revisit.begin(), rrr_revisit.end());

    assert(_purge_during_renumber || rrr_margs.to_delete.size() == 0);
    TRACE(876, (_purge_during_renumber ? "YES" : "NOT") << " purging " << _name);
    


    // Sat May 11 13:39:40 2013 switched with following statement 
    // temp_indices shouldn't need any batch_migration...
    // if (! _purge_during_renumber)
    // {
    //     return SPLATT_OK;
    // }
    


    // rrr.args has all the parts we need (to_move and to_delete) --
    // get it ready to mgrate
    rrr.args.prepare();

    
    // remove hash entities from rrr_margs.todelete
    if (_live_hash)
    {
	unindex_faces(rrr_margs.to_delete.begin(), rrr_margs.to_delete.end());

	// this guy will unindex faces that move unexpectedly...
	MigrateMonitor renum_monitor(_node2faces, _nodes, _width);

	//int last_count = _nodeproxy.size();
	int last_count = _nodes.size();
	
	// do the work!    
	if (_nodeproxy.batch_migrate(rrr.args, &renum_monitor) != SPLATT_OK)
	{
	    return SPLATT_FAIL;
	}

	index_faces(rrr_margs.to_delete.begin(), rrr_margs.to_delete.end());
	int new_count = _nodes.size();
	if (new_count > last_count)
	{
	    // index faces pushed on the end
	    assert(last_count % _width == 0);
	    assert(new_count % _width == 0);
	    index_faces_nodes(last_count, _nodes.size());
	}

	_node2faces.purge();

	assert(validate_hash() == SPLATT_OK);
    }
    else
    {
	// do the work!    
	if (_nodeproxy.batch_migrate(rrr.args) != SPLATT_OK)
	{
	    return SPLATT_FAIL;
	}
    }
    TRACE(876, "renumber migration complete for " << _name);

    // migrate the associated data
    for (unsigned int d=0; d < _data.size(); d++)
    {
	if (_data[d]->batch_migrate(rrr.args) != SPLATT_OK)
	{
	    return SPLATT_FAIL;
	}
    }

    return SPLATT_OK;
}




int explicit_index::find(int etype, const int* nodes)
{
    assert(etype == _etype);

    const fast_index& lockdown = _node2faces;
    
    intset match = lockdown[nodes[0]];
    for (int n=1; n<_width; n++)
    {
	match.intersect(lockdown[nodes[n]]);
    }

    if (match.size() == 0)
    {
	return -1;
    }
    else if (match.size() > 1)
    {
#ifdef SPLATT_DEBUG
	LOG("find matched multiple faces!");
	for (int n=0; n<_width; n++)
	{
	    std::cout<< nodes[n] << " ";
	}
	std::cout<< std::endl;
#endif // SPLATT_DEBUG

	for (unsigned int f=0; f<match.size(); f++)
	{
	    for (int x=0; x<_width; x++)
	    {
		std::cout << _nodes[match[f]*_width+x] << " ";
	    }
	    std::cout << std::endl;
	    
//	    LOG(match[f]);
	}
	assert(match.size() == 1);
	return match[0];
    }
    else
    {
	return match[0];
    }
}


intset explicit_index::findall(int nnodes, const int* nodes)
{
    const fast_index& lockdown = _node2faces;
    
    intset rval = lockdown[nodes[0]];
    for (int n=1; n<nnodes; n++)
    {
	rval.intersect(lockdown[nodes[n]]);
    }

    return rval;
}






status explicit_index::do_migrate(migrate_args& args)
{
    if (!args.prepared)
    {
	if (args.prepare() != SPLATT_OK)
	{
	    return SPLATT_FAIL;
	}
    }

    if (_live_hash)
    {
	unindex_faces(args.to_delete.begin(), args.to_delete.end());
	
	MigrateMonitor mmon(_node2faces, _nodes, _width);
	int last_count = _nodes.size();
	

	if (_nodeproxy.batch_migrate(args, &mmon) != SPLATT_OK)
	{
	    return SPLATT_FAIL;
	}

	index_faces(args.to_delete.begin(), args.to_delete.end());
	int new_count = _nodes.size();


	if (new_count > last_count)
	{
	    assert(last_count % _width == 0);
	    assert(new_count % _width == 0);
	    index_faces_nodes(last_count, _nodes.size());
	}


    }
    else
    {
	if (_nodeproxy.batch_migrate(args) != SPLATT_OK)
	{
	    return SPLATT_FAIL;
	}
	
    }
    

    for (unsigned int d=0; d < _data.size(); d++)
    {
	if (_data[d]->batch_migrate(args) != SPLATT_OK)
	{
	    return SPLATT_FAIL;
	}
    }

    return SPLATT_OK;
}




status explicit_index::replace(int face, const int* nn)
{
    int* face_start = &_nodes[face*_width];
    if (_live_hash)
    {

// we allow this because we can clean it up
//	assert(find(_etype, nn) == -1);

	for (int n=0; n<_width; n++)
	{
	    _node2faces[face_start[n]].remove(face);
	}

	for (int n=0; n<_width; n++)
	{
	    face_start[n] = nn[n];
	}

	for (int n=0; n<_width; n++)
	{
	    _node2faces[face_start[n]].insert(face);
	}
    }
    else
    {
	for (int n=0; n<_width; n++)
	{
	    face_start[n] = nn[n];
	}
    }

    return SPLATT_OK;
}




// do not uncomment without fixing this!

// status explicit_index::add_faces(const std::deque<int>& vec)
// {
//     if (_live_hash)
//     {
// 	int faceid=_nodes.size()/_width;


// 	for (unsigned int f=0; f<vec.size(); f+= _width)
// 	{
// 	    for (int n=0; n<_width; n++)
// 	    {
// 		_node2faces[vec[n]].insert(faceid);
// 	    }

// 	    faceid++;
// 	}	
//     }

//     // now put the actual nodes in
//     _nodes.insert(_nodes.end(), vec.begin(), vec.end());

//     return SPLATT_OK;    
// }


// the faces defined by the nodes from start to end need to be fixed
void explicit_index::index_faces_nodes(int start, int end)
{
//    DEBUG("indexing face nodes from " << start << " to " << end << " | " << _width);
    assert (start % _width == 0);
    assert ((end-start) % _width == 0);

    for (; start < end; start += _width)
    {
	// we allow this because we can clean it up
	// assert(find(_etype, &_nodes[start]) == -1);
	

	int face = start / _width;

	for (int i=0; i<_width; i++)
	{
	    _node2faces[_nodes[start+i]].insert(face);
	}
    }

}



status explicit_index::validate_hash()
{
    if (! _live_hash)
	return SPLATT_OK;
    

    const fast_index& lockdown = _node2faces;
    
    // is everything in the hash valid?
    std::map<int,intset>::const_iterator mit;
    bool bad=false;
    LOG("validating hash for " << _name);
    LOG("odb contains: " << _odb.nglobal() << " globals");
    LOG("odb contains: " << _odb.nlocal() << " locals");
    LOG("odb contains: " << _odb.nphantom() << " phantoms");
    LOG("hash contains: " << lockdown._global.size()<< " globals");
    LOG("hash contains: " << lockdown._local.size()<< " locals");
    LOG("index contains: " << size() << " faces ("<<_nodes.size()<<"/"<<_width<<")");
    

    int min=10000, max=0, total=0, count=0, maxnode=-1;

    for (mit=lockdown._global.begin(); mit!=lockdown._global.end(); mit++)
    {
	int node = mit->first;
	const intset& faces = mit->second;

	int size = faces.size();
	if (size <= min) min = size;
	if (size >= max) { max = size; maxnode = node; }
	total += size;
	count++;
	

	for (unsigned int f=0; f<faces.size(); f++)
	{
	    if (faces[f] < 0)
	    {
		ERROR("hash contains negative face");
		bad = true;
//		return SPLATT_FAIL;
	    }

	    if (faces[f] * _width >= (int)_nodes.size())
	    {
		ERROR("hash contains nonexistent face [global]: " << faces[f] << " of " << this->size() << ", refrenced by node " << node);
		bad=true;
//		return SPLATT_FAIL;
	    }

	    int start = faces[f] * _width;
	    bool foundit=false;
	    for (int i=0; i<_width; i++)
	    {
		if (_nodes[start+i] == node)
		{
		    foundit=true;
		    break;
		}		
	    }

	    if (! foundit)
	    {
		ERROR("hash contains incorrect face ("<< faces[f] <<") for global node " << node);
		for (int i=0; i<_width; i++)
		{
		    LOG("face["<<i<<"]="<<_nodes[start+i]);
		}
		bad=true;
//		return SPLATT_FAIL;
	    }
	}
    }
    

    for (unsigned int l=0; l<lockdown._local.size(); l++)
    {
	int node = l+lockdown._low;
	const intset& faces = lockdown._local.at(l);

	int size = faces.size();
	if (size < min) min = size;
	if (size > max) { max = size; maxnode = node; }
	total += size;
	count++;
	
	for (unsigned int f=0; f<faces.size(); f++)
	{
	    if (faces[f] < 0)
	    {
		ERROR("hash contains negative face");
		bad = true;
//		return SPLATT_FAIL;
	    }

	    if (faces[f] * _width >= (int)_nodes.size())
	    {
		ERROR("hash contains nonexistent face [local]: " << faces[f] << " of " << this->size());
		bad=true;
//		return SPLATT_FAIL;
	    }

	    int start = faces[f] * _width;
	    bool foundit=false;
	    for (int i=0; i<_width; i++)
	    {
		if (_nodes[start+i] == node)
		{
		    foundit=true;
		    break;
		}		
	    }

	    if (! foundit)
	    {
		ERROR("hash contains incorrect face "<<(faces[f])<<" for local node " << node << "("<<l<<")");
		for (int i=0; i<_width; i++)
		{
		    LOG("face["<<i<<"]="<<_nodes[start+i]);
		}
		bad=true;
//		return SPLATT_FAIL;
	    }
	}
    }
    
    LOG("hash stats: " << min << " " << max << "@" << maxnode << " | "<< total*1.0/count << " " << count << "|" << lockdown[maxnode]);
    

    // is everything in the node index in the hash?
    for (unsigned int f=0; f<_nodes.size(); f+= _width)
    {
	for (int i=0; i<_width; i++)
	{
	    if (! lockdown[_nodes[f+i]].contains(f/_width))
	    {
		for (int j=0; j<_width; j++)
		{
		    std::cout << _nodes[f+j] << " ";
		}
		std::cout << std::endl;
				

		LOG("looking for " << _nodes[f+i] << ", hash has " << lockdown[_nodes[f+i]]);
		
		for (int m=0; m<(int)lockdown[_nodes[f+i]].size(); m++)
		{
		    std::cout << lockdown[_nodes[f+i]][m] << ":";
		    for (int j=0; j<_width; j++)
		    {
			std::cout << _nodes[lockdown[_nodes[f+i]][m]*_width+j] << " ";
		    }
		    std::cout << std::endl;
		}

                ERROR("hash has incomplete info about face " << f/_width);
		bad=true;
		break;
	    }
	}


	// not worth checking this, since we encourage duplicates now (kinda)

	//make sure this face only shows up once in the index
	// intset matches = findall(_width, &_nodes[f]);
	// if (matches.size() != 1)
	// {
	//     LOG("too many matches in " << _name << "! " << matches.size()  << "/" << _width);

        //     for (int n=0; n<_width; n++)
        //     {
        //         std::cout << _nodes[f+n] << " ";
        //     }
        //     std::cout << std::endl;
	//     //bad = true;
	// }
    }

    assert(!bad);
    if (bad)
    {
	return SPLATT_FAIL;
    }
    else
    {
	return SPLATT_OK;
    }
}




status explicit_index::deliver(const deliver_args& args, temp_index& tmpout, bool also_data)
{
    LLOG(1, "");
    _nodeproxy.deliver(args, &(tmpout._nodeproxy));
    tmpout.eltnos.resize(tmpout.size(), -1);
    if (tmpout._live_hash)
    {
	tmpout.build_hash();
    }
    
    
    if (also_data)
    {
	for (unsigned int d=0; d<_data.size(); d++)
	{
	    tmpout._data_names.push_back(_data_names[d]);
	    tmpout._data.push_back(_data[d]->deliver(args));
	}
    }
    

    return SPLATT_OK;
}



status explicit_index::validate_entities()
{
    bool notbad=true;

    int count = _nodes.size()/_width;


    for (int f=0; f<count; f++)
    {
	// make sure this entity has _width distinct nodes
	std::set<int> nds;

	for (int n=0; n<_width; n++)
	{
	    nds.insert(_nodes[f*_width+n]);
	}

	if ((int)nds.size() != _width)
	{
	    ERROR("rogue entity found");
	    for (int n=0; n<_width; n++)
	    {
		std::cout << _nodes[f*_width+n] << " ";
	    }
	    std::cout << std::endl;
	    notbad=false;
	}

    }



    assert(notbad);
    if (notbad)
    {
	return SPLATT_OK;
    }
    else
    {
	return SPLATT_FAIL;
    }
}



status explicit_index::save(std::ofstream& out)
{
    unsigned int n=0;
    out << size() << std::endl;
    while (n < _nodes.size())
    {
	for (int i=0; i<_width; i++)
	{
	    out << _nodes[n+i] << " ";
	}
	out << std::endl;
	n += _width;
    }

    return SPLATT_OK;
}


static const int line_length=1024;

status explicit_index::load(std::ifstream& in)
{
    char buffer[line_length];

    int count;

    {
	in.getline(buffer, line_length);
	std::stringstream ss(buffer);
	ss  >> count;
    }
    
    _nodes.clear();
    _nodes.reserve(count * _width);
    
    for (int n=0; n<count; n++)
    {
	in.getline(buffer, line_length);
	std::stringstream ss(buffer);
	
	for (int i=0; i<_width; i++)
	{
	    int val;
	    ss >> val;
	    _nodes.push_back(val);
	}
    }
    
    for (unsigned int d=0; d<_data.size(); d++)
    {
	_data[d]->resize(size());
    }


    return SPLATT_OK;
}


status explicit_index::load_nodes(std::vector<int>& data, int flags)
{
    if (flags & SPLATT_STEAL_DATA)
    {
	_nodes.swap(data);
    }
    else
    {
	_nodes = data;
    }

    for (unsigned int d=0; d<_data.size(); d++)
    {
	_data[d]->resize(size());
    }

    return SPLATT_OK;
}


const int* const explicit_index::local_nodes() const
{
    if (_local_is_dirty)
    {
        _local_nodes.resize(_nodes.size());
        _local_ownership.resize(size());

        int n=0;
        for (int c=0; c<size(); c++)
        {
            int smallest = _nodes[n];

            for (int cn=0; cn<_width; cn++)
            {
                int inode = _nodes[n];

                if (inode < smallest) smallest = inode;

                _local_nodes[n] = _odb.g2l_p(inode);
                n++;
            }

            if (_odb.owns(smallest))
            {
                _local_ownership[c] = 1;
            }
            else
            {
                _local_ownership[c] = 0;
            }
        }

        _local_is_dirty=false;
    }

    return &_local_nodes[0];
}


const std::vector<int>& explicit_index::local_ownership() const
{
    if (_local_is_dirty)
    {
        local_nodes();
    }
    return _local_ownership;
}

// these must agree...
static const int MAX_COLORS=64; // number of bits in COLORS
//typedef std::bitset<MAX_COLORS> COLORS;
typedef unsigned long int COLORS;


int explicit_index::color_faces(std::vector<int>& colors,
                                std::vector<int>& color_count) const
{
    local_nodes();              // ensure local node calculation
    
    std::vector<COLORS> node_colors(_odb.nlocal_p(), 0);
    unsigned int count = _nodes.size() / _width;

    colors.clear();
    colors.resize(count, 0);
    color_count.clear();
    color_count.resize(MAX_COLORS, 0);
    
    int max_color=-1;

    for (unsigned int c=0; c<count; c++)
    {
        COLORS current=0;
        int base_node = c*_width;

        for (int n=0; n < _width; n++)
        {
            current |= node_colors[ _local_nodes[base_node+n] ];
        }

        // choose smallest unused color
        
        COLORS match=0x1;
        int newcol=0;
        for (; newcol<MAX_COLORS; newcol++)
        {
//            printf("%u, trying color %d -- %u\n", c, newcol, match);
            if (! (match & current))
            {
                break;
            }
            else
            {
                match <<=1;
            }
        }

        if (newcol == MAX_COLORS)
        {
            ERROR("exceeded max colors: " << newcol);
            return -1;
        }


        colors[c] = newcol;
        color_count[newcol]++;

        if (newcol > max_color)
        {
            max_color = newcol;
        }

        for (int n=0; n<_width; n++)
        {
            node_colors[ _local_nodes[base_node+n] ] |= match;
        }
    }

    _local_is_dirty=true;
    return max_color+1;
}

void explicit_index::group_by(std::vector<int>& tags,
                              std::vector<int>& tag_counts,
                              int num_tags)
{
    std::vector<int> offsets(tag_counts.size()+1);
    std::vector<int> newlocs(tags.size());

    for (unsigned int i=1; i<tag_counts.size(); i++)
    {
        offsets[i] = offsets[i-1]+tag_counts[i-1];
    }
    offsets[tag_counts.size()] = offsets[tag_counts.size()-1]
        + tag_counts[tag_counts.size()-1];
    // save this ...
    _order_groups = offsets;


    for (unsigned int c=0; c<tags.size(); c++)
    {
        newlocs[c] = offsets[tags[c]];
        offsets[tags[c]]++;
    }
    
    LLOG(1, "reordering elements for group_by");
    _nodeproxy.reorder(newlocs);
    for (unsigned int d=0; d<_data.size(); d++)
    {
        LLOG(1, "reordering data for group_by");
        _data[d]->reorder(newlocs);
    }

}


void explicit_index::group_by_color()
{
    std::vector<int> colors;
    std::vector<int> counts;
    int max_colors;

    max_colors = color_faces(colors, counts);
    group_by(colors, counts, max_colors);
}
