//  -*- Mode: C++; -*- 
#include <splatter.h>
using namespace splatter;



// INDEX SUPERCLASS
index::index(const ownerdb& odb, std::string name, index_type type, int flags)
    : _odb(odb), _name(name), _type(type), _flags(flags)
{
}


index::~index()
{
    for (unsigned int i=0; i<_data.size(); i++)
    {
	delete _data[i];
    }
}


status index::attach_data(std::string name, data_proxy* p)
{
    _data.push_back(p);
    _data_names.push_back(name);
    p->resize(size());
    return SPLATT_OK;
}


status index::detach_data(std::string name)
{
    unsigned int d;
    for (d=0; d<_data.size() && _data_names[d] != name; d++)
    {}

    if (_data_names[d] == name)
    {
	// found it
	delete _data[d];
	_data.erase(_data.begin() + d);
	_data_names.erase(_data_names.begin() + d);
	return SPLATT_OK;
    }
    else
    {
	ERROR("no data named " << name);
	return SPLATT_FAIL;
    }
}

