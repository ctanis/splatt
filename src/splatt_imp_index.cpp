//  -*- Mode: C++; -*- 
#include <splatter.h>
using namespace splatter;


// // IMP_INDEX
// implicit_index::implicit_index(const ownerdb& odb,
// 			       std::string name, int nodetype,
// 			       int flags) :
//     index(odb, name, SPLATT_IMPLICIT, flags),
//     _etype(nodetype)
// {    
// }




// status implicit_index::add_globals(const std::set<int>& newg)
// {
//     std::set<int>::const_iterator curs = newg.begin();
    
//     for (; curs != newg.end(); curs++)
//     {
// 	// add truly new globals
// 	if (_glob2loc.find(*curs) == _glob2loc.end())
// 	{
// 	    _glob2loc[*curs]=1;
// 	}
	
//     }

//     // // rebuild communication maps
//     // return build_sync_maps();

//     return SPLATT_OK;
// }



// status implicit_index::build_sync_maps()
// {
//     std::map<int,int>::iterator gmap;

//     _loc2glob.clear();
    
//     //    DEBUG("nlocal:" << _nlocal);
//     // FIGURE OUT WHAT WE NEED
    
//     // auto-sorted!
//     int curs=0;
//     int last_owner=-1;
//     for (gmap = _glob2loc.begin(); gmap != _glob2loc.end(); gmap++)
//     {
// 	int owner = _odb.owner(gmap->first);

// 	_loc2glob.push_back(gmap->first);

// 	if (owner > last_owner)	// we may have skipped some ranks
// 	{
// 	    for (int i=last_owner+1; i<=owner; i++)
// 	    {
// 		// displacement for rank i -- starting at end of local data
// 		// update when we get to new rank's data
// 		_cmap.in_displs[i]=curs;
// //		DEBUG("in_displs["<<i<<"]="<<_cmap.in_displs[i]);
// 	    }
// 	}
// 	last_owner = owner;

// 	gmap->second = curs++;	// store position of this gid
//     }

//     // set remaining input displacements
//     for (unsigned int i=last_owner+1; i<_cmap.in_displs.size(); i++)
//     {
// 	_cmap.in_displs[i]=curs;
// //	DEBUG("in_displs["<<i<<"]="<<_cmap.in_displs[i]);
//     }
    


//     // TELL EVERYONE ELSE WHAT WE NEED

//     // build sizes out of displacements
//     for (int r=0; r<_odb.pctx().np(); r++)
//     {
// 	_cmap.in_sizes[r] = _cmap.in_displs[r+1]-_cmap.in_displs[r];
//     }

    
//     // ALSO FIGURE OUT WHAT EVERYONE ELSE NEEDS FROM ME
//     MPI_Alltoall(&(_cmap.in_sizes[0]), 1, MPI_INT,
// 		 &(_cmap.out_sizes[0]), 1, MPI_INT,
// 		 _odb.pctx().comm());

//     // for (int r=0; r<pctx.np(); r++)
//     // {
//     // 	DEBUG("sending " << _cmap.outsizes[r] << " to " << r);
//     // 	DEBUG("getting " << _cmap.insizes[r] << " from " << r);
//     // }

//     // now, outamts is size of buffer for each outgoing rank

//     int total_out_size=0;
//     for (int r=0; r<_odb.pctx().np(); r++)
//     {
// 	_cmap.out_displs[r]=total_out_size;
// 	total_out_size += _cmap.out_sizes[r];
//     }

// //    DEBUG("making room for " << total_out_size << " ids");
//     _cmap.out_ids.resize(total_out_size);

//     // read actual index needs from each remote rank
//     MPI_Alltoallv(&(_loc2glob[0]),	// send my global id needs
// 		  &(_cmap.in_sizes[0]),		// sizes of global id needs per rank
// 		  &(_cmap.in_displs[0]),	// displacement of global id needs per rank
// 		  MPI_INT,
// 		  &(_cmap.out_ids[0]),	// read remote global id needs
// 		  &(_cmap.out_sizes[0]),	// number of values for each rank
// 		  &(_cmap.out_displs[0]),	// displacement of ranks in _outgoing
// 		  MPI_INT,
// 		  _odb.pctx().comm());

//     // convert outids to local numbering
//     for (unsigned int i=0; i<_cmap.out_ids.size(); i++)
//     {
// 	_cmap.out_ids[i] -= _odb.low();
//     }

//     // rewrite in_displs to account for local nodes
//     for (unsigned int n=0; n<_cmap.in_displs.size(); n++)
//     {
// 	_cmap.in_displs[n] += _odb.nlocal();
//     }
    

//     DEBUG("comm_buffers built for ra_index " << _name);

    

//     // make sure all attached data has room for phantom contents
//     for (unsigned int i=0; i<_data.size(); i++)
//     {
// 	_data[i]->resize(size());
//     }
    
//     return SPLATT_OK;
// }


// status implicit_index::sync(int dataid)
// {
//     if (_dirty)
//     {
// 	build_sync_maps();
//     }

//     if (dataid == -1)
//     {
// 	// sync all data
// 	for (unsigned int d=0; d < _data.size(); d++)
// 	{
// 	    if (_data[d]->sync(_mgr->pctx(), _cmap) == SPLATT_FAIL)
// 	    {
// 		ERROR("data sync failed for " << _name << " / "
// 		      << _data_names[dataid]);
// 		return SPLATT_FAIL;
// 	    }

// 	}
//     }
//     else
//     {
// 	// just sync one
// 	if (_data[dataid]->sync(_mgr->pctx(), _cmap) == SPLATT_FAIL)
// 	{
// 	    ERROR("data sync failed for " << _name << " / "
// 		  << _data_names[dataid]);
// 	    return SPLATT_FAIL;
// 	}
	
//     }
    

//     return SPLATT_OK;
// }


status implicit_index::renumber(const ownerdb& new_odb,
				const std::vector<int>& newgids,
				std::map<int,int>& global_map)
{
    LLOG(1, "renumbering an implicit index: " << newgids.size() << ";" << _odb << ";" << new_odb);;
    assert((int)newgids.size() == _odb.nlocal());

    if (_data.size() == 0)
    {
    	LOG("no data attached, so no need to renumber implicit_index");
    	return SPLATT_OK;
    }


    migrate_args args(new_odb.pctx());
    
    
    for (int n=0; n<_odb.nlocal(); n++)
    {
	if (newgids[n] == -1)
	{
	    args.to_delete.push_back(n);
	}
	else
	{
	    int owner = new_odb.owner(newgids[n]);
	
	    if (owner != new_odb.me())
	    {
		args.to_move[owner].push_back(n);
		args.to_delete.push_back(n);
	    }
	}
	
    }
    args.prepare();



    // distribute a copy of these global ids using batch_migrate
    std::vector<int> localids = newgids;
    data_proxy_std<int> proxy(localids);

    proxy.batch_migrate(args);


#ifdef SPLATT_DEBUG
    for (unsigned int n=0; n<localids.size(); n++)
    {
        assert(localids[n] != -1);
	if (! new_odb.owns(localids[n]))
	{
	    DEBUG("received a node i don't own: " <<  newgids[n]);
	}
    }
    LLOG(2, "finished checking all nodes");
#endif	// SPLATT_DEBUG

    for (unsigned int n=0; n<localids.size(); n++)
    {
	assert(new_odb.owns(localids[n]));
	localids[n] = new_odb.g2l(localids[n]); // now they're local
    }
    //  now we know exactly the out-of-order values in the
    //  post-migrate data


    int x=0;
    for (unsigned int d=0; d<_data.size(); d++)
    {
	DEBUG("batch migrating args for renumber " << _data_names[d] << "," << _data[d]->size());
	_data[d]->batch_migrate(args);
	DEBUG("reorder for renumber " << _data_names[d] << "," << _data[d]->size());
	DEBUG("asserting that " << _data[d]->size() << " == " << localids.size());
	assert(_data[d]->size() == (int)localids.size());

	_data[d]->reorder(localids);

	_data[d]->resize(new_odb.nlocal());
	

	DEBUG("done");
	x++;
    }

    // register that new sync_maps are needed prior to another sync
    return SPLATT_OK;
}


status implicit_index::sync(const parallel_ctx& pctx, const sync_args& args)
{

    for (unsigned int d=0; d<_data.size(); d++)
    {
	if (_data[d]->sync(pctx, args) != SPLATT_OK)
	{
	    ERROR ("data sync failed for " << _name << "/" << _data_names[d]);
	    return SPLATT_FAIL;
	}
    }

    return SPLATT_OK;
}


status implicit_index::sync_one(const parallel_ctx& pctx, const sync_args& args, std::string tag)
{

    for (unsigned int d=0; d<_data.size(); d++)
    {
	if (_data_names[d] == tag)
	{

	    if (_data[d]->sync(pctx, args) != SPLATT_OK)
	    {
		ERROR ("data sync failed for " << _name << "/" << _data_names[d]);
		return SPLATT_FAIL;
	    }
	    else
	    {
		return SPLATT_OK;
	    }
	}
    }

    ERROR("couldn't find data with tag " << tag);
    return SPLATT_FAIL;
}


status implicit_index::revert_to_local(int nlocal)
{
    DEBUG("resizing implicit data  to " << nlocal);
    for (unsigned int d=0; d<_data.size(); d++)
    {
	DEBUG("old size: " << _data[d]->size());
	_data[d]->resize(nlocal);
    }

    return SPLATT_OK;
}
