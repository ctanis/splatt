//  -*- Mode: C++; -*- 
#ifndef _SPLATT_PARALLEL_H_
#define _SPLATT_PARALLEL_H_


namespace splatter
{
    
    /*!
      Container of all MPI details for the current mesh
    */
    class parallel_ctx
    {
    public:

	/*! Instantiate with provided MPI communicator
          \param comm the MPI communicator to use
        */
	explicit parallel_ctx(MPI_Comm comm);


	/*! Instantiate placeholder context.  Requires `init()` prior to use */
	explicit parallel_ctx();

	/*! Initialize this `parallel_ctx`
          \param comm the MPI communicator to use
        */
	void init(MPI_Comm comm);

	/*! Rebind stdout/stderr to file stdout.<rank>
          \param ignore_rank MPI rank that should continue printing to stdout
          \param file_prefix optional prefix before stdout/stderr in filename
        */
	void rebind_stdio(int ignore_rank=-1, std::string file_prefix="") const;

	/*! \returns number of processors in this MPI communicator */
	int np() const { return _np; }

	/*! \returns this process's rank on this MPI communicator */
	int rank() const { return _rank; }

	/*!  \returns this MPI communicator */
	MPI_Comm& comm() const { return _comm; }

	/*! \returns rank of processor considered the official source in `broadcast` */
	int root() const { return _root; }

        const char* machine_name() const { return _macname; }


	/*! Wrapper around MPI_Reduce
          \param in local data for reduction
          \param op MPI operator for reduction
          \returns global reduction value */
	template <typename T> T reduce(T in, MPI_Op op) const;

	/*! Wrapper around MPI_Broadcast
          \param vec data to broadcast
          \param root source of official data (see `root()` if -1 is used
        */
	template <typename T> void broadcast(std::vector<T>& vec, int root=-1) const;

        /*! Wrapper around MPI_Barrier */
        void barrier() const;
        

	/*! Create new `parallel_ctx` using values from another
         \param other the other `parallel_ctx` to copy
         */
	parallel_ctx(const parallel_ctx& other);

	/*! Assignment operator
         \param other the source of the assigned values
        */
	parallel_ctx& operator=(const parallel_ctx& other);
	

    protected:
	void set_communicator(MPI_Comm comm);
	// void set_max_threads(int mt);

	int	_np;
	int	_rank;
	// int	_max_threads;
	int	_root;
        char _macname[MPI_MAX_PROCESSOR_NAME];
	mutable MPI_Comm _comm;
    };


}


#include "inline/parallel_cpp.h"



#endif	/* _SPLATT_PARALLEL_H_ */
