/*  -*- Mode: C++; -*-  */

#ifndef _SPLATT_GEOM_H_
#define _SPLATT_GEOM_H_

namespace splatter
{
    namespace geom
    {

	// return squared euclidean distance between x and y
	inline double dist2(const splatter::array<double,3>& x, const splatter::array<double,3>& y)
	{
	    double ans=0;
	    for (int i=0; i<3; i++)
	    {
		double c = y[i] - x[i];
		ans += c*c;
	    }

	    return ans;
	}

	inline double magnitude(const splatter::array<double,3>& x)
	{
	    return sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]);
	}


	inline double dot(const splatter::array<double,3>& A,
			  const splatter::array<double,3>& B)
	{
	    double ans=0;
	    for (int n=0; n<3; n++)
	    {
		ans += (A[n]*B[n]);
	    }

	    return ans;
	}

	inline splatter::array<double,3> cross(const splatter::array<double,3>& A,
					       const splatter::array<double,3>& B)
	{
	    splatter::array<double,3> ans;

	    ans[0] = A[1]*B[2] - A[2]*B[1];
	    ans[1] = A[2]*B[0] - A[0]*B[2];
	    ans[2] = A[0]*B[1] - A[1]*B[0];

	    return ans;
	}


	inline double tet_volume( const splatter::array<double,3>& A,
				  const splatter::array<double,3>& B,
				  const splatter::array<double,3>& C,
				  const splatter::array<double,3>& D )
	{
	    return std::abs(dot(A-D, cross(B-D, C-D)) / 6.0);
	}

	inline double tri_area( const splatter::array<double,3>& A,
				const splatter::array<double,3>& B,
				const splatter::array<double,3>& C)
	{
	    return magnitude(cross(B-A,C-A)) * 0.5;
	}

	

    }
}



#endif /* _SPLATT_GEOM_H_ */
