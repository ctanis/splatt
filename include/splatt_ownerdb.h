/*  -*- Mode: C++; -*-  */

#ifndef _SPLATT_OWNERDB_H_
#define _SPLATT_OWNERDB_H_

namespace splatter
{
    /*! Container of all parallel distribution / node ownership data
     */
    class ownerdb
    {
    public:
	/*! Public constructor: creates invalid `ownerdb` */
        ownerdb() : _rank(-1), _np(0), _low(0), _high(0) {}
	
        /*! \param g the global node id being considered
           \returns true iff the local processor owns global node g
         */
	bool	owns(int g) const; // all are inline

        /*! \param g the global node id being considered
          \returns the rank of hte process that owns global node g
        */
	int	owner(int g) const;

        /*! \returns the local processor's rank */
	int	me() const;

        /*! \returns the number of processors in this MPI communicator */
	int	np() const;

        /*! \returns the high node id (exclusive) assigned to the local rank */
	int	high() const;

        /*! \returns the low node id (inclusive) assigned to the local rank */
	int	low() const;
        
        /*! \param g the global node id being considered
          \returns the corresponding local node id, assuming it is owned locally */
	int	g2l(int g) const;	// assuming g is owned by us

        /*! \param l the local node id being considered
          \returns the corresponding global node id, assuming l is valid */
	int	l2g(int l) const;

        /*! \param g the global node id being considered
          \returns the corresponding local node id, assuming it is owned locally or a phantom node */
	int	g2l_p(int g) const; // look in phantom thing

        /*! \param l the local node id being considered
          \returns the corresponding local node id, assuming it is owned locally or a phantom node */
	int	l2g_p(int l) const; // look in phantom thing

        /*! \returns the number of nodes owned locally */
	int	nlocal() const;
        /*! \returns the number of global nodes */
	int	nglobal() const;
        /*! \returns the total number of local nodes, including phantom nodes */
	int	nlocal_p() const; // size including phantom
        /*! \returns the number of phantom nodes locally */
	int	nphantom() const;
	

        /*! \returns the underlying node distribution */
	const int* node_dist() const;

        /*! \returns the underlying `parallel_ctx` */
	const parallel_ctx& pctx() const;
	
        /*! <b>Do not call</b> */
	void debug_phantoms() const;
	

    private:
	friend class part_mgr;
	ownerdb(const parallel_ctx& pctx, const std::vector<int>& node_dist);
	status init(const parallel_ctx& pctx, std::vector<int>& node_dist);

	// this is destructive to 'other'
	ownerdb& operator=(ownerdb& other);

	parallel_ctx _pctx;
	std::vector<int> _node_dist;
	int _rank;
	int _np;
	int _low;
	int _high;

	std::map<int,int>	_glob2loc;
	std::vector<int>	_loc2glob;

    };

    inline std::ostream& operator<<(std::ostream& out, const ownerdb& odb)
    {
	out << "[odb " << odb.me() << "/" << odb.np() << ", " << odb.low() <<
	    "-" << odb.high() << " | " << odb.nlocal() << "/" << odb.nglobal() << "]";

	return out;
    }
    

}

#include "inline/ownerdb_cpp.h"

#endif /* _SPLATT_OWNERDB_H_ */
