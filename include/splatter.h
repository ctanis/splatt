//  -*- Mode: C++; -*- 
#ifndef _SPLATT__H_
#define _SPLATT__H_

#ifndef OMPI_SKIP_MPICXX
#define OMPI_SKIP_MPICXX
#endif

#include <mpi.h>
//#include <omp.h>

#include <fstream>
#include <vector>
#include <list>
#include <deque>
#include <map>
#include <set>
#include <algorithm>
#include <exception>
#include <string>
#include <cassert>

/** namespace containing all user-accessible classes and subroutines **/
namespace splatter
{
    class parallel_ctx;		// a MPI/thread config
    class part_mgr;		// manager of this rank's part
    class ownerdb;		// arbiter of who owns what
    class index;		// some index of mesh entities
    class data_proxy;		// structure around data associated with an index
    class app_hook;		// callback for mesh modifications
    class coarsen_kernel;       // storage for coarsening bookkeeping
    
    enum status { SPLATT_FAIL, SPLATT_OK };


    // flags
    const int 	SPLATT_STEAL_DATA=0x2;
    const int 	SPLATT_FAST_INDEX=0x4;
//    const int	SPLATT_AUTO_AUGMENT=0x8;
//    const int	SPLATT_NODE_MASTER=0x16;
//    const int	SPLATT_PHANTOM_DEPS=0x32;
    const int	SPLATT_PHANTOM_DEPS=0x8;


    const int SPLATT_UVEC_INIT_CAPACITY=4;

// Eventually this would be nice, but everything just uses ~int~ now...
//    typedef int node_id;
}


#include "splatt_log.h"
#include "splatt_conf.h"
#include "splatt_helpers.h"
#include "splatt_mpi_types.h"
#include "splatt_parallel.h"
#include "splatt_topo.h"
#include "splatt_ownerdb.h"
#include "splatt_sync.h"
#include "splatt_data.h"
#include "splatt_index.h"
#include "splatt_hooks.h"
#include "splatt_core.h"
#include "splatt_io.h"

// include this in your application
// #include "splatt_query.h"


namespace splatter
{
//    status decomp(part_mgr& mgr, std::string index_name, int nparts, int* edgecutp);

    // this can be run on any communicator, but does not do any node
    // movement, as the long as the ranks involved are contiguous
    // starting with 0 -- otherwise the ownerdb will not work!
    status decomp(MPI_Comm comm, splatter::index* idx, int nparts, int* edgecut,
		  ownerdb odb,
		  std::vector<int>& new_gids, std::vector<int>& new_dist,
		  bool adapt=false, int flag=-1);    

    status decomp(part_mgr& mgr, bool adapt=false,
                  MPI_Comm comm=MPI_COMM_WORLD, int nparts=-1);

}



#ifdef USE_RESTRICT
#define restrict __restrict__
#else
#define restrict
#endif

#endif	/* _SPLATT__H_ */
