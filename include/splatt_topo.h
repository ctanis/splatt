/*  -*- Mode: C++;  -*-  */

#ifndef _SPLATT_TOPO_H_
#define _SPLATT_TOPO_H_

namespace splatter
{
    #define SPLATT_MAX_SUB_ENTTITY 6
    #define SPLATT_MAX_NODES	27

    /*! \brief A description of a valid entity type for a mesh.

      Note: when constructing a part_mgr, a valid topological configuration is
      required.  This should simply be an entity_cfg*, pointing to an array of
      these entity_cfg objects.  Note that the position in this array
      identifies the entity type.  Therefore an entity_cfg only makes sense in
      the context of a larger array -- at least where subface_type is
      concerned.

\code{.cpp}
const entity_cfg topo::std_entities[]=
{
// NODE
    { "node", 0, 1, 0},
// EDGE
    { "edge", 1, 2, 2,
      {
	  { topo::NODE, { 0 } },
	  { topo::NODE, { 1 } }
      }
    },
// FACE_TRI
    { "tri", 2, 3, 3,
      {
	  { topo::EDGE, { 0, 1 } },
	  { topo::EDGE, { 1, 2 } },
	  { topo::EDGE, { 2, 0 } }
      }
    },
// FACE_QUAD
    { "quad", 2, 4, 4,
      {
	  { topo::EDGE, { 0, 1 } },
	  { topo::EDGE, { 1, 2 } },
	  { topo::EDGE, { 2, 3 } },
	  { topo::EDGE, { 3, 1 } }
      }
    },
// CELL_TET
    { "tet", 3, 4, 4,
      {
	  { topo::TRI, { 0, 1, 2 } },
	  { topo::TRI, { 1, 3, 2 } },
	  { topo::TRI, { 0, 3, 1 } },
	  { topo::TRI, { 0, 2, 3 } }
      }
    },
// CELL_PYR
    { "pyramid", 3, 5, 5,
      {
	  { topo::QUAD, { 0, 3, 2, 1 } },
	  { topo::TRI, { 0, 1, 4 } },
	  { topo::TRI, { 1, 2, 4 } },
	  { topo::TRI, { 2, 3, 4 } },
	  { topo::TRI, { 3, 0, 4 } }
      }
    },    

    SPLATT_ENTITY_END
}
\endcode
     */
    struct entity_cfg
    {
	std::string name; /**< the name of this entity type (triangle, tet, etc.) */
	int dim;	  /**< dimension of this entity */
	int nnodes;	  /**< number of nodes comprising this entity */
	int numsubfaces;  /**<number of subentitys for this type */

	struct
	{
	    int	subface_type;	/**< type corresponds to index of the
				 * subface type in the vector/array
				 * that encompasses this current
				 * entity_type */
	    int	subface_nodes[SPLATT_MAX_NODES];
	} subfaces[SPLATT_MAX_SUB_ENTTITY]; /**<vector of subfaces and corresponding nodes*/
    };

    
    // see splatt_topo.cpp
    namespace topo
    {

#define SPLATT_ENTITY_END {""}
    

	extern const entity_cfg std_entities[];
	static const int num_std_entities=6;
	

	/*! enumerated type mapping entity type symbols to their positions in std_entities */
	enum std_entity_type
	{
	    NODE=0,
	    EDGE=1,
	    TRI=2,
	    QUAD=3,
	    TET=4,
	    PYR=5
	};
    }


    // class etype_graph
    // {
    // public:
    // 	status add_type(int id, const entity_cfg& e);
    // 	status finalize();

    // 	const entity_cfg& operator[](int type) const;
	
    // private:
    // 	struct node
    // 	{
    // 	    int			type;
    // 	    entity_cfg		cfg;
    // 	    std::map<int,node*> up;
    // 	    std::map<int,node*> down;
    // 	};
	
    // 	std::map<int, node*>	_type_map;
    // 	std::vector<entity_cfg>	_etypes;


    // };


    namespace cgns
    {
        enum cgns_entity_type
        {
            NODE=0,
            EDGE2=1,
            EDGE3=2,
            TRI3=3,
            TRI6=4,
            QUAD4=5,
            QUAD9=6,
            TET4=7,
            TET10=8,
            PYR5=9,
            PYR14=10,
            PRISM6=11,
            PRISM18=12,
            HEX8=13,
            HEX27=14
        };

        extern const entity_cfg cgns_entities[];
        static const int num_cgns_entities = 15;
        
        // cgns ElementType_T
        // NODE, BAR_2, BAR_3,
        //     TRI_3, TRI_6, QUAD_4, QUAD_8, QUAD_9,
        //     TETRA_4, TETRA_10, PYRA_5, PYRA_14,
        //     PENTA_6, PENTA_15, PENTA_18,
        //     HEXA_8, HEXA_20, HEXA_27,



    }

    
    
    

    
}
#endif	/* _SPLATT_TOPO_H_ */
