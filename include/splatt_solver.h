//  -*- mode: c++; -*- 
#ifndef _SPLATT_SOLVER_H_
#define _SPLATT_SOLVER_H_

// utilities for building field-based solvers


namespace splatter
{

    class FullCSRBuilder
    {
    public:
        FullCSRBuilder(part_mgr& mgr) :
            _odb(mgr.get_ownerdb())
        {
//            _hash.resize(_odb.nlocal_p());
            _hash.resize(_odb.nlocal());
        }


        // remember dependency between nodes
        void operator()(splatter::index*, int, int,
                        int width, int* nodes) const
        {
            for (int n1=0; n1<width; n1++)
            {
                if (_odb.owns(nodes[n1])) // fix? Fri Jun 30 17:45:43 2017
                {

	    
//                int l = _odb.g2l_p(nodes[n1]);
                    int l = _odb.g2l(nodes[n1]);

                    for (int n2=0; n2<width; n2++)
                    {
		
                        _hash[l].insert(_odb.g2l_p(nodes[n2]));
                    }
                }
            }
        }

        void getCSR(std::vector<int>& ia, std::vector<int> &ja)
        {
            std::vector<int> tmp;
            getCSR(ia, tmp, ja, false);
        }



        void getCSR(std::vector<int>& ia, std::vector<int>& iau, std::vector<int>& ja,
                    bool do_iau=true)
        {

            // preallocate ia,ja
            ia.reserve(_hash.size()+1);

            if (do_iau)
            {
                iau.reserve(_hash.size());
            }

            // std::map<int, std::set<int> >::iterator ss;

            // int total=0;
            // for (ss=_hash.begin(); ss != _hash.end(); ss++)
            // {
            //     total += ss->second.size();
            // }
            int total=0;
            for (unsigned int n=0; n<_hash.size(); n++)
            {
                total += _hash[n].size();
            }
            ja.reserve(total);
	

            for (unsigned int n=0; n<_hash.size(); n++)
            {
                // std::set<int>::iterator nbrs = (_hash _AT(n).begin());
                // std::set<int>::iterator nbrs = _hash[n].begin();

                ia.push_back(ja.size());
                //while (nbrs != _hash[n].end())
                for (unsigned int v=0; v<_hash[n].size(); v++)
                {
                    if (_hash[n][v] == (int)n)
                    {
                        // it's the diagonal
//		    TRACE(626, "iau.size(): " << iau.size() << ", n: " << n);
                        if (do_iau)
                        {
                            assert(iau.size() == n);
                            iau.push_back(ja.size());
                        }
                    }
		
                    // ja.push_back(*nbrs);
                    // nbrs++;
                    ja.push_back(_hash[n][v]);
                }
            }
            ia.push_back(ja.size());
            _hash.clear();		// i should not need this
        }
    
    

    private:
        //std::map<int, std::set<int> > _hash; // TODO replace this with a vector
        //std::vector<std::set<int> > _hash;
        //std::vector<uniqvec<int> > _hash;
        mutable std::vector<intset> _hash;
        const ownerdb& _odb;
    };


}
#endif /* _SPLATT_SOLVER_H_ */

