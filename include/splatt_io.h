/*  -*- Mode: C++; -*-  */

#ifndef _SPLATT_IO_H_
#define _SPLATT_IO_H_

#ifdef SPLATT_CGNS
#include <cgnslib.h>
#endif  // SPLATT_CGNS


namespace splatter
{
    namespace starcd
    {
	typedef array<double,3> node_coords;
	typedef int		tet_cond_data;
	typedef int		bnd_cond_data;


	status load_all(const std::string filename_prefix, parallel_ctx pctx,
			std::vector< node_coords >&  coords,
			std::vector< int >&    tets,
			std::vector< tet_cond_data >& vcond,
			std::vector< int >&    bnds,
			std::vector< bnd_cond_data >& bcond);

	status load_nodes(std::string filename, parallel_ctx pctx,
			  std::vector<node_coords>& coords);

	status load_tets(std::string filename, parallel_ctx pctx,
			 std::vector<int>& faces,
			 std::vector<tet_cond_data>& vcond);
	
	status load_boundaries(std::string filename, parallel_ctx pctx,
			       std::vector<int>& faces,
			       std::vector<bnd_cond_data>& bcond);

    }

    // field view formats
    namespace fvuns
    {
	status write_grid(std::string filename,
			  const ownerdb& odb,
			  splatter::index* node_index,
			  splatter::index* tet_index,
			  std::vector<starcd::node_coords>& coords);
	
	status write_results(std::string filename,
			     const ownerdb& odb,
			     std::vector<std::string> vars,
			     std::vector<double>& Q);
	

    }
    
#ifdef SPLATT_CGNS
    namespace cgns
    {
        struct cgns_handle
        {
            parallel_ctx pctx;
            int          cg;
        };

        typedef array<double,3> node_coords;

        status cgns_analyze(std::string filename);

        status cgns_init(std::string filename, parallel_ctx pctx, cgns_handle* hp);
        status cgns_close(cgns_handle* hp);
        status cgns_load_coords(cgns_handle* hp, int base, int zone,
                                std::vector<node_coords>& coords);
        status cgns_load_elements(cgns_handle* hp, int base, int zone, int section,
                                  std::vector<int>& c2n,
                                  cgns_entity_type& etype,
                                  std::string& name);

        status cgns_load_all(part_mgr& mgr, std::string filename,
                             int base=1, int zone=1);

        // dumps it all to base1, zone1
        status cgns_write_all(part_mgr& mgr, std::string file_prefix);


    }
#endif  // SPLATT_CGNS



}


#endif	/* _SPLATT_IO_H_ */
