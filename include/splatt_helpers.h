//  -*- Mode: C++; -*- 
#ifndef _SPLATT_HELPERS_H_
#define _SPLATT_HELPERS_H_

#include <iomanip>
#include <sstream>
#include <vector>
#include <strings.h>

namespace splatter
{

    /** template wrapper around a standard C array, allowing them to
     *  be used in STL containers, and providing MPI types (see get_mpi_type())
     and IO formatting. */

    template <typename T, int SIZE>
    class array
    {
    public:
	/*! create of size specified in template, initialize to all zeros **/
	array()
	{
	    // TODO: decide if memset is preferable
	    bzero(_data, SIZE*sizeof(T));
	}

	/*! create copying from another array **/
	array(T init[])
	{
	    for (int i=0; i<SIZE; i++)
	    {
		_data[i] = init[i];
	    }
	}

	/*! normal entity access
	  \param c index of the entity to access
	**/
	T& operator[](int c) { return _data[c]; }

	/*! const entity access
	  \param c index of the entity to access
	**/
	const T& operator[](int c) const { return _data[c]; }

	array<T,SIZE> operator+(const array<T,SIZE>& other) const
	{
	    array<T,SIZE> rval;
	    for (int n=0; n<SIZE; n++)
	    {
		rval[n] = _data[n] + other[n];
	    }
	    return rval;
	}

	array<T,SIZE> operator-(const array<T,SIZE>& other) const
	{
	    array<T,SIZE> rval;
	    for (int n=0; n<SIZE; n++)
	    {
		rval[n] = _data[n] - other[n];
	    }
	    return rval;
	}

	template <typename T2>
	array<T,SIZE> operator*(const T2& val) const
	{
	    array<T,SIZE> rval;
	    for (int n=0; n<SIZE; n++)
	    {
		rval[n] = _data[n] * val;
	    }
	    return rval;
	}

	template <typename T2>
	array<T,SIZE> operator/(const T2& val) const
	{
	    array<T,SIZE> rval;
	    for (int n=0; n<SIZE; n++)
	    {
		rval[n] = _data[n] / val;
	    }
	    return rval;
	}

        array<T,SIZE>& operator=(const T& val)
        {
            for (int n=0; n<SIZE; n++)
            {
                _data[n]=val;
            }
            return (*this);
        }

        array<T,SIZE>& operator=(const array<T,SIZE>& val)
        {
            for (int n=0; n<SIZE; n++)
            {
                _data[n]=val[n];
            }
            return (*this);
        }

        T* raw()
        {
            return _data;
        }
        


    protected:
	T	_data[SIZE]; /*!< the actual data */
    };
    //

    
    template <typename T, int SIZE>
    std::istream& operator>>(std::istream& in, array<T,SIZE>& a)
    {
	for (int i=0; i<SIZE; i++)
	{
	    in >> a[i];
	}

	return in;
    }

    template <typename T, int SIZE>
    std::ostream& operator<<(std::ostream& out, array<T,SIZE>& a)
    {
	for (int i=0; i<SIZE; i++)
	{
	    if (i > 0) out << " ";
	    out << a[i];
	}

	return out;
    }




    // **************************************************
    


// **************************************************

    /*! turn an arbitrary value into a string using any bound stream
     * insertion capabilities
     \param v the value to stringify
     **/
    template <typename T>
    std::string stringify(T v)
    {
	std::ostringstream o;
	o << v;
	return o.str();
    }
// **************************************************


#ifndef NO_MPI
    /** simple timer using MPI_Wtime  **/
    class timer
    {
    public:
	/*! create and start a new timer **/
	timer()
	{
	    start();
	}

	/*! start the timer */
	void start()
	{
	    start_time = MPI_Wtime();
	}

	/*! get elapsed wallclock time since construction or last call to start() */
	double time()
	{
	    return MPI_Wtime() - start_time;
	}

    private:
	double start_time;
	
    };
#endif

    /** lightweight set of T values (that is, lighter in memory usage than `std::set<T>`)

	 @tparam T must be a type that has operator< defined
     **/
    template <typename T>
    class uniqvec
    {
    public:
	uniqvec() {
#ifdef SPLATT_UVEC_INIT_CAPACITY
	    _vals.reserve(SPLATT_UVEC_INIT_CAPACITY);
#endif
	}


        // this is kind of a hacky thing to do
        operator bool()
        {
            return _vals.size() > 0;
        }


	/*! insert value, not allowing duplicates
	  \param val value to be inserted
	**/
	void insert(const T& val)
	{
	    typename std::vector<T>::iterator v=_vals.begin();

	    // insert into _vals sorted ascending
	    for (; v != _vals.end(); v++) // TODO: binary search?
	    {
		if (val == *v) return;
		if (val < *v)
		{
		    _vals.insert(v, val);
		    return;
		}
	    }


	    // tack onto the end if we get this for
	    _vals.push_back(val);	    
	}


	/*! check if value is in set
	  \param val value to check for
	  \return `true` if val is in set, `false` otherwise
	**/
	bool contains(const T& val) const
	{
	    // TODO: binary search?
	    return std::find(_vals.begin(), _vals.end(), val) != _vals.end();
	}

	/*! remove entitys from set that are not in `other`
	  \param other set to intersect with
	**/
	void intersect(const uniqvec<T> other)
	{
	    unsigned int me=0, o=0, nextme=0;;
	    while (nextme<_vals.size())
	    {
		// jump over irrelevant others
		while (o<other.size() && other[o]<_vals[nextme])
		{
		    o++;
		}

		if (o == other.size())
		{
		    break;
		}

		if (_vals[nextme] == other[o])
		{
		    _vals[me]=_vals[nextme];
		    me++; nextme++;
		}
		else
		{
		    while (nextme < _vals.size() && _vals[nextme] < other[o])
		    {
			nextme++;
		    }
		}
	    }

	    _vals.resize(me);
	}


	void remove(const T& v)
	{
	    typename std::vector<T>::iterator match = std::find(_vals.begin(), _vals.end(), v);

	    if (match != _vals.end())
	    {
		_vals.erase(match);
	    }
	}

	void clear()
	{
	    _vals.clear();
	}

	/*! number of entities in set */
	size_t size() const { return _vals.size(); }

	/*! normal entity access
	  \param t index of entity to access
	**/
	T& operator[](int t) { return _vals[t]; }

	/*! const entity access
	  \param t index of entity to access
	**/
	const T& operator[](int t) const { return _vals[t]; }

    private:
	std::vector<T> _vals;
    };
    
    template <typename T>
    std::ostream& operator<<(std::ostream& out, const uniqvec<T>& v)
    {
	out << "[";
	for (unsigned int n=0; n<v.size(); n++)
	{
	    if (n>0) out << " ";
	    out << v[n];
	}

	out << "]";
	return  out;
    }


    /* minimum size that we even care about being clever resizing */
#define SPLATT_FI_SIZE_THRESH 10000

    /* allowed future percentage of global hits that should have been local */
#define SPLATT_FI_RATIO_THRESH .025

    typedef   uniqvec<int> intset;
    
    /** mapping from `int` to `intset` with a range of values
     * considered *local* so that they should exist in a contiguous
     * vector for faster reference **/
    class fast_index
    {
    public:
	fast_index() : _low(0), _high(0) {}
	fast_index(int low, int high) : _low(0), _high(0)
	{
	    update(low,high);
	}

	// update the range of ints we consider "local"
	status update(int low, int high) 
	{
	    LLOG(1, "updating a fast_index");
	    // we can fake an extended _local array, by using _global
	    // unless the proposed change is too drastic
	    int currsize = _high - _low;
	    int newlow = _low;
	    int newhigh = _high;

	    if (currsize < SPLATT_FI_SIZE_THRESH)
	    {
		newlow=low;
		newhigh=high;
	    }
	    else
	    {		
		double lowrat = abs(_low - low)*1.0/currsize;
		double highrat = abs(high - _high)*1.0/currsize;
		

		if (lowrat >SPLATT_FI_RATIO_THRESH)
		{
		    // gotta rebase the vector
		    newlow = low;
		}
		if (highrat > SPLATT_FI_RATIO_THRESH)
		{
		    newhigh = high;
		}

		
	    }

	    DEBUG("post heuristic: ("<<_low<<","<<_high<<")->(" << newlow << "," << newhigh<<")");
	    
	    bool skip_back = false;
	    if (newlow > _low)
	    {
		// copy to global and erase
		for (int n=_low; n < newlow && n<_high; n++)
		{
		    if (_local[n-_low].size() > 0)
		    {
			_global[n]=_local[n-_low];
		    }

		}

		if (newlow > _high)
		{
		    skip_back=true; // disjoint sets, so we don't need to work on the back
		    _local.resize(newhigh-newlow);
		    // invalidate entire local index
		    for (unsigned int i=0; i<_local.size(); i++)
		    {
			_local[i].clear();
		    }
		}
		else
		{
		    _local.erase(_local.begin(), _local.begin()+(newlow-_low));
		}	
	    }
	    else if (newlow < _low)
	    {
		// make room and move from global

		if (newhigh < _low)
		{
		    skip_back=true; // disjoint sets, so we don't need to work on the back

                    for (unsigned int i=0; i<_local.size(); i++)
                    {
                        if (_local[i].size() > 0)
                        {
                            _global[i+_low]=_local[i];
                        }

                        // invalidate entire local index
                        _local[i].clear();
                    }

		    _local.resize(newhigh-newlow);
		}
		else
		{
		    _local.insert(_local.begin(), _low-newlow, intset());
		}
	    }

//	    DEBUG("front tacked on: " << skip_back);
	    
	    if (! skip_back)
	    {
		if (newhigh < _high)
		{
		    // copy to global and erase
		    for (int n=newhigh; n<_high; n++)
		    {
			if (_local[n-newlow].size() > 0)
			{
			    _global[n]=_local[n-newlow];
			}
		    }

		    if (newhigh-newlow < (int)_local.size())
		    {
			//	_local.erase(_local.begin()+newhigh-_low, _local.end());
			_local.erase(_local.begin()+(newhigh-newlow), _local.end());
		    }
		}

		if (newhigh > _high)
		{
		    // resize, copy from global
		    _local.insert(_local.end(), newhigh-_high, intset());
		}
	    }
	    
	    _low = newlow;
	    _high = newhigh;

	    // move from global into local
	    std::map<int,intset>::iterator glob;
	    for(glob = _global.begin(); glob != _global.end(); )
	    {
		bool erase_me = false;

		if (glob->first >= _low && glob->first <_high)
		{
		    _local[glob->first - _low] = glob->second;
		    erase_me = true;
		}
		else if (glob->second.size() == 0)
		{
		    erase_me = true;
		}

		if (erase_me)
		{
		    std::map<int,intset>::iterator nextmatch = glob;
		    nextmatch++;
		    _global.erase(glob);
		    glob=nextmatch;
		}
		else
		{
		    glob++;
		}
	    }



//	    DEBUG("fast_index updated: " << _low << "-" << _high << ", " << _local.size());
	    return SPLATT_OK;
	}

	intset& operator[](int g) 
	{
	    if (g >= _low && g < _high)
	    {
		return _local[g-_low];
	    }
	    else
	    {
		return _global[g];
	    }
	}

	const intset& operator[](int g) const
	{
	    if (g >= _low && g < _high)
	    {
		return _local[g-_low];
	    }
	    else
	    {
		std::map<int,intset >::const_iterator match = _global.find(g);
		if (match == _global.end())
		{
		    return _nomatch;
		}
		else
		{
		    return match->second;
		}

	    }
	}

	void clear()
	{
	    _low=0;
	    _high=0;
	    _local.clear();
	    _global.clear();
	}


	



	/** erase unused global structures **/
	void purge(){
	    std::map<int,intset>::iterator curs = _global.begin();

	    while (curs != _global.end())
	    {
		if (curs->second.size() == 0)
		{
		    std::map<int,intset>::iterator x = curs;
		    curs++;
		    _global.erase(x);
		}
		else
		{
		    curs++;
		}
	    }
	}

    private:
	friend class  explicit_index;

	int _low, _high;
	intset _nomatch;
	std::vector<intset >   _local;
	std::map<int, intset > _global;
    };
    

    inline bool hash_contains(const fast_index& fi, const int* ids, int num)
    {
	assert(num > 0);
	intset match = fi[ids[0]];
	int n=1;
	
	while (match.size() > 0 && n<num)
	{
	    match.intersect(fi[ids[n]]);
	    n++;
	}

	return match.size() > 0;
    }


    inline void hash_insert(fast_index& fi, int tag, const int* ids, int num)
    {
	for (int n=0; n<num; n++)
	{
	    fi[ids[n]].insert(tag);
	}
    }

    inline int hash_find(const fast_index& fi, const int* ids, int num)
    {
	assert(num>0);
	intset match=fi[ids[0]];
	int n=1;

	while (match.size() > 0 && n<num)
	{
	    match.intersect(fi[ids[n]]);
	    n++;
	}

#ifdef SPLATT_DEBUG
	if (match.size() > 1)
	{
	    for (int n=0; n<num; n++)
	    {
		std::cout << ids[n] << " ";
	    }
	    std::cout << std::endl;
	    
	    
	    ERROR("found more than one match: " << match << " for input  of size " << num);
	}
#endif
	if (match.size() == 0)
	{
	    return -1;
	}
	else
	{
	    return match[0];
	}
    }

    class picky_matcher
    {
    public:
        picky_matcher() : _count(0) {}

        picky_matcher(int count, const int* nodes) : _count(count)
        {
            for (int c=0; c<_count; c++)
            {
                _node_counter[nodes[c]]++;
            }
        }

        bool distinct() const
        {
            return (int)_node_counter.size() == _count;
        }

        bool match(const int* n2) const
        {
            for (std::map<int,int>::const_iterator curs=_node_counter.begin();
                 curs != _node_counter.end(); curs++)
            {

                int target = curs->first;
                int nc = 0;
                for (int n=0; n<_count; n++)
                {
                    if (n2[n] == target)
                    {
                        nc++;
                    }
                }

                if (nc != curs->second)
                {
                    return false;
                }
            }

            return true;        
        }


    private:
        int _count;
        std::map<int,int> _node_counter;
    };
    


    // defined in splatt_mem.cpp
    void get_mem_usage(double* vm_usage, double* resident_set);
    void log_sys_mem();
}



#endif	// _SPLATT_HELPERS_H_
