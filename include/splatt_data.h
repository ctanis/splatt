/*  -*- Mode: C++; -*-  */

#ifndef _SPLATT_DATA_H_
#define _SPLATT_DATA_H_

namespace splatter
{
    class proxy_monitor
    {
    public:
	virtual void moving(int,int)=0;
    };
    


    /*! Proxy for user data */
    class data_proxy
    {
    public:
	data_proxy(int flags=0);
	virtual ~data_proxy();	

        /*! \returns size of data being proxied */
	virtual int size()=0;

        /*! \returns number of data elements per entity */
	virtual int size_per() = 0;
	
        /*! Removes all data contents (be careful!) */
        virtual void clear() = 0;
        
        /*! \returns pointer to underlying data (be careful!) */
	virtual void* raw() = 0;
	
        /*! Resize underlying data
          \param s number of elements to resize for
          \returns success or failure */
	virtual status resize(unsigned int s)=0;

        /*! Insert slots for new data
          \param n number of slots to insert
        */
	virtual void insert(int n) = 0;
	
        /*! Insert 1 slot for new data */
	virtual void add_one()
	{
	    insert(1);
	}
	
        /*! Apply the `batch_migrate` protocol.
           \param args protocol args
           \returns success or failure */
	virtual status batch_migrate(const migrate_args& args,
				     proxy_monitor* m=NULL)=0;

        /*! Apply the `deliver` protocol.
           \param args protocol args
           \param out optional proxy for collected data
           \returns pointer to collecting proxy
        */
	virtual data_proxy* deliver(const deliver_args& args, data_proxy* out=NULL)=0;


        /*! Apply the `sync` protocol.
           \param pctx the `parallel_ctx` to use
           \param args protocol args
          \returns success or failure
        */
        virtual status sync(parallel_ctx pctx, const sync_args& args)=0;

	/*! Apply the `reorder protocol.
          \param localids new local ids for all local data
          \returns success or failure */
	virtual status reorder(const std::vector<int>& localids)=0;

        /*! Load data from restart file stream.
          \param in the restart file stream.
        */
	virtual status load(std::ifstream& in)=0;

        /*! Save data to a restart file stream.
          \param out the restart file stream.
        */
	virtual status save(std::ofstream& out, int limit=-1)=0;

	// logging
	virtual std::string dump()=0;

        /*! Create a new `data_proxy` capable of storing the same kind of data
          \returns the type clone
        */
        virtual data_proxy* type_copy()=0;
        /*!  Forcibly copy data between proxies, assumign they are type compatible
         \param dest position in local data
         \param src  position in source data
         \param src_proxy data proxy containing source data
        */         
        virtual void  store_raw(int dest,int src, data_proxy* src_proxy)=0;
        
        

    protected:
	int		_flags;
    };
    

    /*!
      a data_wrapper for standard template vectors
    */
    template< typename T > class data_proxy_std;


    // return value must be deleted
    template <typename T>
    data_proxy* proxy(std::vector<T>& vec, int size_per=1, int flags=0)
    {
	return new data_proxy_std<T>(vec, size_per, flags);
    }

}



#include "inline/data_cpp.h"

#endif	// _SPLATT_DATA_H_
