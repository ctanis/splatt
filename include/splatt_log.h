//  -*- Mode: C++; -*- 
#ifndef _SPLATT_LOG_H_
#define _SPLATT_LOG_H_


#include <iostream>
#include <iomanip>
#include <cassert>

#ifdef SPLATT_TRACE
#define TRACE(C, msg) if (SPLATT_TRACE == C) \
    { std::cout << "T(" << __FILE__ << ":"  << __LINE__ << " " \
		<< __FUNCTION__ << ")" << std::setprecision(15) \
		<< msg << std::endl; } else { DEBUG(msg); }



#else
#define TRACE(C, msg)
#endif


#ifdef SPLATT_DEBUG

#ifndef SPLATT_LOG_LEVEL
#define SPLATT_LOG_LEVEL 10
#endif	// SPLATT_LOG_LEVEL

//#define DEBUG(msg) std::cerr << "debug: " << msg << std::endl
#define DEBUG(msg) std::cout << "(" << __FILE__ << ":"  << __LINE__ << " " << __FUNCTION__ << ")" << std::setprecision(15) << msg << std::endl
#define LOG(msg) std::cout << "(" << __FILE__ << ":" << __LINE__<< " " << __FUNCTION__ << ")" << std::setprecision(15) << msg << std::endl
#define ERROR(msg) std::cout << "ERROR<<" << __FILE__<<":"<< __LINE__<<" "<<__FUNCTION__ << ">>" << std::setprecision(15) << msg << std::endl; \
    assert(false)

#define ERROR_NO_ABORT(msg) std::cout << "ERROR<<" << __FUNCTION__ << ">>" << std::setprecision(15) << msg << std::endl;


#define LLOG(lvl,msg) if ((lvl)<=SPLATT_LOG_LEVEL)  { std::cout << "<<" << __FILE__<<":"<<__LINE__<<" "<<__FUNCTION__ << ">>" << std::setprecision(15) << msg << std::endl; }


#else

#ifndef SPLATT_LOG_LEVEL
#define SPLATT_LOG_LEVEL -1
#endif


#define DEBUG(msg)
#define LOG(msg) std::cout << "(" << __FUNCTION__ << ")" << std::setprecision(15) << msg << std::endl
#define ERROR(msg) std::cout << "ERROR<<" << __FUNCTION__ << ">>" << std::setprecision(15) << msg << std::endl; \
     \
    MPI_Abort(MPI_COMM_WORLD, -1)

#define ERROR_NO_ABORT(msg) std::cout << "ERROR<<" << __FUNCTION__ << ">>" << std::setprecision(15) << msg << std::endl;

#define LLOG(lvl,msg) if ((lvl)<=SPLATT_LOG_LEVEL)  { std::cout << "<<" << __FUNCTION__ << ">>" << std::setprecision(15) << msg << std::endl; }


#endif





#ifdef LINUX
// #define LOG_MEM { double vm, resident; get_mem_usage(&vm,&resident); LOG("process memory: " << vm << "; " << resident); log_sys_mem(); }
// #ifdef LOG_MEM_FULL
// #define LOG_MEM { double vm, resident; get_mem_usage(&vm,&resident); LOG("process memory: " << vm << "; " << resident); log_sys_mem(); }
// #else
//#define LOG_MEM { double vm, resident; get_mem_usage(&vm,&resident); LOG("process memory: " << vm << "; " << resident); }
#define LOG_MEM(n, msg) { double vm, resident; get_mem_usage(&vm,&resident); TBL(n, msg << " vm", vm); }
#else
#define LOG_MEM(n,msg)
#endif

#define TBL(n, s,v) std::cout << "__tbl." << n << "|"<< s << "|" << v << std::endl;




#endif	// _SPLATT_LOG_H_
