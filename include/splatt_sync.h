//  -*- Mode: C++; -*- 
#ifndef _SPLATT_SYNC_H_
#define _SPLATT_SYNC_H_

namespace splatter
{
    class comm_buffer
    {
    public:
	comm_buffer() : sendbuf(NULL), bufsize(0)
	{
	}
	

	~comm_buffer()
	{
	    if (sendbuf != NULL)
	    {
		LLOG(2,"freeing up comm buffer " << bufsize);
		free(sendbuf);
	    }
	}
	
	    
	void* operator()(size_t size) const
	{
	    if (size > bufsize)
	    {
		LLOG(2,"(re)allocating a new comm buffer " << bufsize << "->" << size);
		sendbuf = realloc(sendbuf, size);
		bufsize = size;
	    }
	    else
	    {
		LLOG(2, "reusing an old comm buffer " << bufsize << "->" << size);
	    }
	    if ((sendbuf == NULL) && (size > 0))

	    {
		ERROR("cannot allocate communication buffer of size " << size);
		return NULL;
	    }

	    LLOG(2, "allocation succeeded");
	    return sendbuf;
	}
	

    protected:
	mutable void*  sendbuf;
	mutable size_t bufsize;
    };
    

    class sync_args
    {
    public:
	sync_args() {}

	sync_args(int np) : in_sizes(np),
			    in_displs(np+1),
			    out_sizes(np),
			    out_displs(np+1),
			    out_ids(np)
	{
	}

	void* buffer(size_t eltsize) const
	{
	    return buff(eltsize * out_ids.size());
	}

	int required_size() const
	{
	    int total= in_displs[0];
	    for (unsigned int r=0; r<in_sizes.size(); r++)
	    {
		total += in_sizes[r];
	    }

	    return total;
	}
	    


	// how many entities do we expect from each rank
	std::vector<int>	in_sizes;

	// where should each rank put their entities (locally)
	std::vector<int>	in_displs;

	// how many entities are we sending to each rank
	std::vector<int>	out_sizes;

	// from whence should we get each ranks data (locally)
	std::vector<int>	out_displs;

	// what entities are we storing in the outgoing buffer
	// note: buffer must be size_per_element * out_ids.size()
	std::vector<int>	out_ids;


	// sync raw pointer, assuming x is big enough
	template <typename T> status sync_raw(parallel_ctx pctx, T* x, int size_per=1) const;
	


    private:
	comm_buffer	buff;
    };


    class migrate_args
    {
    public:
	migrate_args(parallel_ctx pctx) :
	    to_move(pctx.np()),
	    in_sizes(pctx.np()),
	    pctx(pctx),
	    guaranteed_sorted(false),
	    prepared(false)
	{
	}
	
	status prepare()
	{
	    if (pctx.np() > 1)
	    {
		std::vector<int> outsizes(pctx.np());
		for (unsigned int r=0; r<outsizes.size(); r++)
		{
		    outsizes[r] = to_move[r].size();
		}

                TRACE(876, "about to barrier");
                MPI_Barrier(MPI_COMM_WORLD);
                TRACE(876, "about to alltoall");
		MPI_Alltoall(&outsizes[0], 1, MPI_INT,
			     &in_sizes[0], 1, MPI_INT,
			     MPI_COMM_WORLD);
                TRACE(876, "finishd alltoall");
	    }
	    else
	    {
		in_sizes[0]=0;
	    }
	    
	    if (! guaranteed_sorted)
	    {
		LLOG(2, "sorting deletions and making them unique: " <<
		     to_delete.size());
		std::sort(to_delete.begin(), to_delete.end());
		to_delete.erase(std::unique(to_delete.begin(), to_delete.end()),
				to_delete.end());
	    }
	    
#if 2 <= SPLATT_LOG_LEVEL
            LOG("prepared args for batch_migrate: ");
            for (int p=0; p<pctx.np(); p++)
            {
                LOG("rank " << p << " -- in " << in_sizes[p] << "; out " << to_move[p].size());
            }
#endif

	    prepared=true;
	    return SPLATT_OK;
	}

	// must call prepare() first
	void* recv_buffer(size_t eltsize) const
	{
	    int total=0;
	    for (unsigned int i=0; i<in_sizes.size(); i++)
	    {
		total += in_sizes[i];
	    }
	    
	    return recvbuf(eltsize * total);
	}


	void* send_buffer(size_t eltsize) const
	{
	    int total=0;
	    for (unsigned int i=0; i<to_move.size(); i++)
	    {
		total += to_move[i].size();
	    }

	    return sendbuf(eltsize * total);
	}
	

	// which entities should be deleted
	std::deque<int>			to_delete;

	// which entities go where
	std::vector<std::deque<int> >	to_move; // grouped by destination rank


	// how much to expect from each rank
	std::vector<int>		in_sizes;


	parallel_ctx			pctx;
	bool				guaranteed_sorted;

	bool				prepared;
    private:
	comm_buffer			sendbuf;
	comm_buffer			recvbuf;

    };
    
    //typedef std::deque<int> delivery_id_coll;
    typedef std::set<int> delivery_id_coll;
    

    class deliver_args
    {
    public:
	deliver_args(const parallel_ctx& pctx) : ids_per_rank(pctx.np()),
						 in_sizes(pctx.np()),
						 pctx(pctx),
						 prepared(false)
	{}

	status prepare()
	{
	    std::vector<int> outsizes(pctx.np());
	    for (unsigned int r=0; r<outsizes.size(); r++)
	    {
		outsizes[r]=ids_per_rank[r].size();
	    }

	    MPI_Alltoall(&outsizes[0], 1, MPI_INT,
			 &in_sizes[0], 1, MPI_INT,
			 pctx.comm());

	    prepared=true;
	    return SPLATT_OK;
	}

	delivery_id_coll& operator[](int l)
	{
	    return ids_per_rank[l];
	}

	const delivery_id_coll& operator[](int l) const
	{
	    return ids_per_rank[l];
	}


	void clear()
	{
	    ids_per_rank.clear();
	    ids_per_rank.resize(pctx.np());
	    in_sizes.clear();
	    in_sizes.resize(pctx.np());
	    prepared=false;
	}
	



	// call prepare first...
	void* send_buffer(size_t eltsize) const
	{
	    int total=0;
	    for (unsigned int i=0; i<ids_per_rank.size(); i++)
	    {
		total += ids_per_rank[i].size();
	    }

	    return sendbuf(eltsize * total);
	}
	
	int recv_size() const
	{
	    int total=0;
	    for (unsigned int i=0; i<in_sizes.size(); i++)
	    {
		total += in_sizes[i];
	    }
	    
	    return total;
	}


//	status exchange_nodes(std::vector<int>& results);


	std::vector< delivery_id_coll >	ids_per_rank;

	// how much to expect from each rank
	std::vector<int>		in_sizes;
	const parallel_ctx&		pctx;
	bool				prepared;

    private:
	comm_buffer			sendbuf;
    };
    


}

#include "inline/sync_cpp.h"

#endif	// _SPLATT_SYNC_H_
