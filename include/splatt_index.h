/*  -*- Mode: C++ -*-  */

#ifndef _SPLATT_INDEX_H_
#define _SPLATT_INDEX_H_


namespace splatter
{

    /*! internal flag to fake virtual template functions */
    enum index_type 
    {
	SPLATT_IMPLICIT,
	SPLATT_EXPLICIT
//	SPLATT_LOCAL,
//	SPLATT_HYBRID,
//	SPLATT_CSR
    };

    // subclasses of index defined below...
    class implicit_index;
    class explicit_index;
    class temp_index;
    

    /*! \brief Superclass for other indices; collection of mesh entities.
     */
    class index
    {
    public:
	virtual ~index();
	
	/*! Associate data from a data_proxy with this index.  Changes to the
	  index (through the appropriate means) will be mirrored in the
	  associated data.
	  
	  NOTE: index destructor will delete the proxy.

	  \param name identifying tag for the attached data
	  \param p pointer to proxy
	  \returns success or failure
	*/
	status attach_data(std::string name, data_proxy* p);

	/*! Remove associated data, deleting proxy.
	 */


	status detach_data(std::string name);
	


	/*! Access associated data.

	  \param name identifying tag of requested data
	  \param orig model data type (it's complicated...)
	*/
	template <typename D>  D& data(std::string name, const D& orig);

	void* data_raw(std::string name);
	
	data_proxy* dproxy(std::string tag);
	

// ---------------------------------------------------------------------------
// basic accessors

	/*! \returns identifying name used for this index (used in mesh queries) */
	std::string	name() const; // inline

	/*! \returns type of actual index value */
	index_type	type() const; // inline

	/*! \returns flags used to configure the index */
	int		flags() const; // inline

	/*! \returns the number of entities in the index */
	virtual int	size() const; // inline


	

// ---------------------------------------------------------------------------
// APPLY & co.

	/*! Apply op to each entity in this index, with the positional id's specified by the iterator
	  \param op see index_op
	  \param start iterator to beginning face id
	  \param end end iterator
	*/
	template <typename OP, typename IT> void apply(const OP& op, IT start, IT end);

	/*! Apply op to every entityt in this index
	  \param op see index_op
	*/
	template <typename OP> void apply(const OP& op);
	
	/*! Apply some index_op - esque member function to each entity in this
	 *  index, with the positional id's specified by the iterator
	  \param op see index_op
	  \param start iterator to beginning face id
	  \param end end iterator
	*/
	template <typename OP, typename IT, void (OP::*func)(index*,int,int,int,int*) const>
	void apply(const OP& op, IT start, IT end);


	/*! Apply some index_op - esque member function to every entity in this
	  index
	  \param op see index_op
	*/
	template <typename OP, void (OP::*func)(index*,int,int,int,int*) const>
	void apply(const OP& op);


    protected:
	friend class part_mgr;

	/*! Superclass constructor for all index instances.
	  \param reference to ownership authority
	  \param name identifying tag for this index
	  \param type index_type for this index (for dispatching non-virtual template functions)
	  \param flags index creation flags
	*/
	index(const ownerdb& odb, std::string name, index_type type, int flags);



	/*! pure virtual method for renumbering entities according to
	 *  local_gid, and potentially redistributing nodes according to
	 *  new_odb
	 */
	virtual status renumber(const ownerdb& new_odb,
				const std::vector<int>& local_gid,
				std::map<int,int>& global_map)=0;

	const ownerdb&		_odb;

	std::string	_name;
	index_type	_type;
	int		_flags;

	std::vector<std::string>	_data_names;
	std::vector<data_proxy*>	_data;
    };
    


    /*! index subclass for collections of single-node entities, stored as a
     *  range of node ids -- specifically the range of nodes owned by the
     *  current rank and potentially any phantom entities

     This is really just used for the master node index, but i guess it could
     conceivably be used for something else...
     */
    class implicit_index : public index
    {
    public:
	/*! Constructor: called by `part_mgr` */
	// implicit_index(const ownerdb& odb,
	// 	       std::string name, int node_type, int flags=0);

	implicit_index(const ownerdb& odb) : index(odb, "nodes", SPLATT_IMPLICIT, 0) { }
	


        /*! \returns number of nodes represented by this index
	  (just nlocal!)
	 */
	int size() const;	// inline
	

    private:
	friend class index;
	friend class part_mgr;

	/*! moves all associated data so that the index appears to have been renumbered
	 */
	status renumber(const ownerdb& new_odb,
			const std::vector<int>& newgids,
			std::map<int,int>& global_map);

	/*! synchronize all attached data, according to args
	  see part_mgr::sync_node_data */
	status sync(const parallel_ctx& pctx, const sync_args& args);

	template <typename T>
	status sync_one(const parallel_ctx& pctx, const sync_args& args,
		    std::vector<T>& data, int size_per);

	status sync_one(const parallel_ctx& pctx, const sync_args& args, std::string tag);
	

	
	
	/*! throw away all phantom data */
	status revert_to_local(int nlocal);

	/*! call some OP::*func on each node provided by iterator -- see index::apply */
	template <typename OP, typename IT, void (OP::*func)(index*,int,int,int,int*) const>
	void do_apply(const OP& op, IT start, IT end);

	/*! call some OP::*func on every node -- see index::apply */
	template <typename OP, void (OP::*func)(index*,int,int,int,int*) const>
	void do_apply(const OP& op);

	int _etype;
    };
    

// include query-related class to augment explicit_index
#include "inline/exp_index_query_cpp.h"


    /*!  An index of homogeneous entities and
     *  associated data.  Potentially (probably) hashes node positions for
     *  fast lookup.
     */
    class explicit_index : public index
    {
    public:
	/*! Constructor: called by `part_mgr` */
	explicit_index(const ownerdb& odb,
		       std::string name, int width, int entity_type,
		       std::vector<int>& nodes, int flags=0);
		
	/*! Constructor: called by `part_mgr` */
	explicit_index(const ownerdb& odb,
		       std::string name, int width, int entity_type,
		       int flags=0);


	~explicit_index();


        /* Load external data as node ids into index.
           \param data node ids to load
           \param flags flags for configuring index behavior
           \returns success or failure
        */
	status load_nodes(std::vector<int>& data, int flags=0);
	


	/*! \returns number of entities stored in index */
	int size() const;	// inline

	/*!
	  unchecked (raw) entity lookup

	  \param f desired entity position
	  \returns const int* to nodes belonging to entity at position f
	*/
	const int* operator[](int f) const // inline
	{
	    return &_nodes[f*_width];
	}


	/* Exchange  arbitrary data, storing incoming remote data in a temp_index

	   \param args specification of requested data (see deliver_args)
	   \param tmpout storage for incoming data
	   \returns success or failure
	*/
	status deliver(const deliver_args& args, temp_index& tmpout, bool also_data=true);
	
	/* Replace nodes in entity with new nodes.  Respects fast lookup hash, if necessary.

	   \param f position of entity to replace
	   \param nodes new node ids for this entity
	   \returns success or failure
	*/
	status replace(int f, const int* nodes);
	
	/* Find position of entity with nodes.
	   \param etype type of entity to look for (must be appropriate for this index)
	   \param nodes nodes of desired entity
	   \returns position of requested entity, or -1 if cannot be found
	*/
	int find(int etype, const int* nodes);
	intset findall(int nnodes, const int* nodes);
	
	
	/*! Enable fast lookup for this index */
	status build_hash();

	
	/*! Add a single entity to this index -- respects fast lookup hash if necessary
	  NOTE: user must ensure that data is extended appropriately.

	  \param newface_nodes nodes to use for the new entity
	  \returns position of new entity
	*/
	inline int add_face(const int* newface_nodes);


	/*! Figure out which faces contain a node -- hash must be enabled

	   \param g global id of desired node
	   \returns const reference to intset of related faces
	*/
	const intset& faces(int g) const { return _node2faces[g]; }

	/*! \returns number of nodes in this index's entity type  */
	int width() const { return _width; }

	/*! \returns raw pointer to indexed nodes */
	//const int* nodes() const { return &_nodes[0]; }
	const std::vector<int>& nodes() const { return _nodes; 	}
	

	/*! \returns whether fast lookup hash is enabled for this index */
	bool is_hashed() { return _live_hash; }


        /*! Specify whether this index should delete local nodes when no longer needed */
        void set_purge(bool p);
        

	/*! \returns const reference to the fast lookup hash */
	const fast_index& hash() { return _node2faces; }

	/*! Used to apply a query to this index -- has to be public, but don't
	 *  call it directly */
	template <typename Q_OP > void do_query(const Q_OP& op, int specific=-1);
	

	/*! Used to apply a query to this index, faces pulled by iterator */
	template <typename Q_OP, typename IT>
	void do_query_it(const Q_OP& op, IT begin, IT end);
	

	/*! <b>Do not call outside a query.</b>
            \returns a query object for searching this index quickly */
	exp_index_searcher get_query_searcher()
	{
//	    LLOG(1, "called for " << _name );
	    if (! _live_hash)
	    {
		LLOG(0, "spontaneously building hash for " << _name);
		build_hash();
	    }

//	    assert(_live_hash);
	    return exp_index_searcher(this, _nodes, _node2faces, _etype, _width);
	}
	

	

	/*! <b>Globally</b> delete all faces with ids iterated over between start and end
         \param start beginning iterator to container of entity ids to delete
         \param end end iterator of container
         \returns success or failure
         */
	template <typename IT>
	status delete_faces(IT start, IT end);

        // /*! __Globally__ delete all faces, not trusting the hash to be picky enough.  Use this with no-type indices that may have repeated node-ids */
        // template <typename IT>
	// status delete_faces_careful(IT start, IT end);


	/*! Ensures that all entities are stored on processes that need them
	 * (based on ownership of underlying entity nodes).
         \param clobber_duplicates check and purge local entities that are duplicates
	 */
	status sync_phantom_layer(bool clobber_duplicates=true);

        /*! \returns entity type for this index */
	int etype() const { return _etype; }

	/*! Expensive debugging/validation method <b>Do not call.</b>*/
	status validate_hash();

	/*! Expensive debugging/validation method <b>Do not call.</b>*/
	status validate_entities();
	

        const int* const local_nodes() const;
        const std::vector<int>& local_ownership() const;

        /*! assign each face a unique id such that no faces with the same
         *  color share nodes
         \param colors out vector containing new color tags
         \returns number of colors assigned
         */
        int color_faces(std::vector<int>& colors,
                        std::vector<int>& color_count) const;

        /*! reorder index so that same-face tags are contiguous.
          \param tags per-face numeric tags
          \param num_tags number of tags
        */
        void group_by(std::vector<int>& tags,
                      std::vector<int>& tag_counts,
                      int num_tags);

        void group_by_color();

        /*! get compressed group tag vector */
        const std::vector<int>& groups() { return _order_groups; }

    protected:
	friend class index;
	friend class part_mgr;
	friend class temp_index;

	status save(std::ofstream& out);
	status load(std::ifstream& in);


	/*! move faces according to migrate_args -- used mostly for deleting??  dangerous! */
	virtual status do_migrate(migrate_args& args);



	/* Renumbers all stored entities according to translation of all nodes, in parallel..
	   Moves all entities to appropriate new owner, along with attached data.

	   \param new_odb alternate node ownership to use for this move
	   \param newgids new global ids for locally owned nodes
	   \param global_map cache of remote node lookups
	*/
	virtual status renumber(const ownerdb& new_odb,
				const std::vector<int>& newgids, std::map<int,int>& global_map);


	// call some other OP func on each face in range -- called by index::apply
	template <typename OP, typename IT, void (OP::*func)(index*,int,int,int,int*) const>
	void do_apply(const OP& op, IT start, IT end);

	// call some other OP func on each face in range  -- called by index::apply
	template <typename OP, void (OP::*func)(index*,int,int,int,int*) const>
	void do_apply(const OP& op);

	// remove thse faces from the hash -- looking for iterators
	template <typename T> void unindex_faces(T start, T end);
	// add these faces to the hash  -- looking for iterators
	template <typename T> void index_faces(T start, T end);
	
	// range of int indices (nodes within faces) to reindex
	void index_faces_nodes(int start, int end);
	

	const int		_etype;
	const int		_width;


	std::vector<int>&	_nodes;
	data_proxy_std<int>	_nodeproxy; // used to treat these vector of nodes like data

	fast_index		_node2faces;

	bool			_live_hash;

	bool			_deliver_during_renumber; // send entities to new owning ranks
	bool			_purge_during_renumber; // delete moved entities when no longer relevant

	std::vector<int>	_saved_nodes; // only used if we are not
					      // stealing nodes
	

        std::vector<int>        _order_groups;


        mutable bool                    _local_is_dirty;  
        mutable std::vector<int>        _local_nodes;
        mutable std::vector<int>       _local_ownership;

    };
    

    #include "splatt_temp_index.h"

    /*! \brief Functionality required for use in `index::apply`

      Any functor used in apply must have something with this operator (It is not required
      that this class be  explicitly extended)
    */
    class index_op
    {
    public:
	/*!  This operator will be called on each member of the index to which
	  it's applied.  Due to the way it's done with templates, it can be
	  optimized into direct manipulation of the index -- Compile-time
	  polymorphism!

	  \param idx index being applied to
	  \param eltno id of current entity
	  \param etype type of current entity
	  \param nnodes number of nodes in current entity
	  \param nodes actual nodes of current entity
	 */
	virtual void operator()(splatter::index* idx, int eltno, int etype,
				int nnodes, int* nodes) const =0;

    };

}

#include "inline/index_cpp.h"
#include "inline/apply_cpp.h"
// #include "inline/__iterator_cpp.h"

#endif	// _SPLATT_INDEX_H_
