#ifndef _SPLATT_DEBUG_H_
#define _SPLATT_DEBUG_H_

namespace splatter
{
    void validate_phantom_layer(part_mgr& mgr, std::string index_name);
}


#endif	// _SPLATT_DEBUG_H_
