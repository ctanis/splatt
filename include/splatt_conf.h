//  -*- mode: c++; -*- 

#ifndef _SPLATT_CONF_H_
#define _SPLATT_CONF_H_

// #include <splatter.h>
#include <splatt_log.h>
#include <map>
#include <string>
#include <algorithm>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdexcept>

#include <sys/types.h>
#include <sys/stat.h>

namespace splatter
{
    // conf stuff from the old claw days

    namespace conf
    {

        // for converting to/from strings
        template <typename T>
        std::string
        inline stringify(T v)
        {
            std::ostringstream o;
            o << v;
            return o.str();
        }

        template <typename T>
        void
        unstringify(T& v, const std::string s)
        {
            std::istringstream i(s);
            i >> v;
            return;
        }

        template <typename T>
        T
        unstringify(const std::string s)
        {
            std::istringstream i(s);
            T v;
            i >> v;
            return v;
        }

        namespace _ti		// treemap internal
        {
            // should somehow make this settable?
            const char KEY_DELIM=':';

            static char lowerchar(char c) { return (char)tolower(c); }

            inline std::string keytok(std::string& key)
            {
                // get chunk up to EOS or Conf::KEY_DELIM
                std::string rval="";
                std::size_t first_delim = key.find_first_of(KEY_DELIM);

                if (first_delim == std::string::npos)
                {
                    key.swap(rval);
                }
                else
                {
                    rval = key.substr(0, first_delim);
                    key = key.substr(first_delim+1);
                }


                // lowercase it
                std::transform(rval.begin(), rval.end(), rval.begin(),
                               lowerchar);


                // return chunk
                return rval;
            }


        }


        template <typename T>
        class treemap
        {
        public:
            class KeyNotFoundException : public std::runtime_error
            {
            public:
                KeyNotFoundException() : std::runtime_error("key not found")
		{ }

                KeyNotFoundException(std::string msg) :
                    std::runtime_error("key not found \""+msg+"\"")
		{ }
	
            };


            T& value(const std::string& path, bool create=true) {
                treemap<T>* subreg = subtreemap(path, create);

                if (subreg)
                {
                    return subreg->_value;
                }
                else
                {
                    throw KeyNotFoundException(path);
                }
            }


            treemap* subtreemap(std::string path, bool create=false) {

                treemap* curs=this;
                std::string nextkey;

                while (path.length() > 0)
                {
                    nextkey = _ti::keytok(path);
                    typename std::map< std::string, treemap<T> >::iterator
                        match=curs->_children.find(nextkey);

                    if (match == curs->_children.end())
                    {
                        if (create)
                        {
                            // [] syntax inserts new child
                            curs = &(curs->_children[nextkey]);
                        }
                        else
                        {
                            return NULL;
                        }
                    }
                    else
                    {
                        curs = &(match->second);
                    }
                }

                return curs;	
            }

        private:
            T _value;
            std::map < std::string, treemap<T> > _children;
        };

        // conf value types

        class RValue
        {
        public:
            RValue() : _next(NULL) { }
	    virtual ~RValue() {
		if (_next != NULL) {
		    delete _next;
		}
	    }
	    
            virtual int toInt() const { return (int)((double)*this); }
            virtual double toDouble() const { return (double) *this; }

            virtual operator double() const=0;
            operator std::string() const { return strval(); }

            virtual void set(std::string s)=0;
            virtual std::string strval() const=0;

	    const RValue* next() const { return _next; }

	    friend class RValueHandle;

	protected:
	    RValue* _next;
        };

        // TODO: move to cpp file?
        inline std::ostream& operator<<(std::ostream& o, const RValue& v) {

            // experimental 'linked list' version..
	    // const RValue* p = &v;
	    // while (p)
	    // {
	    // 	o << p->strval();
	    // 	p = p->next();
	    // }
            // return o;

	    return o << v.strval();
		
	    
        }


        class RString : public RValue
        {
        public:
            RString(std::string sval) : _s(sval) { }

            int toInt() const { return unstringify<int>(_s); }
            operator double() const { return unstringify<double>(_s); }

            void set(std::string s) { _s = s; }
            std::string strval() const { return _s; }

        protected:
            std::string _s;
        };


        class RDouble : public RValue
        {
        public:
            RDouble(double dval) : _d(dval) { }

            operator double() const { return _d; }

            void set(std::string s) { _d = unstringify<double>(s); }
            std::string strval() const { return stringify(_d); }

        protected:
            double _d;
        };


        class RInt : public RValue
        {
        public:
            RInt(int ival) : _i(ival) { }

            int toInt() const { return _i; }
            operator double() const { return (double)_i; }

            void set(std::string s) { _i = unstringify<int>(s); }
            std::string strval() const { return stringify(_i); }

        protected:
            int _i;
        };


        // end types


        class RValueHandle
        {
        public:
            RValueHandle() : _ptr(NULL) { }
            ~RValueHandle() { delete _ptr; }

            RValue* ptr() { return _ptr; }

            // mildly guarded copy constructor
            RValueHandle(const RValueHandle& h) {
                assert(h._ptr == NULL);
                _ptr = h._ptr;
            }

            // validator goes here?


            friend class registry;

        protected:
	    void push(RValue* n) {
		n->_next = _ptr;
		_ptr = n;
	    }
	    
            RValue* _ptr;
        };


        class
        registry
        {
        public:
            registry() : del_reg(true),
                         _registry(new treemap<RValueHandle>())
	    { }

            ~registry() {
                if (del_reg) { delete _registry; }
            }

            // warning, the treeconf assumes ownership of this pointer
            void set(std::string path, RValue* pval)
	    { _registry->value(path).push(pval); }

            void set(std::string path, int ival)
	    { _registry->value(path).push(new RInt(ival)); }

            void set(std::string path, double dval)
	    { _registry->value(path).push(new RDouble(dval)); }

            void set(std::string path, std::string sval)
	    { _registry->value(path).push(new RString(sval)); }


            // throws exception if it does not exist
            RValue& value(const std::string & path) {
                RValue* ptr = valuePtr(path);

                if (ptr == NULL)
                {
                    throw UnsetException(path);
                }

                return  *ptr;

            }
	
            const RValue& value(const std::string & path) const {
                const RValue* ptr = valuePtr(path);

                if (ptr == NULL)
                {
                    throw UnsetException(path);
                }

                return  *ptr;

            }



            // read-only operator access
            const RValue& operator[](const std::string& path) const {
                return value(path);
            }

	    template<typename LAMBDA>
	    void foreach(const std::string& path, LAMBDA f) const {
		// recursively call f on the nodes that path maps to
		foreach_impl(valuePtr(path), f);
	    }

            RValue* valuePtr(const std::string& path) {
                return _registry->value(path, false)._ptr;
            }

            const RValue* valuePtr(const std::string& path) const {
                return _registry->value(path, false)._ptr;
            }


            const registry childReg(const std::string& path) const {
                return registry(_registry->subtreemap(path, false));
            }

            registry childReg(const std::string& path) {
                return registry(_registry->subtreemap(path, false));
            }


            class UnsetException : public std::runtime_error
            {
            public:
                UnsetException() : std::runtime_error("empty value handle")
		{ }

                UnsetException(std::string msg) :
                    std::runtime_error("empty value handle \""+msg+"\"")
		{ }
	
            };

            // HACK: copying the registry points to the original data
            // structure.  this is to facilitate childReg on older
            // compilers
            registry(const registry& reg) : del_reg(false), _registry(reg._registry)
	    { } 



        protected:
	    

            registry(treemap<RValueHandle>* subreg) : del_reg(false),
                                                      _registry(subreg)
	    { }

            bool del_reg;
            treemap<RValueHandle>* _registry;

	    template<typename LAMBDA>
	    void foreach_impl(const RValue* p, LAMBDA f) const {
		if (p == NULL) {
		    return;
		}

		foreach_impl(p->next(), f);
		f(*p);
	    }
        };


        // load contents of file at path into registry.  return nonzero if
        // success
        inline int load(registry& reg, const std::string& path)
        {
            std::ifstream in;

            struct stat statbuf;
            if (stat(path.c_str(), &statbuf) != 0)
            {
                ERROR_NO_ABORT("error statting " << path);
                return 0;
            }

            in.open(path.c_str());
            if (! in.is_open())
            {
                ERROR_NO_ABORT("could not open " << path);
                return 0;
            }

            const int LINESIZE =1024;
            char linebuf[LINESIZE];

            while (in.getline(linebuf, LINESIZE))
            {
                std::stringstream ss(linebuf);
                std::string key;
                std::string value;

                ss >> key;

                if (! key.empty())
                {
                    ss >> value;
                    reg.set(key, value);
                }
                
            }

            return 1;
        }



    }


}



#endif /* _SPLATT_CONF_H_ */
