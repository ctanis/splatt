    /*! a special, user-space version of an explicit_index for facilitating
      complex mesh manipulation logic.  Be careful!  It tries to maintain a
      mapping to position in original index.

      Fast hash lookup is always enabled.
     */
    class temp_index : public explicit_index
    {
    public:
	/*! Construct a new `temp_index`
	  \param mgr pointer to relevant part_mgr
	  \param entity_type topological entity type for the contents of this index
	  \param update_hash will this entity have enough distribution of mesh elements for the hash to benefit from having dedicate room for data associated with every local node ?
	 */
	temp_index (part_mgr* mgr, int entity_type, bool update_hash=false);

        /*! Construct a new `temp_index` modeled after an explicit index
          \param ei `explicit_index` to model after
          \param update_hash this index is expected to use a wide range of local nodes
        */
	temp_index(explicit_index& ei, bool update_hash=false);

        /*! Construct a new `temp_index` with no type
          \param mgr the authority `part_mgr`
          \param width number of nodes per entity
          \param update_hash this index is expected to use a wide range of local nodes
        */
        static temp_index no_type(part_mgr* mgr, int width, bool update_hash=false);

        /*! Temporarily treat an array as a temp_index so it can be  renumbered
          \param mgr the authority `part_mgr`
          \param data the `std::vector` to process
        */
        static temp_index vec_wrapper(part_mgr* mgr, std::vector<int>& data);
        
        /*! Remove all index contents */
        status clear();
        

	/*! Non-const entity accessor.
          \param f entity to look up
          \returns pointer to nodes of specified entity */
	int* operator[](int f) // inline
	{
	    return &_nodes[f*_width];
	}


	/*! Const version of entity accessor.
          \param f entity to look up
          \returns pointer to nodes of specified entity */
	const int* operator[](int f) const // inline
	{
	    return &_nodes[f*_width];
	}



	/*! Add a entity to this index
          \param eltno position in source index for entity, if applicable
          \param newface_nodes nodes for the new entity
          \returns local position of new entity
         */
	int add_face(int eltno, const int* newface_nodes)
	{
	    int x = explicit_index::add_face(newface_nodes);
	    eltnos.push_back(eltno);
	    //eltnos[x]=eltno;
//	    DEBUG("adding face to temp_index: " << _live_hash);
	    return x;
	}

	/*! Add a entity to this index
          \param newface_nodes nodes for the new entity
          \returns local position of new entity
         */
	int add_face(const int* newface_nodes)
	{
	    int x = explicit_index::add_face(newface_nodes);
	    eltnos.push_back(-1);
	    //eltnos[x]=eltno;
//	    DEBUG("adding face to temp_index: " << _live_hash);
	    return x;
	}

	// would this be more useful if it also added data from other?
	// TODO: deal with data here

	/*! Copy all entities from one temp_index
	  \param other the original temp_index
          \param unique check for and purge duplicates
          \returns success or failure
	*/
	status add_all(const temp_index& other, bool unique=false)
	{
	    assert(other._etype == _etype);

	    if (unique)
	    {
		for (int n=0; n<other.size(); n++)
		{
		    if (find(other[n]) == -1)
		    {
			add_face(other[n]);
		    }
		}		
	    }

	    else
	    {
		// assuming these are compatible types
		for (int n=0; n<other.size(); n++)
		{
		    add_face(other[n]);
//		add_face(other.origid(n), other[n]);
		}
	    }
	    
	    
	    return SPLATT_OK;
	}


        /*!
          \param f local entity being checked
          \returns original id in source index */
	int origid(int f) const
	{
	    return eltnos[f];
	}


        /*! Finds a face in this index
          \param nodes the face to be searched for
          \returns position of face or -1 if not found
        */
	int find(const int* nodes) const
	{
	    return hash_find(_node2faces, nodes, _width);
	}


        /*! Renumber this index (see `part_mgr`) */
	virtual status renumber(const ownerdb& new_odb,
				const std::vector<int>& newgids,
				std::map<int,int>& global_map);

        /*! Query support <b>Do not call directly</b> */
	template <typename Q_OP>
	void do_query(const Q_OP& op, int specific=-1)
	{
	    if (specific >= 0)
	    {
		assert(specific*_width< (int)_nodes.size());
		op(this, eltnos[specific], _etype, _width, &_nodes[specific*_width]);
	    }
	    else
	    {
		int face=0;
//		DEBUG("in temp_index::do_query: " << _nodes.size() << "/" << _width << " -- " << eltnos.size());
		for (unsigned int f=0; f< _nodes.size(); f+= _width)
		{
		    //DEBUG("eltnos["<<face<<"/"<<eltnos.size()<<"]="<<eltnos[face]);
		    // pass eltnos from original index
		    op(this, eltnos[face++], _etype, _width, &_nodes[f]);
		    //op(crumbs,qdepth+1,this, face++, _etype, _width, &_nodes[f]);
		}
	    }
	}

        /*! Apply the `migrate` protocol
          \param args protocol args
          \returns success or failure
        */
	status do_migrate(migrate_args& args)
	{
	    return explicit_index::do_migrate(args);
	}
	

    private:
	friend class explicit_index;
	std::deque<int> eltnos;

	static std::vector<int> dummy_nodes;

        // private constructor called by static ::no_type
        temp_index(part_mgr* mgr, bool x, int width, bool update_hash);

        // private constructor called by static ::vec_wrapper
        temp_index(part_mgr* mgr, bool x, std::vector<int>& data);
    };
    
