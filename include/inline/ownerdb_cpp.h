//  -*- Mode: C++ -*- 
namespace splatter
{


    // constructor
    inline ownerdb::ownerdb(const parallel_ctx& pctx, const std::vector<int>& node_dist) :
	_pctx(pctx), _node_dist(node_dist), _rank(_pctx.rank()), _np(_pctx.np()),
	_low(_node_dist[_rank]), _high(_node_dist[_rank+1])
    {
    }


    inline int	ownerdb::me() const { return _rank; }
    inline int	ownerdb::np() const { return _np; }
    inline int  ownerdb::low() const{ return _low; }
    inline int  ownerdb::high() const { return _high; }

    inline bool ownerdb::owns(int g) const
    {
	return (_low <= g && g < _high)||(g==-1); // we /own/ an unset node
    }


    inline int ownerdb::owner(int g) const
    {
	assert((int)_node_dist.size()-1 == _np);
        if (g == -1) return _rank;
        
	// binary search of _node_dist;
	int low=0;
	int high=_np;
	int mid;

	do
	{
	    mid = (low+high) / 2;

	    if (g < _node_dist[mid])
	    {
		high = mid;

	    }
	    else if (g >= _node_dist[mid+1])
	    {
		low = mid+1;
	    }
	    else
	    {
		return mid;
	    }
	}
	while (low < high);

	ERROR("could not find owner of node " << g << "|" << *this);
	return -1;
    }

    inline int ownerdb::nlocal() const
    {
	return _high-_low;
    }

    inline int ownerdb::nglobal() const
    {
	return _node_dist[_np];
    }

    inline const int* ownerdb::node_dist() const
    {
	return &_node_dist[0];
    }

    inline const parallel_ctx& ownerdb::pctx() const
    {
	return _pctx;
    }

    inline int ownerdb::g2l(int g) const
    {
	assert(owns(g));
	return (g - _low);
    }

    inline int ownerdb::l2g(int l) const
    {
	assert(l >= 0 && l < _high-_low);
	return (l + _low);
    }


    inline int ownerdb::g2l_p(int g) const
    {
	if (owns(g))
	{
	    return g2l(g);
	}
	else
	{
	    std::map<int,int>::const_iterator match = _glob2loc.find(g);
	    if (match == _glob2loc.end())
	    {
		return -1;
	    }
	    else
	    {
		return match->second + nlocal();
	    }
	}
    }

    inline int ownerdb::l2g_p(int l) const
    {
	int loc = nlocal();
	
	if (l < loc)
	{
	    return l2g(l);
	}
	else
	{
	    // l is phantom
	    assert (l-loc < (int)_loc2glob.size());
	    return _loc2glob[l-loc];
	}


    }

    inline int ownerdb::nlocal_p() const
    {
	return nlocal() + _loc2glob.size();
    }

    inline int ownerdb::nphantom() const
    {
	return _loc2glob.size();
    }


}
