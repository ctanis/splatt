//  -*- Mode: C++; -*- 
#include <iterator>

namespace splatter
{
    class MigrateMonitor : public proxy_monitor
    {
    public:
	MigrateMonitor(fast_index& fi, const std::vector<int>& nodes, int width) :
	    fi(fi), nodes(nodes), width(width)
	{}

	void moving(int from, int to)
	{
//	DEBUG("manually unindexing face " << from);
	
	    int start = from*width;
	    for (int n=0; n<width; n++)
	    {
		fi[nodes[start+n]].remove(from);
	    }
	}

    private:
	fast_index& fi;
	const std::vector<int>& nodes;
	int width;
    

    };



    inline std::string index::name() const
    {
	return _name;
    }
    
    inline index_type index::type() const
    {
	return _type;
    }

    inline int index::flags() const
    {
	return _flags;
    }

    inline int	index::size() const // virtual
    {
	return 0;
    }

    inline int implicit_index::size() const
    {
	return _odb.nlocal(); // + _glob2loc.size();
    }

    inline int explicit_index::size() const
    { 
	return _nodes.size() / _width;
    }

    template <typename D>
    D& index::data(std::string name, const D& source)
    {
        static D error;
        
	for (unsigned int d=0; d<_data.size(); d++)
	{
	    if (_data_names[d] == name)
	    {
		return (*static_cast<D*>((_data[d])->raw()));
	    }
	    
	}
	ERROR("index has no data " << name << " -- returning source!");
	return error;
    }

    inline void* index::data_raw(std::string name)
    {
	for (unsigned int d=0; d<_data.size(); d++)
	{
	    if (_data_names[d] == name)
	    {
		return _data[d]->raw();
	    }
	    
	}
	ERROR("index has no data " << name);
	return NULL;
    }

    inline data_proxy* index::dproxy(std::string name)
    {
	for (unsigned int d=0; d<_data.size(); d++)
	{
	    if (_data_names[d] == name)
	    {
		return _data[d];
	    }
	    
	}
	ERROR("index has no data " << name);
	return NULL;
    }



    inline int explicit_index::add_face(const int* nf)
    {
	int faceid= _nodes.size()/_width;

//    DEBUG("adding a new face");
	if (_live_hash)
	{
// we allow this because we can clean it up
//	assert(find(_etype, nf) == -1);
	    for (int n=0; n<_width; n++)
	    {
		_nodes.push_back(nf[n]);
//	    DEBUG("hashing face " << faceid << " for "<< nf[n]);
		_node2faces[nf[n]].insert(faceid);
	    }
	}
	else
	{
	    for (int n=0; n<_width; n++)
	    {
		_nodes.push_back(nf[n]);
	    }
	}

	for (unsigned int d=0; d<_data.size(); d++)
	{
	    _data[d]->add_one();
	}

    
	return faceid;
    }



    template <typename T>
    void explicit_index::unindex_faces(T start, T end)
    {
	for (; start != end; start++)
	{
//	    DEBUG("manually[2] unindexing " << *start);
	    int facenodes = *start * _width;

	    if (facenodes >= (int)_nodes.size()) // this could happen
	    {
		continue;
	    }

	    for (int i=0; i<_width; i++)
	    {
//		DEBUG(_nodes[facenodes+i]);
//		DEBUG(_node2faces[_nodes[facenodes+i]]);
		_node2faces[_nodes[facenodes+i]].remove(*start);
//		DEBUG(_node2faces[_nodes[facenodes+i]]);
	    }
   
	}	
    }


    template <typename T>
    void explicit_index::index_faces(T start, T end)
    {
	for (; start != end; start++)
	{
	    int facenodes = *start * _width;

	    if (facenodes >= (int)_nodes.size()) // this could happen
	    {
		continue;
	    }


	    for (int i=0; i<_width; i++)
	    {
		_node2faces[_nodes[facenodes+i]].insert(*start);
	    }
	}

    }


    // query support
    template <typename Q_OP>
    void explicit_index::do_query(const Q_OP& op, int specific)
    {
//	DEBUG("do_query called: " << _etype << "," << _width << "," << _nodes.size());
	

	if (specific >= 0)
	{
//	    DEBUG("specific: " << specific << ", _width: " << _width << ", size: " << _nodes.size());
	    assert(specific*_width < (int)_nodes.size());
	    op(this,specific, _etype, _width, &_nodes[specific*_width]);
	}
	else
	{
	    int face=0;
	    for (unsigned int f=0; f< _nodes.size(); f+= _width)
	    {
		op(this,face++, _etype, _width, &_nodes[f]);
	    }
	}
	
    }

    template <typename Q_OP, typename IT>
    void explicit_index::do_query_it(const Q_OP& op, IT begin, IT end)
    {
	for (; begin != end; begin++)
	{
	    int idx= *begin * _width;
	    op(this,*begin, _etype, _width, &_nodes[idx]);
	}
    }




    // delete faces
    template <typename IT>
    status explicit_index::delete_faces(IT start, IT end)
    {
        // if (_etype == -1)
        // {
        //     LOG("calling delete_faces_careful for " << _name);
        //     return delete_faces_careful(start,end);
        // }
        TRACE(876, "delete_faces for " << _name);
	LLOG(1, "going to delete faces");
	if (! _live_hash)
	{
	    ERROR("cannot delete_faces ("<<_name<<") without live hash");
	    assert(0);
	    return SPLATT_FAIL;
	}

//	DEBUG("in delete_faces:" << _odb);
	// communicate faces to remote owners
	deliver_args delarg(_odb.pctx());
	migrate_args migarg(_odb.pctx());

        TRACE(876, "");

	for (; start != end; start++)
	{
	    int* face_nodes = &_nodes[*start*_width];
	    migarg.to_delete.push_back(*start);
	    
	    
	    for (int n=0; n<_width; n++)
	    {
		if (_purge_during_renumber) // we know exactly who can have this
                {
                    int o = _odb.owner(face_nodes[n]);
                    if (o != _odb.me())
                    {
                        delarg[o].insert(*start);
                    }
                }
                else
                {
                    // tell everybody to delete this.. use sparingly!
                    for (int o=0; o<_odb.pctx().np(); o++)
                    {
                        if (o != _odb.me())
                        {
                            delarg[o].insert(*start);
                        }
                    }
                    
                }
                
	    }
	}
        TRACE(876, "gonna barrier");
        MPI_Barrier(MPI_COMM_WORLD);
	delarg.prepare();

        TRACE(876, "");
            
	temp_index remote_deleted(*this);
	status didit=deliver(delarg, remote_deleted, false); // don't send data
        TRACE(876, "deliver status: " << didit);
	if (didit)
	{
	    // process deletions
	    for (int f=0; f<remote_deleted.size(); f++)
	    {
		// find the requested deletions
		intset match = findall(_width, remote_deleted[f]);

                if (_etype == -1) // gotta be pickier than findall
                {
                    picky_matcher match_sig(_width, remote_deleted[f]);

                    if (match_sig.distinct())
                    {
                        // _width unique nodes -> no hash problems
                        // queue up the deletions
                        for (unsigned int m=0;m<match.size() ; m++)
                        {
                            migarg.to_delete.push_back(match[m]);
                        }
                    }
                    else
                    {
                        // check for the precise node breakdown in each match

                        for (unsigned int m=0; m<match.size(); m++)
                        {
                            if (match_sig.match(&_nodes[match[m]*_width]))
                            {
                                migarg.to_delete.push_back(match[m]);
                            }
                        }
                    }
                }
                else
                {
                    // queue up the deletions
                    for (unsigned int m=0;m<match.size() ; m++)
                    {
                        migarg.to_delete.push_back(match[m]);
                    }
                }
                
	    }
            TRACE(876, "done building migarg");
            

	    // TRACE(1999, "found " << migarg.to_delete.size() << " to delete");
	    // TRACE(1999, "current size: "<< size());

	    migarg.prepare();
	    TRACE(876, "migarg prepared");

	    unindex_faces(migarg.to_delete.begin(), migarg.to_delete.end());
	    MigrateMonitor delmon(_node2faces, _nodes, _width);
	    int last_count=_nodes.size();

	    if (_nodeproxy.batch_migrate(migarg, &delmon) == SPLATT_FAIL)
	    {
		ERROR("migration of nodes for deletion failed");
		return SPLATT_FAIL;
	    }
            TRACE(876, "batch_migrate of node data complete");

//	    TRACE(1999, "post-migrate size: " << size());
	    

	    index_faces(migarg.to_delete.begin(), migarg.to_delete.end());
	    int new_count = _nodes.size();

	    if (new_count > last_count)
	    {
		// index faces pushed on the end
		assert(last_count % _width == 0);
		assert(new_count % _width == 0);
		index_faces_nodes(last_count, _nodes.size());
	    }

	    _node2faces.purge();

            TRACE(876, "about to batch_migrate data " << _data.size());
	    for (unsigned int d=0; d<_data.size(); d++)
	    {
		if (_data[d]->batch_migrate(migarg) == SPLATT_FAIL)
		{
		    ERROR("data migration ("<<_name<<"/"<<_data_names[d] << ") failed");
		    return SPLATT_FAIL;
		}
	    }

	    return SPLATT_OK;
	}
	else
	{
	    return SPLATT_FAIL;
	}
    }


// template <typename IT>
//     status explicit_index::delete_faces_careful(IT start, IT end)
//     {
// 	LLOG(1, "going to delete faces careful");
// 	if (! _live_hash)
// 	{
// 	    ERROR("cannot delete_faces ("<<_name<<") without live hash");
// 	    assert(0);
// 	    return SPLATT_FAIL;
// 	}

// //	DEBUG("in delete_faces:" << _odb);
// 	// communicate faces to remote owners
// 	deliver_args delarg(_odb.pctx());
// 	migrate_args migarg(_odb.pctx());

// 	for (; start != end; start++)
// 	{
// 	    int* face_nodes = &_nodes[*start*_width];
// 	    migarg.to_delete.push_back(*start);
	    
	    
// 	    for (int n=0; n<_width; n++)
// 	    {
// 		int o = _odb.owner(face_nodes[n]);
// 		if (o != _odb.me())
// 		{
// 		    delarg[_odb.owner(face_nodes[n])].insert(*start);
// 		}		
// 	    }
// 	}
// 	delarg.prepare();

// 	temp_index remote_deleted(*this);
// 	status didit=deliver(delarg, remote_deleted, false); // don't send data


// 	if (didit)
// 	{
// 	    // process deletions
// 	    for (int f=0; f<remote_deleted.size(); f++)
// 	    {
// 		// find the requested deletions
// 		intset match = findall(_width, remote_deleted[f]);

//                 picky_matcher match_sig(_width, remote_deleted[f]);
                
                
//                 if (match_sig.distinct())
//                 {
//                     // _width unique nodes -> no hash problems
//                     // queue up the deletions
//                     for (unsigned int m=0;m<match.size() ; m++)
//                     {
//                         migarg.to_delete.push_back(match[m]);
//                     }
//                 }
//                 else
//                 {
//                     // check for the precise node breakdown in each match

//                     for (unsigned int m=0; m<match.size(); m++)
//                     {
//                         if (match_sig.match(&_nodes[match[m]*_width]))
//                         {
//                             migarg.to_delete.push_back(match[m]);
//                         }
//                     }

//                 }
                
// 	    }

// 	    // TRACE(1999, "found " << migarg.to_delete.size() << " to delete");
// 	    // TRACE(1999, "current size: "<< size());

// 	    migarg.prepare();
	    
// 	    unindex_faces(migarg.to_delete.begin(), migarg.to_delete.end());
// 	    MigrateMonitor delmon(_node2faces, _nodes, _width);
// 	    int last_count=_nodes.size();

// 	    if (_nodeproxy.batch_migrate(migarg, &delmon) == SPLATT_FAIL)
// 	    {
// 		ERROR("migration of nodes for deletion failed");
// 		return SPLATT_FAIL;
// 	    }

// //	    TRACE(1999, "post-migrate size: " << size());
	    

// 	    index_faces(migarg.to_delete.begin(), migarg.to_delete.end());
// 	    int new_count = _nodes.size();

// 	    if (new_count > last_count)
// 	    {
// 		// index faces pushed on the end
// 		assert(last_count % _width == 0);
// 		assert(new_count % _width == 0);
// 		index_faces_nodes(last_count, _nodes.size());
// 	    }

// 	    _node2faces.purge();


// 	    for (unsigned int d=0; d<_data.size(); d++)
// 	    {
// 		if (_data[d]->batch_migrate(migarg) == SPLATT_FAIL)
// 		{
// 		    ERROR("data migration ("<<_name<<"/"<<_data_names[d] << ") failed");
// 		    return SPLATT_FAIL;
// 		}
// 	    }

// 	    return SPLATT_OK;
// 	}
// 	else
// 	{
// 	    return SPLATT_FAIL;
// 	}
//     }


    template <typename T>
    status implicit_index::sync_one(const parallel_ctx& pctx,
				    const sync_args& args,
				    std::vector<T>& data, int size_per)
    {
	data_proxy* tmp = proxy(data, size_per);

	status rval = tmp->sync(pctx, args);

	delete(tmp);	
	return rval;
    }


} // splatter

