//  -*- Mode: C++; -*- 
// filter -- see splatt_query.h
class exp_index_searcher
{
  public:
    exp_index_searcher(std::vector<int>& nodes, fast_index& hash) :
    idxnodes(nodes), hash(hash), master(NULL), etype(-1), width(-1), valid(false) {}

    status init(part_mgr* mgr)
    {
	if (valid)
	{
	    return SPLATT_OK;
	}
	else
	{
	    return SPLATT_FAIL;
	}
    }

    // filter mode
    template <typename OUT>
	void operator()(splatter::index* idx,
			const int& eltno, const int& etype,
			const int& nnodes, const int* nodes, OUT& o) const
    {
	assert(nnodes > 0);
	intset match = hash[nodes[0]];
	int n=1;

	while (match.size() > 0 && n < nnodes)
	{
	    match.intersect(hash[nodes[n]]);
	    n++;
	}

	if (match.size() > 0)
	{
	    for (unsigned int f=0; f<match.size(); f++)
	    {
//		DEBUG("found face " << match[f] << "|" << match[f]*width << " -- " << idxnodes.size());
		o(master, match[f], exp_index_searcher::etype, width, &(idxnodes[match[f]*width]));
	    }
	}	    
    }

    // cond mode
    bool operator()(splatter::index* idx,
		    const int& eltno, const int& etype,
		    const int& nnodes, const int* nodes) const
    {
	assert(nnodes > 0);
	return hash_contains(hash, nodes, nnodes);
    }

  private:
    friend class explicit_index;

  exp_index_searcher(splatter::index* main, std::vector<int>& nodes, fast_index& hash, int etype, int width)
      : idxnodes(nodes), hash(hash), master(main), etype(etype), width(width), valid(true) {}

    const std::vector<int>& idxnodes;
    const fast_index& hash;
    splatter::index* master;
    const int etype;
    const int width;
    const bool valid;
};
    
