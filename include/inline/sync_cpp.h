//  -*- Mode: C++; -*- 
namespace splatter
{
    
    template <typename T>
    status sync_args::sync_raw(parallel_ctx pctx, T* x, int size_per) const
    {
	T* sendbuf = (T*)buffer(size_per * sizeof(T));

	// load sendbuf
	for (unsigned int n=0; n<out_ids.size(); n++)
	{
	    for(int i=0; i<size_per; i++)
	    {
		sendbuf[n*size_per + i]
		    = x[out_ids[n]*size_per + i];
	    }
	}


	// exchange directly into memory area at end of data (see displs)
	MPI_Alltoallv(sendbuf,
		      const_cast<int*>(&(out_sizes[0])),
		      const_cast<int*>(&(out_displs[0])),
		      get_mpi_type(T(),size_per),
		      x,
		      const_cast<int*>(&(in_sizes[0])),
		      const_cast<int*>(&(in_displs[0])),
		      get_mpi_type(T(),size_per),
		      pctx.comm());

	return SPLATT_OK;
    }
}
