//  -*- Mode: C++ -*- 
namespace splatter
{
    // declared in ../splatt_core.h

    inline const parallel_ctx& part_mgr::pctx() const 
    {
	return _pctx;
    }
    
    inline const ownerdb& part_mgr::get_ownerdb() const
    {
	return _odb;
    }

    inline splatter::implicit_index* part_mgr::get_node_index()
    {
	return &_node_index;
    }

    inline ownerdb part_mgr::make_ownerdb(std::vector<int> nd)
    {
	return ownerdb(_pctx, nd);
    }

    // inline const etype_graph& part_mgr::get_etypes() const
    // {
    // 	return _entity_adj;
    // }

    inline const entity_cfg* part_mgr::get_etypes() const
    {
	return _entities;
    }

    template <typename T>
    status part_mgr::sync_node_data(std::vector<T>& data, int size_per)
    {
	return _node_index.sync_one(_pctx, _sync_args, data, size_per);
    }

    template <typename T>
    status part_mgr::sync_raw_node_data(T* data, int size_per)
    {
	return _sync_args.sync_raw(_pctx, data, size_per);
    }


    template <typename T>
    status part_mgr::query(T op)
    {
	status istat = op.init(this);

	if (istat == SPLATT_OK)
	{
	    op();
	    return SPLATT_OK;
	}
	else
	{
	    ERROR("query init failed");
	    return SPLATT_FAIL;
	}
    }

    // template <typename IT>
    // status part_mgr::translate_nodes(IT start, IT finish)
    // {
    //     for (; start != finish; start++)
    //     {
    //         std::map<int,int>::iterator match = _last_renum_map.find(*start);
    //         if (match != _last_renum_map.end())
    //         {
    //             *start = match->second;
    //         }
    //         else
    //         {
    //             ERROR("node not found in _last_renum_map: " << *start);
    //             return SPLATT_FAIL;
    //         }
    //     }

    //     return SPLATT_OK;
    // }

}
