//  -*- Mode: C++; -*- 
namespace splatter
{

    template <typename T>
    T parallel_ctx::reduce(T in, MPI_Op op) const
    {
	T rval;
	MPI_Allreduce(&in, &rval, 1, get_mpi_type<T>(), op, _comm);
	return rval;
    }


    template <typename T>
    void parallel_ctx::broadcast(std::vector<T>& vec, int root) const
    {
	if (root < 0)
	{
	    root = _root;
	}

	int size=vec.size();
	MPI_Bcast(&size, 1, MPI_INT, root, _comm);
	
	vec.resize(size);
	MPI_Bcast(&vec[0], size, get_mpi_type<T>(), root, _comm);
	
    }
    
}
