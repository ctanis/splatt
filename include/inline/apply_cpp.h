//  -*- Mode: C++; -*- 

namespace splatter
{
    // vanilla functor apply over range
    template <typename OP, typename IT>
    void index::apply(const OP& op, IT start, IT end)
    {
	switch(_type)
	{
	 case SPLATT_IMPLICIT:
	     static_cast<implicit_index*>(this)->do_apply<OP,IT,&OP::operator()>(op, start, end);
	     break;
	 case SPLATT_EXPLICIT:
	     static_cast<explicit_index*>(this)->do_apply<OP,IT,&OP::operator()>(op, start, end);
	     break;

	 default:
	     ERROR("cannot 'apply+iterators' to index with type " << _type);
	}

	return;
    }

    // specify a function to apply over range
    template <typename OP, typename IT, 
	      void (OP::*func)(index*,int,int,int,int*) const>
    void index::apply(const OP& op, IT start, IT end)
    {
	switch(_type)
	{
	 case SPLATT_IMPLICIT:
	     static_cast<implicit_index*>(this)->do_apply<OP,IT, func>(op, start, end);
	     break;
	 case SPLATT_EXPLICIT:
	     static_cast<explicit_index*>(this)->do_apply<OP,IT, func>(op, start, end);
	     break;

	 default:
	     ERROR("cannot 'apply+iterators' to index with type " << _type);
	}

	return;
    }




    template <typename OP>
    void index::apply(const OP& op)
    {
	switch(_type)
	{
	 case SPLATT_IMPLICIT:
	     static_cast<implicit_index*>(this)->do_apply<OP,&OP::operator()>(op);
	     break;
	 case SPLATT_EXPLICIT:
	     static_cast<explicit_index*>(this)->do_apply<OP,&OP::operator()>(op);
	     break;

	 default:
	     ERROR("cannot 'apply+noiterators' to index with type " << _type);
	}

	return;
    }

    template <typename OP,
	      void (OP::*func)(index*,int,int,int,int*) const>
    void index::apply(const OP& op)
    {
	switch(_type)
	{
	 case SPLATT_IMPLICIT:
	     static_cast<implicit_index*>(this)->do_apply<OP,func>(op);
	     break;
	 case SPLATT_EXPLICIT:
	     static_cast<explicit_index*>(this)->do_apply<OP,func>(op);
	     break;

	 default:
	     ERROR("cannot 'apply+noiterators' to index with type " << _type);
	}

	return;
    }

    
    // TODO: pass global ids in nodes array
    template <typename OP, typename IT,
	      void (OP::*func)(index*,int,int,int,int*) const
	      >
    void implicit_index::do_apply(const OP& op, IT start, IT end)
    {
	for(; start != end; start++)
	{
	    int g = _odb.l2g(*start);
	    (op.*func)(this, *start, _etype, 1, &g);
	}
    }

    template <typename OP,
	      void (OP::*func)(index*,int,int,int,int*) const
	      >
    void implicit_index::do_apply(const OP& op)
    {
	for (int n=0; n<_odb.nlocal(); n++)
	{
	    int g=_odb.l2g(n);
	    (op.*func)(this, n, _etype, 1, &g);
	}

    }



    template <typename OP, typename IT, 
	      void (OP::*func)(index*,int,int,int,int*) const>
    void explicit_index::do_apply(const OP& op, IT start, IT end)
    {
	LLOG(2, _name << "(with iterator)");
	for(; start != end; start++)
	{
	    assert(*start >= 0 && *start < (int)_nodes.size()/_width);
	    (op.*func)(this, *start, _etype, _width, &_nodes[*start *_width]);
	}
    }

    template <typename OP,
	      void (OP::*func)(index*,int,int,int,int*) const
	      >
    void explicit_index::do_apply(const OP& op)
    {
	LLOG(2, _name << "(all)");
	int face=0;
	for(unsigned int f=0; f < _nodes.size(); f += _width)
	{
	    (op.*func)(this, face++, _etype, _width, &_nodes[f]);
	}

    }

    
}
