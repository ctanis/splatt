//  -*- Mode: C++; -*- 
#include <limits>

namespace splatter
{
    
    // data_proxy for std::vector's
    template< typename T >
    class data_proxy_std : public data_proxy
    {
    public:
    	data_proxy_std(std::vector<T>& d, int size_per=1, int flags=0) :
    	    data_proxy(flags),
	    deleteme( (flags & SPLATT_STEAL_DATA) ? new std::vector<T>() : NULL),
	    _data( deleteme ? (*deleteme) : d),
	    _size_per(size_per)
    	{
	    if (flags & SPLATT_STEAL_DATA)
	    {
		//deleteme->swap(d);
		_data.swap(d);
//		TRACE(11000, "swapped: " << _data.size()<< ";" << d.size());
		
				    
	    }	    
	}
	

    	~data_proxy_std()
    	{
	    if (deleteme != NULL)
	    {
		delete deleteme;
	    }
    	}
	
        void clear() { _data.clear(); }

	int size() { return  _data.size() / _size_per; }

	int size_per() { return _size_per; }

	void* raw() { return &_data; }


	status resize(unsigned int ns)
	{
	    if (ns*_size_per < _data.size())
	    {
		LLOG(1, "resizing data to smaller size: " << _data.size() << "-> " << ns*_size_per);
	    }
	    
	    _data.resize(ns*_size_per);
	    return SPLATT_OK;
	}

	// room for one more thing....
	void insert(int n)
	{
	    _data.insert(_data.end(), n*_size_per, T());
	}


	status batch_migrate(const migrate_args& args,
			     proxy_monitor* monitor=NULL);

	data_proxy_std<T>* deliver(const deliver_args& args, data_proxy* out=NULL);

	status sync(parallel_ctx pctx, const sync_args& args);
	
	status reorder(const std::vector<int>& localids);
	
	status load(std::ifstream& in);

	status save(std::ofstream& out, int limit=-1);

	std::string dump();
	
        data_proxy* type_copy();
        void store_raw(int dest, int src, data_proxy* p2);
        


    protected:
	std::vector<T>* deleteme;
    	std::vector<T>& _data;
	const int _size_per;
    };
    
} // splatter



namespace splatter
{
    
    // this absolutely must process data in the same order as
    // exp_index::initial_distribute


    template< typename T >
    status data_proxy_std<T>::batch_migrate ( const migrate_args& args,
					      proxy_monitor* monitor )
    {
	LLOG(2, ((monitor == NULL) ? "no proxy_monitor" : "using proxy_monitor"));
	if (! args.prepared)
	{
	    ERROR("attempt to batch_migrate with unprepared args");
	    return SPLATT_FAIL;
	}
	
	T* sendbuff = (T*)args.send_buffer(sizeof(T)*_size_per);
	T* recvbuff = (T*)args.recv_buffer(sizeof(T)*_size_per);

	// if (sendbuff == NULL || recvbuff==NULL)
	// {
	//     ERROR("cannot allocate communication buffer!");
	//     return SPLATT_FAIL;
	// }
	

	std::vector<MPI_Request> req( 2*args.pctx.np() );
	int reqcount=0;

	int send_curs=0;
	int recv_curs=0;
	

	LLOG(2, "initiating mpi");
	// send a bunch of immediate messages
	for (int r=0; r < args.pctx.np(); r++)
	{
	    if (args.in_sizes[r] > 0)
	    {
		//responses[r] = new std::vector<T>(insizes[r]*size_per);
		MPI_Irecv(&recvbuff[recv_curs],
			  args.in_sizes[r]*_size_per,
			  get_mpi_type(T()),
			  r, 0, args.pctx.comm(), &req[reqcount++]);

		recv_curs += args.in_sizes[r]*_size_per;
	    }


	    if (args.to_move[r].size() > 0)
	    {
		T* lbuff = &sendbuff[send_curs];
		
		for (unsigned int f=0; f<args.to_move[r].size(); f++)
		{
		    for (int i=0; i<_size_per; i++)
		    {
			lbuff[f*_size_per + i] =
			    _data[args.to_move[r][f] * _size_per + i];
		    }
		}

		MPI_Isend(lbuff,
			  args.to_move[r].size() * _size_per,
			  get_mpi_type(T()),
			  r,
			  0,
			  args.pctx.comm(),
			  &req[reqcount++]);

		send_curs += args.to_move[r].size() * _size_per;
	    }
	}
	LLOG(2, "mpi complete");


	// wait for messages to finish
	MPI_Status dummy;
	for (int r=0; r<reqcount; r++)
	{
	    MPI_Wait(&req[r], &dummy);
	}

    
	unsigned int last_delete = args.to_delete.size();
	unsigned int delete_next=0;

	LLOG(2, "moving incoming into data array: " << recv_curs);


	// copy incoming data into _data -- starting with replacing
	// the ones to be deleted
	for (int f=0; f < recv_curs; f += _size_per)
	{
	    if (delete_next < last_delete)
	    {
		// overwrite the next one to delete

		for (int i=0; i<_size_per; i++)
		{
		    _data[args.to_delete[delete_next]*_size_per + i] =
			recvbuff[f + i];
		}
		
		delete_next++;
	    }
	    else
	    {
		// none left to delete .. just push it on the end

		for (int i=0; i<_size_per; i++)
		{
		    _data.push_back(recvbuff[f + i]);
		}
	    }
	}

	
	// finishing up by folding good data over remaining that need to be
	// deleted
	while (delete_next < last_delete)
	{
            // TRACE(911, "asserting: " << args.to_delete[last_delete-1]+1*_size_per
            //       << "("<< args.to_delete[last_delete-1] << "+1*" << _size_per << ")"
            //       << "<=" << _data.size());
	    // make sure our to_delete is not pointing past the end of _data
	    assert(args.to_delete[last_delete-1]+1 * _size_per <= (int)_data.size());
	    

	    if ((args.to_delete[last_delete-1]+1) * _size_per  == (int)_data.size())
	    {
		// delete "the last thing"
		last_delete--;
		for (int v=0; v<_size_per; v++)
		{
		    _data.pop_back();
		}
	    }
	    else
	    {
		// copy "the last thing" into the delete_next spot
		unsigned int idx = args.to_delete[delete_next];
		unsigned int start = idx*_size_per;

		if (monitor != NULL)
		{
		    // this is the index we are moving to idx
		    monitor->moving(_data.size()/_size_per -1, idx);
		}


		// move it backwards..
		for (int i=_size_per-1; i>=0; i--)
		{
		    _data[start+i] = _data.back();
		    _data.pop_back();
		}
	    

		delete_next++;
	    }
	}	


	return SPLATT_OK;
    }



    template <typename T>
    status data_proxy_std<T>::sync(parallel_ctx pctx, const sync_args& args)
    {
	LLOG(1, "syncing data");
	if (pctx.np() < 2)
	{
	    LLOG(1, "no need to sync data: run is serial");
	    return SPLATT_OK;
	}
	

	// phantom data may have shrunk
	// assert(args.required_size() >= size());
	resize(args.required_size());

	if (pctx.np() > 1)
	{
            if (_data.size() > 0)
            {
                return args.sync_raw(pctx, &_data[0], _size_per);
            }
            else
            {
                return args.sync_raw(pctx, (T*)NULL, _size_per);
            }
            
	}


#ifdef ORIGSYUNC
	T* sendbuf = (T*)args.buffer(_size_per * sizeof(T));
	
	
	// load sendbuf
	for (unsigned int n=0; n<args.out_ids.size(); n++)
	{
	    for(int i=0; i<_size_per; i++)
	    {
		sendbuf[n*_size_per + i]
		    = _data[args.out_ids[n]*_size_per + i];
	    }
	}

// #ifdef SPLATT_DEBUG
// 	DEBUG("_data.size()="<<_data.size() << ", size_per: " << _size_per);
// 	for (int n=0; n<pctx.np(); n++)
// 	{
// 	    DEBUG("sync out_sizes " << n << ":" << args.out_sizes[n]);
// 	    DEBUG("sync out_displs " << n << ":" << args.out_displs[n]);
// 	    DEBUG("sync in_sizes " << n << ":" << args.in_sizes[n]);
// 	    DEBUG("sync in_dspls " << n << ":" << args.in_displs[n]);
// 	    DEBUG("sendbuf: " << sendbuf);
// 	    DEBUG("_data[0]:" << _data[0]);
// 	    DEBUG("pctx.comm(): " << pctx.comm());
// 	}
// #endif // SPLATT_DEBUG

	T fake[]= { T() };
	// exchange directly into memory area at end of data (see displs)
	MPI_Alltoallv(sendbuf,
		      const_cast<int*>(&(args.out_sizes[0])),
		      const_cast<int*>(&(args.out_displs[0])),
		      get_mpi_type(T(),_size_per),
		      (_data.size() == 0 ? fake : &(_data[0])),
		      const_cast<int*>(&(args.in_sizes[0])),
		      const_cast<int*>(&(args.in_displs[0])),
		      get_mpi_type(T(),_size_per),
		      pctx.comm());
	
	LLOG(1, "sync complete");
	
#endif
	return SPLATT_OK;
    }
    


    // in situ reorder -- no MPI involved!
    template <typename T>
    status data_proxy_std<T>::reorder(const std::vector<int>& localids)
    {
	LLOG(1, "reordering data" << localids.size() << ";" << _data.size());
	assert(localids.size() == _data.size() / _size_per);

#ifdef SPLATT_DEBUG
	// verify reorder args
	std::vector<int> tmpcopy = localids;
	std::sort(tmpcopy.begin(), tmpcopy.end());

	for (unsigned int n=0;  n<tmpcopy.size(); n++)
	{
//	    TRACE(911, n << ": " << tmpcopy[n]);
            assert(tmpcopy[n] == (int)n);
	}
#endif	// SPLATT_DEBUG


	std::vector<int> prev(localids.size(), -1);
	for (int n=0; n<(int)localids.size(); n++)
	{
	    // find beginning of cycle

	    if ((prev[n] == -1) && (localids[n]!= n))
	    {
		// handle the cycle starting here
		int p = localids[n];
		prev[p] = n;

		// until the end of the cycle
		while (p != n)
		{
		    prev[localids[p]]=p;
		    p = localids[p];
		}


		std::vector<T> tmp(_size_per);
		for (int i=0; i<_size_per; i++)
		{
		    tmp[i] = _data[n*_size_per + i];
		}


		while (prev[p] != n)
		{
		    // copy from prev[p] into p
		    for (int i=0; i<_size_per; i++)
		    {
			_data[p*_size_per + i] =
			    _data[prev[p]*_size_per + i];
		    }

		    p = prev[p];
		}


		// copy tmp into  prev[p]
		for (int i=0; i<_size_per; i++)
		{
		    _data[localids[n]*_size_per + i] = tmp[i];
		}
	    }
	}


	return SPLATT_OK;
    }


    // you must delete the return value of this...
    template <typename T>
    data_proxy_std<T>*
    data_proxy_std<T>::deliver(const deliver_args& args, data_proxy* out)
    {
	LLOG(1, "delivering (to proxy "<<out<<")");

	assert(args.prepared);
	if (! args.prepared)
	{
	    ERROR("attempt to batch_migrate with unprepared args");
	    return NULL;
	}

	T* sendbuff = (T*)args.send_buffer(sizeof(T)*_size_per);

	if (out == NULL)
	{
	    // output 
	    std::vector<T>* newdata = new std::vector<T>(args.recv_size()*_size_per);
	    out = new data_proxy_std<T>(*newdata, _size_per, 0);
	    static_cast<data_proxy_std<T>*>(out)->deleteme=newdata;
	}
	assert(_size_per == out->size_per());

	data_proxy_std<T>* newproxy = static_cast<data_proxy_std<T>*>(out);
	newproxy->_data.resize(args.recv_size()*_size_per);


	std::vector<MPI_Request> req( 2*args.pctx.np() );
	int reqcount=0;

	int send_curs=0;
	int recv_curs=0;

	for (int r=0; r<args.pctx.np(); r++)
	{
	    if (args.in_sizes[r] > 0)
	    {
		MPI_Irecv(&(newproxy->_data[recv_curs]),
			  args.in_sizes[r]*_size_per,
			  get_mpi_type(T()),
			  r, 0, args.pctx.comm(), &req[reqcount++]);
		recv_curs += args.in_sizes[r]*_size_per;
	    }


	    if (args.ids_per_rank[r].size() > 0)
	    {
		T* lbuff=&sendbuff[send_curs];

		delivery_id_coll::iterator it;
		int f=0;
		for (it = args.ids_per_rank[r].begin();
		     it != args.ids_per_rank[r].end(); it++)
		{
		    for (int i=0; i<_size_per; i++)
		    {
			// DEBUG("writing to lbuff @ " << f*_size_per+i);
			// DEBUG("from " << args.ids_per_rank[r][f]*_size_per + i);
			// DEBUG("of " << _data.size());
			lbuff[f*_size_per + i] =
			    _data[*it * _size_per + i];
		    }
		    f++;
		}

		MPI_Isend(lbuff,
			  args.ids_per_rank[r].size() * _size_per,
			  get_mpi_type(T()),
			  r,
			  0,
			  args.pctx.comm(),
			  &req[reqcount++]);

		send_curs += args.ids_per_rank[r].size() * _size_per;
	    }
	}


	// data in motion, let's wait for messages to finish
	MPI_Status dummy;
	for (int r=0; r<reqcount; r++)
	{
	    MPI_Wait(&req[r], &dummy);
	}

	
	return newproxy;
    }


    template <typename T>
    status data_proxy_std<T>::load(std::ifstream& in)
    {
	static const int line_length=1024;
	char buffer[line_length];
	int size, size_per;

	{
	    in.getline(buffer, line_length);
	    std::stringstream ss(buffer);

	    ss >> size;
	    ss >> size_per;
	}
	LLOG(1, size << " data * " << size_per);
	

	assert(size_per == _size_per);

	_data.clear();

	int total = size * _size_per;
	_data.resize(total);

	for (int n=0; n< total; n++)
	{
	    in.getline(buffer, line_length);
	    std::stringstream ss(buffer);

	    ss >> _data[n];
	}
	


	return SPLATT_OK;
    }


    template <typename T>
    status data_proxy_std<T>::save(std::ofstream& out, int limit)
    {
	// out << std::scientific << std::setprecision(std::numeric_limits<T>::digits10);
	int bounds;
	
	if (limit >= 0)
	{
	    bounds = limit * _size_per;
	    if (bounds > (int)_data.size())
	    {
		ERROR("request to save more data than we have");
		return SPLATT_FAIL;
	    }
	}
	else
	{
	    limit = size();
	    bounds = _data.size();
	}


	out << limit << " " << _size_per << std::endl;

	for (int n=0; n<bounds; n+=_size_per)
	{
	    for (int w=0; w<_size_per; w++)
	    {
		out << _data[n + w] << std::endl;
	    }
	    
	}


	return SPLATT_OK;
    }

    template <typename T>
    std::string data_proxy_std<T>::dump()
    {
	return "size: " + stringify(size()) + "; cap: " + stringify(_data.capacity());
    }


    // spontaneous data proxy that can hold the same kinds of values, suitable
    // for attaching to "internal" indices
    template <typename T>
    data_proxy* data_proxy_std<T>::type_copy()
    {
        std::vector<T> tmp;
        return new data_proxy_std<T>(tmp, _size_per, SPLATT_STEAL_DATA);
    }

    template <typename T>
    void data_proxy_std<T>::store_raw(int dest, int src, data_proxy* p2)
    {
        data_proxy_std<T>* other = static_cast<data_proxy_std<T>*>(p2);

        for (int i=0; i<_size_per; i++)
        {
            _data[dest*_size_per+i] =
                other->_data[src*_size_per + i];
        }
    }

}


	
