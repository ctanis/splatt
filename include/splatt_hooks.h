//  -*- mode: c++; -*- 

#ifndef _SPLATT_HOOKS_H_
#define _SPLATT_HOOKS_H_

namespace splatter
{
    enum hook_entry { SPLATT_RENUMBER_HOOK, SPLATT_REFINE_HOOK };

    static const std::string refine_id_tag="_SPLATT_newids";
    static const std::string refine_status_tag="_SPLATT_ref_status";
    

    class app_hook
    {
    public:
	virtual status run(part_mgr*, void*hostdata)=0;
	virtual ~app_hook() { }
    };


    class func_hook;
    

    func_hook* hookify(void(*func)(part_mgr*,void*,void*),void* userdata);
    
    

    class func_hook : public app_hook
    {
    public:
	status run(part_mgr* m, void* hostdata)
	{
	    func(m, hostdata, userdata);
	    return SPLATT_OK;
	}

    private:
	friend func_hook* hookify(void(*func)(part_mgr*,void*,void*),void*);
	void (*func)(part_mgr*,void*, void*);
	void* userdata;
    };


    inline func_hook* hookify(void(*func)(part_mgr*,void*,void*),void* data)
    {
	func_hook* rval = new func_hook();
	rval->func = func;
	rval->userdata=data;
	return rval;
    }
    

    //hook user args
    struct refine_arg
    {
	int		oldent;
	int		numnew;
	const int*	newent;
        temp_index*     edgemap;
    };
    



}




#endif /* _SPLATT_HOOKS_H_ */
