#ifndef _SPLATT_COARSEN_H_
#define _SPLATT_COARSEN_H_

namespace splatter
{
    static std::string coarsen_idx="_spl_orig_tets";
    static std::string coarsen_tri_idx="_spl_orig_tris";
    static std::string coarsen_pend_idx="_spl_coarsen_pend";
    static std::string bk_repl_count="_spl_repl_count";
    static std::string repl_idx="_spl_replacements";
    static std::string edgespl_idx="_spl_edgesplits";

    struct coarsen_kernel
    {
    public:
        coarsen_kernel(part_mgr* mgr) : mgr(mgr),
                                        curr_edge_map(temp_index(mgr,topo::EDGE,false))
        {}

        part_mgr*       mgr;
        explicit_index* tet_idx;
        explicit_index* bnd_idx;
        explicit_index* origp;
        explicit_index* orig_pendp;
        explicit_index* replp;
        explicit_index* esplitp;
        explicit_index* orig_trip;
        std::deque<int> otl;       // originals to remove from coarsening candidates
        temp_index curr_edge_map;
        std::vector<int> new_ids;
        std::vector<int> unused_nodes;
        std::vector<array<data_proxy*,3> > tet_data_proxies;
        std::vector<array<data_proxy*,2> > tri_data_proxies;
    };


    // call this after any refinements
    status coarsen_update(coarsen_kernel*);

    // call this to get list of coarsening possibilities
    explicit_index& coarsen_candidates(coarsen_kernel*);
    
    // call this to process coarsening (ids correspond to result of coarsen_candidates())
    status do_coarsen(coarsen_kernel*, std::deque<int>&);

    coarsen_kernel* register_coarsening_handlers(part_mgr* mgr,
                                                 explicit_index* tet_idx,
                                                 explicit_index* bnd_idx);

    status coarsen_track_tet(coarsen_kernel*, std::string);
    status coarsen_track_tri(coarsen_kernel*, std::string);
    

}
#endif /* _SPLATT_COARSEN_H_ */
