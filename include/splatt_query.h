/*  -*- Mode: C++;  -*-  */
#ifndef _SPLATT_QUERY_H_
#define _SPLATT_QUERY_H_

/*

#define SQ_SOURCE(a1) \
    template<typename OUT> \
    void operator()(const OUT& a1) const

#define SQ_TERM(a1, a2, a3, a4, a5) \
    void operator()(const splatter::index* a1, const int& a2, const int& a3, const int& a4, const int* a5) const

#define SQ_FILTER(a1, a2, a3, a4, a5, outparm) \
    template <typename OUT> \
    void operator() (const splatter::index* a1, const int& a2, const int& a3, const int& a4, const int* a5, OUT& outparm) const

#define SQ_ID_GEN(a1, a2, a3, a4, a5, a6, a7) \
     void operator()(const splatter::index* a1, const int& a2, const int& a3, const int& a4, const int* a5, int* a6, int& a7) const


#define SQ_COND(a1, a2, a3, a4, a5)					\
     bool operator()(const splatter::index* a1, const int& a2, const int& a3, const int& a4, const int* a5) const
*/


#define SQ_SOURCE(a1) \
    template<typename OUT> \
    void operator()(const OUT& a1) const

#define SQ_TERM(a1, a2, a3, a4, a5) \
    void operator()(splatter::index* a1, const int& a2, const int& a3, const int& a4, const int* a5) const

#define SQ_FILTER(a1, a2, a3, a4, a5, outparm) \
    template <typename OUT> \
    void operator() (splatter::index* a1, const int& a2, const int& a3, const int& a4, const int* a5, OUT& outparm) const

#define SQ_ID_GEN(a1, a2, a3, a4, a5, a6, a7) \
    void operator()(splatter::index* a1, const int& a2, const int& a3, const int& a4, const int* a5, int* a6, int& a7) const


#define SQ_COND(a1, a2, a3, a4, a5) \
    bool operator()(splatter::index* a1, const int& a2, const int& a3, const int& a4, const int* a5) const





namespace splatter
{
    namespace query
    {

// ---------------------------------------------------------------------------
//	FUNDAMENTALS
	
	// extend query_op for a basic init() method
	class query_op
	{
	public:
	    status init( part_mgr* mgr )
	    {
		this->mgr = mgr;
		this->odbp = &(mgr->get_ownerdb());
		return SPLATT_OK;
	    }

	protected:
	    part_mgr* mgr;
	    const ownerdb*  odbp;	// potentially more efficient to use a manually configured odb&?
	};


	template <typename T1, typename T2>
	class join : public query_op
	{
	public: 
	    join (T1 x, T2 y) : x(x), y(y) {}

	    status init(part_mgr* mgr)
	    {
		if (x.init(mgr) == SPLATT_OK)
		{
		    return y.init(mgr);
		}
		else
		{
		    return SPLATT_FAIL;
		}
	    }

	    // primary query start
	    void operator()() const
	    {
		x(y);
	    }

	    SQ_TERM(idx, eltno, etype, nnodes, nodes)
	    {
		x(idx, eltno, etype, nnodes, nodes, y);
	    }

	    
	private:
	    T1 x;
	    T2 y;
	};


	template
	< typename T1,
	  typename T2 >
	join<T1,T2> operator >>=(T1 x, T2 y)
	{
	    return join<T1,T2>(x,y);
	}


	class end : public query_op
	{
	public:
	    status init(part_mgr* mgr)
	    {
		return SPLATT_OK;
	    }

	    // do nothing
	    SQ_TERM(idx, eltno, etype, nnodes, nodes)
	    {}


	};
	


// ---------------------------------------------------------------------------
//	SOURCES	

	class all_faces : public query_op
	{
	public:
	    all_faces(std::string index) : index(index), idx(NULL), stdidx(true) { }

            all_faces(explicit_index& index) : idx(&index), stdidx(true) { }

	    all_faces(temp_index& index, bool use_eltnos=true) :
		idx(&index), stdidx(false), use_eltnos(use_eltnos) { }


	    status init( part_mgr* mgr)
	    {
		query_op::init(mgr);

		if (idx != NULL) return SPLATT_OK;

		splatter::index* tmp = mgr->get_index(index);

		if (tmp == NULL)
		{
		    return SPLATT_FAIL;
		}
		else if (tmp->type() != SPLATT_EXPLICIT)
		{
		    ERROR("attempt to all_faces a non-explicit index");
		    return SPLATT_FAIL;
		}
		else
		{
		    idx = static_cast<explicit_index*>(tmp);
		    return SPLATT_OK;
		}
	    }


	    // call 'o' on every face in index
	    SQ_SOURCE(o)
	    {
		if (stdidx || !use_eltnos)
		{
		    idx->do_query(o);
		}
		else
		{
		    static_cast<temp_index*>(idx)->do_query(o);
		}
	    }

	protected:
	    std::string index;
	    splatter::explicit_index* idx;
	    bool stdidx;
	    bool use_eltnos;
	};


	template <typename IT> 
	class iterated_query : public query_op
	{
	public:
	    iterated_query(std::string index, IT begin, IT end) :
		indexname(index), idx(NULL), use_iterator(true), begin(begin), end(end) {}

	    iterated_query(explicit_index* index, IT begin, IT end) :
		idx(index), use_iterator(true), begin(begin), end(end) {}

	    iterated_query(std::string index) :
		indexname(index),idx(NULL), use_iterator(false) {}

	    iterated_query(explicit_index* index) :
		idx(index), use_iterator(false) {}

	    
	    status init(part_mgr* mgr)
	    {
		query_op::init(mgr);

		if (! idx)
		{
		    idx = mgr->get_index(indexname);

		    if (idx == NULL)
		    {
			return SPLATT_FAIL;
		    }
		}		

		return SPLATT_OK;
	    }

	    SQ_SOURCE(o)
	    {
		if (use_iterator)
		{
		    idx->do_query_it(o, begin, end);
		}
		else
		{
		    idx->do_query(o);
		}
	    }


	private:
	    std::string indexname;
	    explicit_index* idx;
	    bool use_iterator;
	    IT begin;
	    IT end;
	};

	template <typename IT>
	iterated_query<IT> for_each(std::string name, IT begin, IT end)
	{
	    return iterated_query<IT>(name, begin, end);
	}
	
	template <typename IT>
	iterated_query<IT> for_each(explicit_index* idx, IT begin, IT end)
	{
	    return iterated_query<IT>(idx, begin, end);
	}
	
	template <typename IT>
	iterated_query<IT> for_each(explicit_index& idxr, IT begin, IT end)
	{
	    return iterated_query<IT>(&idxr, begin, end);
	}
	

	inline iterated_query<std::vector<int>::iterator> for_each(std::string name)
	{
	    return iterated_query<std::vector<int>::iterator>(name);
	}
	
	inline iterated_query<std::vector<int>::iterator> for_each(explicit_index* idx)
	{
	    return iterated_query<std::vector<int>::iterator>(idx);
	}
	
	inline iterated_query<std::vector<int>::iterator> for_each(explicit_index& idxr)
	{
	    return iterated_query<std::vector<int>::iterator>(&idxr);
	}
	
	



	class one_face : public all_faces
	{
	public:
	    one_face(std::string index, int faceno) : all_faces(index), faceno(faceno) { }
	    one_face(explicit_index& index, int faceno) : all_faces(index), faceno(faceno) {}
            one_face(temp_index& index, int faceno) : all_faces(index), faceno(faceno) { }


	    // call 'o' on one face as chosen in constructor
	    SQ_SOURCE(o)
	    {
		if (stdidx)
		{
		    idx->do_query(o, faceno);
		}
		else
		{
		    static_cast<temp_index*>(idx)->do_query(o, faceno);
		}
	    }

	private:
	    int faceno;
	};




	
// ---------------------------------------------------------------------------
//	FLOW

	// optionally evaluate T1 without otherwise affecting the query flow
	template <typename COND, typename T1>
	class skim_obj : public query_op
	{
	public:
	    skim_obj(COND cond, T1 x) : cond(cond), x(x) {}

	    status init(part_mgr* mgr)
	    {
		if (cond.init(mgr) == SPLATT_OK)
		{
		    return x.init(mgr);
		}

		return SPLATT_FAIL;
	    }

	    // if cond() is true, call 'x' on face, otherwise call 'o'
	    SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
	    {
		if (cond(idx,eltno,etype,nnodes,nodes))
		{
		    x(idx,eltno,etype,nnodes,nodes);
		}
		// else
		// {
                o(idx,eltno,etype,nnodes,nodes);
		// }
	    }

	private:
	    COND cond;
	    T1 x;
	};
	
	template <typename COND, typename T1>
	skim_obj<COND,T1> skim(const COND& cond, const T1& x)
	{
	    return skim_obj<COND,T1>(cond,x);
	}

        class always_true : public query_op
        {
        public:
            SQ_COND(idx, eltno, etype, nnodes, nodes)
            {
                return true;
            }
        };
        

	template <typename T1>
	skim_obj<always_true,T1> skim(const T1& x)
	{
	    return skim_obj<always_true,T1>(always_true(),x);
	}


	
	template <typename COND>
	class where_obj : public query_op
	{
	public:
	    where_obj(COND cond) : cond(cond) {}

	    status init(part_mgr* mgr)
	    {
		query_op::init(mgr);
		return cond.init(mgr);
	    }

	    // if cond() is true, call 'o' on face
	    SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
	    {
		if (cond(idx,eltno,etype,nnodes,nodes))
		{
		    o(idx,eltno,etype,nnodes,nodes);
		}
	    }
	    
	private:
	    COND cond;
	};

	template <typename COND> where_obj<COND> where(const COND& cond)
	{
	    return where_obj<COND>(cond);
	}


	
	template <typename T1, typename T2>
	class split_obj : public query_op
	{
	public:
	    split_obj(T1 x, T2 y) : x(x), y(y) {}

	    status init(part_mgr* mgr)
	    {
		if (x.init(mgr) == SPLATT_OK)
		{
		    return y.init(mgr);
		}

		return SPLATT_FAIL;
	    }

	    // call 'x' and 'y' on face independently
	    SQ_TERM(idx, eltno, etype, nnodes, nodes)
	    {
		x(idx,eltno,etype,nnodes,nodes);
		y(idx,eltno,etype,nnodes,nodes);
	    }

	private:
	    T1 x;
	    T2 y;
	};
	
	template <typename T1, typename T2>
	split_obj<T1,T2> split(const T1& x, const T2& y)
	{
	    return split_obj<T1,T2>(x,y);
	}


        template <typename T1, typename T2, typename T3>
        split_obj<split_obj<T1,T2>, T3> split(const T1& x, const T2& y, const T3& z)
        {
            return split(split(x,y), z);
        }


// ---------------------------------------------------------------------------
//	DATA MANIPULATION

	class subfaces : public query_op
	{
	public:
	    status init(part_mgr* mgr)
	    {
		facecnt=0;
		query_op::init(mgr);
		entities = mgr->get_etypes();
		return SPLATT_OK;
	    }


	    // call 'o' on topological subfaces of this face
	    SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
	    {
		int nodescratch[SPLATT_MAX_SUB_ENTTITY];

		for (int sf=0; sf<entities[etype].numsubfaces; sf++)
		{
		    int sftype = entities[etype].subfaces[sf].subface_type;
		    const entity_cfg& subface = entities[sftype];

		    for (int n=0; n<subface.nnodes; n++)
		    {
			nodescratch[n] = nodes[entities[etype].subfaces[sf].subface_nodes[n]];
		    }

		    // disconnects from an index*
		    o(NULL, facecnt++, sftype, subface.nnodes, nodescratch);
		}
	    }

	private:
	    const entity_cfg* entities;
	    mutable int facecnt;
	};
	

	class unique : public query_op
	{
	public:
	    unique(bool update_hash=false) : update_hash(update_hash)
	    {}

	    status init(part_mgr* mgr)
	    {
		query_op::init(mgr);

		if (update_hash)
		{
		    return hash.update(mgr->get_ownerdb().low(), mgr->get_ownerdb().high());
		}
		else
		{
		    return SPLATT_OK;
		}

	    }


	    // call 'o' on faces that we have not seen before
	    SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
	    {
		// if hash contains a face with all our nodes, skip it
		if (! hash_contains(hash, nodes, nnodes))
		{
		    hash_insert(hash, eltno, nodes, nnodes);
		    o(idx, eltno, etype, nnodes, nodes);
		}
	    }

	private:
	    bool update_hash;
	    mutable fast_index hash;
	};


	class count: public query_op
	{
	public:
	    count(int& var) : var(var) { var=0; }


	    // increment var, call 'o' on face
	    SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
	    {
		var++;
		o(idx,eltno,etype,nnodes,nodes);
	    }

	private:
	    int& var;
	};


	class dump : public query_op
	{
	public:
	    dump(std::ostream& out=std::cout) : out(&out) {}

	    // send face to ostream out, call 'o' on face
	    SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
	    {
		for (int n=0; n<nnodes; n++)
		{
		    (*out) << nodes[n] << " ";
		}
		(*out) << std::endl;

		o(idx, eltno, etype, nnodes, nodes);
	    }


	private:
	    std::ostream* out;
	};
	

// ---------------------------------------------------------------------------
// FACE FLAGGING

	template < typename CONT, typename VAL >
	class flag_obj : public query_op
	{
	public:
	    flag_obj(CONT& container, VAL value) : container(container), value(value)
	    {}

	    SQ_COND(idx, eltno, etype, nnodes, nodes)
	    {
		return  (container[eltno] == value);
	    }

	    SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
	    {
		container[eltno]=value;
		o(idx, eltno, etype, nnodes, nodes);
	    }


	private:
	    CONT& container;
	    VAL value;
	};
	
	template <typename CONTT, typename VALT>
	flag_obj<CONTT,VALT> flag(CONTT& c, VALT v)
	{
	    return flag_obj<CONTT,VALT>(c,v);
	}




// ---------------------------------------------------------------------------
//	CONDITIONS


	class owns : public query_op
	{
	public:
	    owns(int rank=-1) : rank(rank)
	    {}

	    status init(part_mgr* mgr)
	    {
		odbp = &mgr->get_ownerdb();

		if (rank == -1)
		{
		    rank = odbp->me();
		}

		return SPLATT_OK;
	    }

	    // return true if rank owns this face
	    SQ_COND(idx, eltno, etype, nnodes, nodes)
	    {

		int smallest = nodes[0];
		for (int n=1; n<nnodes; n++)
		{
		    if (nodes[n] < smallest) smallest=nodes[n];
		}

		return odbp->owner(smallest) == rank;
	    }

	private:
	    const ownerdb* odbp;
	    int rank;
	};
	


	class phantom : public query_op
	{
	public:
	    status init(part_mgr* mgr)
	    {
		odbp = &mgr->get_ownerdb();
		me=odbp->me();
		return SPLATT_OK;
	    }

	    
	    // return true if this face has a node that I do not own
	    SQ_COND(idx, eltno, etype, nnodes, nodes)
	    {

		for (int n=0; n<nnodes; n++)
		{
		    // is anyone other than me an owner of a node
		    if (odbp->owner(nodes[n]) != me)
		    {
			return true;
		    }
		}

		return false;
	    }
    

	private:
	    int me;
	    const ownerdb* odbp;
	};
	


	class needs : public query_op
	{
	public:
	    needs(int rank=-1) : rank(rank)
	    {}

	    status init(part_mgr* mgr)
	    {
		odbp = &mgr->get_ownerdb();

		if (rank == -1)
		{
		    rank = odbp->me();
		}

		return SPLATT_OK;
	    }


	    // return true if rank owns one of the nodes in this face
	    SQ_COND(idx, eltno, etype, nnodes, nodes)
	    {

		for (int n=0; n<nnodes; n++)
		{
		    if (odbp->owner(nodes[n]) == rank)
		    {
			return true;
		    }
		}

		return false;
	    }


	private:
	    int rank;
	    const ownerdb* odbp;
	};
	

	template <typename COND>
	class not_obj : public query_op
	{
	public:
	    not_obj(COND cond) : cond(cond) {}

	    status init(part_mgr* mgr)
	    {
		return cond.init(mgr);
	    }

	    // return true if cont() returns false (and vice versa)
	    SQ_COND(idx, eltno, etype, nnodes, nodes)
	    {
		return not cond(idx,eltno,etype,nnodes,nodes);
	    }

	    
	private:
	    COND cond;
	};


	template <typename COND>
	class not_obj<COND> negate(const COND& cond)
	{
	    return not_obj<COND>(cond);
	}


// ---------------------------------------------------------------------------
//	SAVE SUBQUERIES

	template<typename CONT>
	class save_id_obj : public query_op
	{
	public:
	    save_id_obj(CONT& ids) : ids(ids) {}

	    // save eltno to ids and call 'o' on face
	    SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
	    {
		ids.push_back(eltno);
		o(idx, eltno, etype, nnodes, nodes);
	    }

	private:
	    CONT& ids;
	};
	

	template<>
	class save_id_obj<std::set<int> > : public query_op
	{
	public:
	    save_id_obj(std::set<int>& ids) : ids(ids) {}

	    
	    // save eltno to set ids and call 'o' on face
	    SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
	    {
		ids.insert(eltno);
		o(idx, eltno, etype, nnodes, nodes);
	    }

	private:
	    std::set<int>& ids;
	};
	

	template <typename CONT>
	save_id_obj<CONT> save_ids(CONT& idcont)
	{
	    return save_id_obj<CONT>(idcont);
	}


	class save_temp : public query_op
	{
	public:
	    save_temp(temp_index& tidx) : tidx(tidx) {}


	    // save face to tidx and call 'o' on face
	    SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
	    {
		assert(nnodes == tidx.width());
		int id=tidx.add_face(eltno, nodes);
		o(idx, id, etype, nnodes, nodes);
	    }

	private:
	    temp_index& tidx;
	};
	

	class save_temp_uniq : public query_op
	{
	public:
	    save_temp_uniq(temp_index& tidx) : tidx(tidx) {}

	    
	    // save face to tidx, if it's new. call  'o' on face regardless
	    SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
	    {
		assert(nnodes == tidx.width());
		int match = tidx.find(nodes);
		
		if (match ==-1)
		{
		    int id=tidx.add_face(eltno, nodes);
		    o(idx, id, etype, nnodes, nodes); 
		}
		else
		{
		    o(idx, match, etype, nnodes, nodes);
		}

	    }

	private:
	    temp_index& tidx;
	};


	


	// // mark in a properly configured mark_set
	// class mark : public query_op
	// {
	// public:
	//     mark( mark_set& ms) : ms(ms)
	//     {} 

	//     SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
	//     {
	// 	ms->mark(idx,eltno);
	// 	o(idx, id, etype, nnodes, nodes); 
	//     }

	// private:
	//     mark_set& ms;
	// };
	
	
// ---------------------------------------------------------------------------
//	OTHER

// where clause -- exp_index_searcher acts as a filter or a cond
	inline exp_index_searcher faces_in(part_mgr* mgr, std::string index_name)
	{
	    splatter::index* idx = mgr->get_index(index_name);

	    if (idx == NULL)
	    {
		ERROR("cannot find index " << index_name);
		std::vector<int> dummy;
		fast_index dummy2;
		return exp_index_searcher(dummy,dummy2);
	    }
	    if (idx->type() != SPLATT_EXPLICIT)
	    {
		ERROR("attempt to `faces_in` an index of type " << idx->type());
		std::vector<int> dummy;
		fast_index dummy2;
		return exp_index_searcher(dummy,dummy2);
	    }


	    return static_cast<explicit_index*>(idx)->get_query_searcher();
	}

	inline exp_index_searcher faces_in(explicit_index* idx)
	{
	    return idx->get_query_searcher();
	}


	inline exp_index_searcher faces_in(explicit_index& tidx)
	{
	    return tidx.get_query_searcher();
	}




	// terminator
	template <typename CHOOSER, typename DATA, typename DEST>
	class sort_obj : public query_op
	{
	public:
	    sort_obj(CHOOSER c, DATA d, DEST& o) : c(c), d(d), o(o) {}

	    status init(part_mgr* mgr)
	    {
		// we don't init o, since it may not be aware of
		// queries
		if (c.init(mgr) == SPLATT_OK)
		{
		    return d.init(mgr);
		}
		else
		{
		    return SPLATT_FAIL;
		}
	    }

	    // save d(face) in o[id] where ids are generated by c
	    SQ_TERM(idx, eltno, etype, nnodes, nodes) 
	    {
		int count;

		c(idx, eltno, etype, nnodes, nodes, ids, count);
//		DEBUG("got count " <<  count);
		for (int i=0; i<count; i++)
		{
		    o[ids[i]].insert(o[ids[i]].end(), d(idx, eltno, etype, nnodes, nodes) );
		}
	    }
	    

	private:
	    mutable int ids[1000];

	    CHOOSER c;
	    DATA d;
	    DEST& o;
	};

	template <typename C,typename D1,typename D2>
	sort_obj<C,D1,D2> sort(C cond, D1 data, D2& dest)
	{
	    return sort_obj<C,D1,D2>(cond,data,dest);
	}


// misc
        
        
        class castnodes : public query_op
        {
        public:
            castnodes(int type, int nnodes) : _type(type), _nnodes(nnodes)
            {}

            SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
            {
                o(idx, eltno, _type, _nnodes, nodes);
            }
                                              
        private:
            int _type;
            int _nnodes;
        };


        class insert_if_needed : public query_op
        {
        public:
            insert_if_needed(std::string idxname) :
                _name(idxname)
            { }

            status init(part_mgr* mgr)
            {
                _index = mgr->get_index(_name);
                if (_index == NULL)
                {
                    return SPLATT_FAIL;
                }
                else
                {
                    return SPLATT_OK;
                }
            }


            SQ_FILTER(idx, eltno, etype, nnodes, nodes, o)
            {
                int match = _index->find(etype, nodes);

                if (match == -1)
                {
                    int f = _index->add_face(nodes);
                    o(idx,f,etype,nnodes,nodes);
                }
                else
                {
                    o(idx,match,etype,nnodes,nodes);
                }
            }


        private:
            std::string _name;
            splatter::explicit_index* _index;
            
        };
        



    }

// ---------------------------------------------------------------------------
//	USEFUL QUERIES
    int count_duplicates(part_mgr*,std::string);
    int count_duplicates(part_mgr*, temp_index&);
    int count_unneeded(part_mgr*,std::string);
    int count_unneeded(part_mgr*,temp_index&);

}




#endif	// _SPLATT_QUERY_H_



// not currently needed ....

// class map_to_orig : public query_op
// {
// public:
//     map_to_orig(bool update_hash=false) : update_hash(update_hash)
//     {}

//     status init(part_mgr* mgr)
//     {
// 	query_op::init(mgr);

// 	if (update_hash)
// 	{
// 	    return hash.update(mgr->get_ownerdb().low(), mgr->get_ownerdb().high());
// 	}
// 	else
// 	{
// 	    return SPLATT_OK;
// 	}

//     }


//     template <typename OUT>
//     void operator()(const splatter::index* idx,
// 		    const int& eltno, const int& etype,
// 		    const int& nnodes, const int* nodes, OUT& o) const
//     {
// 	// if hash contains a face with all our nodes, resend the original face
// 	int match = hash_find(hash, nodes, nnodes);

		
// 	if (match == -1)
// 	{
// 	    hash_insert(hash, eltno, nodes, nnodes);
// 	    o(idx, eltno, etype, nnodes, nodes);
// 	}
// 	else
// 	{
// 	    o(idx, match, etype, nnodes, nodes);
// 	}



//     }

// private:
//     bool update_hash;
//     mutable fast_index hash;
// };

#include "splatt_query_data.h"
