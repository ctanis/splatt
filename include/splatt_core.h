/*  -*- Mode: C++; -*-  */

#ifndef _SPLATT_CORE_H_
#define _SPLATT_CORE_H_


namespace splatter
{

    /*!  `part_mgr` is the main access point for users.

      It is used to create and manipulate indices, as well as provide
      global mesh operations such as renumber().

      It is responsible for maintaining all local content (currently
      based on MPI's reported rank) and provides user access to
      relevant information via the `ownerdb`.

      It maintains the overall topological mapping between indices, and
      ensures that index modifications are propogated properly -- as long as
      they occur through the proper channels.
    */


    class part_mgr
    {
    public:

// ---------------------------------------------------------------------------
// CONSTRUCTION and INITIALIZATION

	/*! Construct a new part_mgr
          \param num_ent number of entities in cfg
	  \param cfg pointer to an appropriate topology configuration
	  \param doinit true if the part_mgr should immediately initialize usign the default parallel_ctx*/
	part_mgr(int num_ent, const entity_cfg* cfg, bool doinit=true);

	/*! Construct a new part_mgr using `topo::std_entities`.
          Requires an explicit call to `init()` */
	part_mgr() : _node_index(_odb), _run_renumber_hooks(true)  { load_topo_cfg(topo::num_std_entities, topo::std_entities); }
	
	~part_mgr();

	/*! Initialize this `part_mgr` with the specified `parallel_ctx`.
	  Default to an auto-configured parallel_ctx based on <code>MPI_COMM_WORLD</code>
	*/
	status init(parallel_ctx pctx = parallel_ctx()); 


	/*! Make sure initial distribution of all indices is correct.  Call
	 *  only after calling `part_mgr::config_node_index` and adding any
	 *  other desired indices */
	status finalize_load();

	
	
// ---------------------------------------------------------------------------
// INDEX CREATION and ACCESS

	/*! Designate size and other behavior (via flags) of the primary node index.
	  \param size number of local nodes owned by this rank
	  \param node_type index in topo_cfg for nodes
	  \param flags extra parameters for index creation
	  \returns handle to node index
	**/
	implicit_index* config_node_index(int size, int node_type=0);
	
	/*! Add a new homogeneous explicit index
	  \param name tag for this set of faces
	  \param entity_type index in topo_cfg for this face type
	  \param nodes vector of node ids definining the faces for this index
	  \param flags extra parameters for index creation
	  \returns handle to created index
	**/
	explicit_index* add_explicit_index(std::string name, int entity_type,
					   std::vector<int>& nodes, int flags=0);

        
        /*! Add a new homogeneous explicit index without loading nodes
	  \param name tag for this set of faces
	  \param entity_type index in topo_cfg for this face type
	  \param flags extra parameters for index creation
	  \returns handle to created index
	**/
	explicit_index* add_explicit_index(std::string name, int entity_type,
					   int flags=0);

	
        /*! Add a new homogeneous explicit index without loading nodes or specifying an entity type
	  \param name tag for this set of faces
	  \param width number of nodes per entity
	  \param flags extra parameters for index creation
	  \returns handle to created index
	**/
        explicit_index* add_explicit_index_notype(std::string name, int width, int flags=0);
        
        

        /*! Get the index managing node data
	  \returns null if index is not configured
	**/
	implicit_index* get_node_index(); // inline

	/*! Get a named index
	  \param name name (tag) of desired index
	  \returns null if index cannot be found
	**/
	explicit_index* get_index(std::string name);

        std::vector<explicit_index*> indices() { return _indices; }


	/*! Track a `temp_index`.
          A tracked index provides phantom node dependencies and is renumbered
         \param idx pointer to `temp_index` that should be tracked
         */
	status track(temp_index* idx);

	/*! Untrack a `temp_index`
          \param idx pointer to `temp_index` that should be untracked
         */
	status untrack(temp_index* idx);
        

// ---------------------------------------------------------------------------
// PROPERTY ACCESSORS
	/*! Access of this mesh's `parallel_ctx`
            \returns a const reference to this part_mgr's parallel_ctx */
	const parallel_ctx& pctx() const; // inline


	/*! Access the authority of node/entity ownership for this mesh
          \returns a const reference to this `part_mgr`'s ownerdb */
	const ownerdb& get_ownerdb() const; // inline

	/*! Create a new ownerdb out of a user-specified node distribution

	  \param nd the node distribution to use -- the user is responsible
	  for making sure it works iwth the current parallel_ctx

	  \returns the new ownerdb (by value)
	*/
	ownerdb make_ownerdb(std::vector<int> nd); // inline


	/*! Get current array of valid entity types
          \returns a const pointer to the topological configuration */
	const entity_cfg* get_etypes() const; // inline
	

// ---------------------------------------------------------------------------
//	PHANTOM 

	/*! Collect all phantom nodes from indices flagged `SPLATT_PHANTOM_DEPS` and those that are explicitly tracked and those passed in the parameter to this method.
	 \param extras explicit list of temp_index* from which to extract phantom nodes
	*/
	status augment_nodes(std::list<temp_index*> extras=std::list<temp_index*>());

	/*! Update data associated with all phantom nodes
          \returns success or failure
	*/
	status sync_all_node_data();

	/*! Update data named tag associated withphantom nodes
          \returns success or failure
	*/
	status sync_node_data(std::string tag);


	/*! Sync unattached data, assuming normal index/data relationship with nodes
          \param dat node data to sync
          \param size_per data count per node
          \returns success or failure
	*/
	template <typename T>
	status sync_node_data(std::vector<T>& dat, int size_per=1);
	
	/*! Sync unattached data, assuming normal index/data relationship with nodes
          \param dat node data to sync
          \param size_per data count per node
          \returns success or failure
	*/
	template <typename T>
	status sync_raw_node_data(T* dat, int size_per=1);
	


// ---------------------------------------------------------------------------
// GLOBAL OPERATIONS

	/*! Renumber the mesh, and optionally any temp_index's specified
	  \param newgids vector of new global ids for the nodes owned by this rank
	  \param externals list of temp_index* to also renumber
	*/
	status renumber(const std::vector<int>& newgids,
			std::list<temp_index*> externals=std::list<temp_index*>());

	/*! Renumber and redistribute the mesh, and optionally any temp_index's specified
	  \param newgids vector of new global ids for the nodes owned by this rank
	  \param dist new node distribution array
	  \param externals list of temp_index* to also renumber
	*/
	status renumber(const std::vector<int>& newgids,
			const std::vector<int>& dist,
			std::list<temp_index*> externals=std::list<temp_index*>());



	/*! Purge all nodes that do not appear in any index
	  \param externals list of temp_index* to check for used nodes
          \returns success or failure
	*/
	status remove_unused_nodes(std::list<temp_index*> externals=std::list<temp_index*>());
	

        status cuthill_mckee();


// ---------------------------------------------------------------------------
// generally not-needed management functions

        /*! Pre-build search structures for all indices flagged `SPLATT_FAST_INDEX`
          \returns success or failure
        */
	status build_fast_indices();

        /*! <b>Do not call</b> */
	status build_phantom_map(std::list<temp_index*> extras=std::list<temp_index*>());
        
        /*! <b>Do not call</b> */
	status build_node_sync_map();


        /*! Remove all phantom node maps and data
          \returns success or failure
        */
	status clear_phantom_data();

        /*! Request the creation of more nodes
          \param amt number of nodes needed on the local partition
          \param newstart output variable for new starting node id
          \param externals list of `temp_index` to renumber after getting more nodes
        */
	status get_more_nodes(int amt, int* newstart, 
			      std::list<temp_index*> externals=std::list<temp_index*>());
	
	
// ---------------------------------------------------------------------------
// QUERY STUFF	
	/*! Querying the mesh
          \param op the query
          \returns success or failure
         */
	template<typename T> status query( T op); // inline

        /*! do not call (reserved for query usage) */
	status set_query_data(std::string name, std::deque<int>* data);

        /*! do not call (reserved for query usage) */
	std::deque<int>* get_query_data(std::string name);
	
// ---------------------------------------------------------------------------
// APP HOOKS
        /*! Register callback for when certain action occurs
          \param point event to trigger callback (e.g., `SPLATT_RENUMBER_HOOK` or `SPLATT_REFINE_HOOK`)
          \param entity type of entity for this hook to process, if applicable
          \param hook the callback function
          \returns success or failure
        */
        
	status register_hook( hook_entry point, int entity, app_hook* hook);

        /*! Print out various information about the state of the mesh */
	void dump();

        /*! Refine the contents of index with specified positions
          \param indexname name of index to refine
          \param marked collection of entity ids to refine
          \param next secondary index affected by refinement
          \returns success or failure
         */
	status refine_marked(std::string indexname, std::deque<int>& marked,
                             explicit_index* next=NULL);

        /*! Refine the contents of index with specified positions, allowing de-refinement
          \param indexname name of index to refine
          \param marked collection of entity ids to refine
          \param kernel handle on derefinement bookkeeping
          \param next secondary index affected by refinement
          \returns success or failure
         */
        status refine_marked(std::string index_name, std::deque<int>& marked,
                             coarsen_kernel* kernel,
                             explicit_index* next=NULL);
        
        /*! Internal refinement.  <b>Do not call</b> */
        status refine_marked_opt(std::string indexname, std::deque<int>& marked,
                                 temp_index& edgemap, std::vector<int>& new_ids,
                                 std::vector<int>& unused_nodes,
                                 explicit_index* next=NULL);
        

        /*! Supress all hooks */
	void inhibit_hooks() { _run_renumber_hooks=false; }

        /*! Reactivate all hooks */
	void enable_hooks() { _run_renumber_hooks=true; }

        /*! Explicitly trigger all renumber hooks */
	void call_hooks();
	
// ---------------------------------------------------------------------------
// RESTART/RELOAD

        /*! Initiate creation of restart files
          \param path_prefix location of restart files
          \returns success or failure
        */
	status save_start(std::string path_prefix);

        /*! Save node data in current restart files
          \param tag node data tag to save
          \returns success or failure
        */
	status save_node_data(std::string tag);

        /*! Save index data in current restart files
          \param idx index whose data should be saved
          \param tag data tag to save
          \returns success or failure
        */        
	status save_data(std::string idx, std::string tag);

        /*! Finish creation of all restart files
          \returns success or failure
        */        
	status save_end();

        
        /*! Restore `part_mgr` state from restart files
          \
          \returns number of nodes loaded
        */        	
	int load(std::string path_prefix);
	// status load_node_data(std::string tag);
	// status load_data(std::string idx, std::string tag);
	// status load_end();


    private:
	implicit_index			_node_index;
	std::vector<explicit_index*>    _indices;

	std::set<temp_index*>	_tracked_indices;
        
	std::list< app_hook* > _renumber_hooks;
	std::vector<std::list< app_hook* > > _refine_hooks;


	// initialization stuff
	status load_topo_cfg(int num, entity_cfg const* cfg);

	status build_global_node_count(int nlocal);


	bool			_initted; // has it been initialized with a valid parallel_ctx?
	parallel_ctx		_pctx;	  // all mpi/threads settings in effect
	
	// the crux of node ownership 
	ownerdb			_odb;

	// phantom node structure
	sync_args		_sync_args;
	
	// index adjacency config
	// etype_graph		_entity_adj;
	int			_num_entities;
	const entity_cfg*       _entities;

        std::map<std::string, std::deque<int>* > query_data;

	bool _run_renumber_hooks;
	
	std::ifstream load_stream;
	std::ofstream save_stream;
    };

}


#include "inline/core_cpp.h"

#endif // _SPLATT_CORE_H_
