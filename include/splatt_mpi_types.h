#ifndef _SPLATT_MPI_TYPES_H_
#define _SPLATT_MPI_TYPES_H_

#include <complex>

namespace splatter
{
    /** template-based determiner of proper MPI_Datatype for a given value **/
    template <typename T> MPI_Datatype get_mpi_type(T ex) 
    {
	ERROR("using base get_mpi_type(ex)!"); return MPI_BYTE;
    }

    /** template-based determiner of proper MPI_Datatype for a given value **/
    template <typename T> MPI_Datatype get_mpi_type()
    {
	ERROR("using base get_mpi_type()!"); return MPI_BYTE;
    }


    template<> inline  MPI_Datatype get_mpi_type<double>(double ex) { return MPI_DOUBLE; }
    template<> inline  MPI_Datatype get_mpi_type<double>() { return MPI_DOUBLE; }

    template<> inline  MPI_Datatype get_mpi_type<int>(int ex) { return MPI_INT; }
    template<> inline  MPI_Datatype get_mpi_type<int>() { return MPI_INT; }

    template<> inline  MPI_Datatype get_mpi_type<std::complex<double> >(std::complex<double> ex) { return MPI_COMPLEX; }
    template<> inline  MPI_Datatype get_mpi_type<std::complex<double> >() { return MPI_COMPLEX; }


    /*! create an empty MPI_Datatype that will be freed up later.
     *  make sure you commit the return value of this!! */
    MPI_Datatype& new_mpi_type();

    /*! clear all custom mpi types created using new_mpi_type */
    void clear_mpi_types();


    template<typename T, int SIZE>
	MPI_Datatype get_mpi_type(splatter::array<T,SIZE> ex)
    {
	static MPI_Datatype* mpit=NULL;
	
	if (mpit == NULL)
	{
	    LLOG(2, "creating new mpi_type for this array");

	    MPI_Datatype& newtype = new_mpi_type();

	    MPI_Type_contiguous(SIZE, get_mpi_type<T>(), &newtype);
	    MPI_Type_commit(&newtype);
	    mpit = &newtype;
	}

	return *mpit;
    }

    template<typename T>
	MPI_Datatype get_mpi_type(T example, int count)
    {
	static std::map<int,MPI_Datatype*> types;

	std::map<int,MPI_Datatype*>::iterator match = types.find(count);

	if (match == types.end())
	{
	    LLOG(2, "creating new mpi_type for count");
	    MPI_Datatype& newtype = new_mpi_type();
	    MPI_Type_contiguous(count, get_mpi_type(example), &newtype);
	    MPI_Type_commit(&newtype);
	    types[count]=&newtype;
	    return newtype;
	}
	else
	{
	    return *(match->second);
	}
	
    }
    
}

#endif	// _SPLATT_MPI_TYPES_H_
