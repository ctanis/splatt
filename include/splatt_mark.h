#ifndef _SPLATT_MARK_H_
#define _SPLATT_MARK_H_


namespace splatter
{
    const int SPLATT_MAX_MARK=10;

    // a collection of mesh entities on which to operate
    class mark_set
    {
    public:
	mark_set() : _tagged_sets[SPLATT_MAX_MARK];

	int size()
	{
	    int total=0;

	    for (unsigned int i=0; i<_tagged_sets.size(); i++)
	    {
		total += _tagged_sets[i].size();
	    }

	    return total;
	}

	void mark(int tag, int ent)
	{
	    assert(tag <= SPLATT_MAX_TAG);
	    _tagged_sets[tag][idx].insert(ent);
	}
	

    protected:
	std::vector<std::set<int> > _tagged_sets;
    };
}


#endif /* _SPLATT_MARK_H_ */
