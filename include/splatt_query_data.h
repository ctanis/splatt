/*  -*- Mode: C++;  -*-  */
#ifndef _SPLATT_QUERY_DATA_H_
#define _SPLATT_QUERY_DATA_H_

namespace splatter 
{
    namespace query 
    {
	template <typename CAPTURE>
	class query_data_save : public query_op
	{
	public:
	    query_data_save(CAPTURE cap, std::string varname) : cap(cap), varname(varname)
	    {

	    }

	    status init(part_mgr* mgr)
	    {
		DEBUG("initting data (write) " << varname);
		if (cap.init(mgr))
		{
		    return mgr->set_query_data(varname, &data);
		}
		else
		{
		    return SPLATT_FAIL;
		}
	    }

	    SQ_FILTER(idx,eltno,etype,nnodes,nodes,o)
	    {
		int count;
		cap(idx, eltno, etype, nnodes, nodes, ids, count);

		data.clear();
		for (int i=0; i<count; i++)
		{
		    data.push_back(ids[i]);
		}

		o(idx,eltno,etype,nnodes,nodes);
	    }

	private:
	    mutable int ids[1000];
	    mutable std::deque<int> data;

	    CAPTURE cap;
	    std::string varname;
	};
	
	// ref
	class query_data_ref : public query_op
	{
	public:
	    query_data_ref(std::string name) : varname(name) {}

	    status init(part_mgr* mgr)
	    {
		DEBUG("initting data (read) " << varname);
		datap = mgr->get_query_data(varname);
		if (datap == NULL)
		{
		    ERROR("query data " << varname << " is null!");
		    assert(0);
		    return SPLATT_FAIL;
		}
		else
		{
		    return SPLATT_OK;
		}
	    }


	    SQ_ID_GEN(idx, eltno, etype, nnodes, nodes, ids, count)
	    {
		count = datap->size();

		for (int i=0; i<count; i++)
		{
		    ids[i] = (*datap)[i];
		}
	    }


	private:
	    std::string varname;
	    std::deque<int>* datap;
	};
	

	template <typename CAPTURE>
	query_data_save<CAPTURE> store(CAPTURE cap, std::string name)
	{
	    DEBUG("registering query data " << name);
	    return query_data_save<CAPTURE>(cap,name);
	}

	inline query_data_ref fetch(std::string name) 
	{
	    DEBUG("referring to query data " << name);
	    return query_data_ref(name);
	}



	class all_needers : public query_op
	{
	public:
	    all_needers(bool skip_me=false) : skip_me(skip_me)
	    {
	    }

	    status init(part_mgr* mgr)
	    {
		odbp = &(mgr->get_ownerdb());
		me = odbp->me();
		return SPLATT_OK;
	    }

	    // generate ids of all node owners for this tet...
	    SQ_ID_GEN(idx, eltno, etype, nnodes, nodes, ids, count)
	    {
		intset owners;
		for (int n=0; n < nnodes; n++)
		{
		    int o = odbp->owner(nodes[n]);
		    if (! (skip_me && o == me))
		    {
			owners.insert(o);
		    }
		    
		}

		count = owners.size();
		for (int n=0; n<count; n++)
		{
		    ids[n]=owners[n];
		}
	    }

	private:
	    const ownerdb* odbp;
	    bool skip_me;	// don't include the local rank
	    int me;
	};
	

	class owner : public query_op
	{
	public:
	    owner(bool skip_me=true) : skip_me(skip_me)
	    {
	    }

	    status init(part_mgr* mgr)
	    {
		odbp = &(mgr->get_ownerdb());
		me = odbp->me();
		return SPLATT_OK;
	    }

	    // generate ids of all node owners for this tet...
	    SQ_ID_GEN(idx, eltno, etype, nnodes, nodes, ids, count)
	    {
                int smallest=nodes[0];
                for (int n=1; n<nnodes; n++)
                {
                    if (nodes[n] < smallest)
                        smallest = nodes[n];
                }

                int face_owner = odbp->owner(smallest);

                if (face_owner == me && skip_me)
                {
                    count=0;
                }
                else
                {
                    count = 1;
                    ids[0]=face_owner;
                }
	    }

	private:
	    const ownerdb* odbp;
	    bool skip_me;	// don't include the local rank
	    int me;
	};
	



	// data extractor
	class face_id : public query_op
	{
	public:
	    int operator()(const splatter::index* idx,
			   const int& eltno, const int& etype,
			   const int& nnodes, const int* nodes) const
	    {
		return eltno;
	    }
	};


    }
}


#endif	// _SPLATT_QUERY_DATA_H_
