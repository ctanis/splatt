#!/usr/bin/perl -w
use strict;
use splatt::restart;
use Data::Dumper;

my ($prefix, $use_coords)=@ARGV;

die "usage error" unless $prefix;

my $restarts = new splatt::restart($prefix);
#print Dumper($restarts);

unless ($restarts and $restarts->validate) {
  die "bad data files;"
}

my $total_nodes = $restarts->total_nodes();
my $varcount = 7;
my @varnames = qw(qnode[0] qnode[1] qnode[2] qnode[3] qnode[4] rank isnew);

if ($use_coords) {

  $varcount = 1;
  @varnames = qw(rank);
   
}



print "total nodes: $total_nodes\n";


my $outfile = "merged-$prefix.fvuns";

#open OUT, ">merged-grid.fvuns" or die "cannot open file for writing";
open OUT, ">$outfile" or die "cannot open file for writing";

local $"="\n";
print OUT  <<EOS
FieldView 2 7
Constants
0. 0. 0. 0.
Grids 1
Boundary Table 0
Variable Names
$varcount
@varnames
Boundary Variable Names
0
Nodes
$total_nodes
EOS
  ;

if ($use_coords) {

  my $coords = $restarts->handle('_node', 'coords');
  for (1...$total_nodes) {
    print OUT $coords->val, "\n";
    $coords->next;
  }

} else {
  # sagar stylee
  my $x = $restarts->handle('_node', 'x');
  my $y = $restarts->handle('_node', 'y');
  my $z = $restarts->handle('_node', 'z');

  # # print coords

  for (1...$total_nodes) {

    print OUT $x->val, $y->val, $z->val, "\n";
    $x->next;
    $y->next;
    $z->next;
  }
}

print OUT <<EOS2
Boundary Faces
0
Elements
EOS2
  ;

my $total_tets =$restarts->total_count('tets');
print "total tets: $total_tets\n";
#print OUT "$total_tets\n";

my $tet = $restarts->handle('tets', '_nodes');

for (1...$total_tets) {
  #   print OUT $tet->val, "\n";
  my $str = $tet->val;
  $str =~ s/^\s+//;
  my @ids = split /\s+/, $str;

  map { $_++ } @ids;
  local $"="   ";
  print OUT "1 1 @ids\n";
  $tet->next;
}

# close OUT;


# open OUT, ">merged-Q.fvuns" or die "cannot open file for writing";


print OUT <<EOS3
Variables
EOS3
  ;

if ($use_coords) {

  my $q = $restarts->handle('_node', 'coords');
  for (1...$total_nodes) {
    print  OUT $q->rank, "\n";
    $q->next;
  }


} else {

  for my $v (0...$varcount-3) {
    # #print variable values

    my $q = $restarts->handle('_node', 'qnode');

    for (1...$total_nodes) {

      my $str = $q->val;
      $str =~ s/^\s+//;
      my @vals = split /\s+/, $str;

      #    local $"="***";
      #    print OUT "@vals\n";
      print OUT $vals[$v], "\n";
      $q->next;
    }
  }

  my $q = $restarts->handle('_node', 'x');
  for (1...$total_nodes) {
    print  OUT $q->rank, "\n";
    $q->next;
  }

  $q = $restarts->handle('_node', 'did_refine');
  for (1...$total_nodes) {
    print  OUT $q->val, "\n";
    $q->next;
  }
}

close OUT;
