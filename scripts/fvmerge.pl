#! /usr/bin/perl -w

use strict;

my $file_prefix = $ARGV[0];
my @files=glob("$file_prefix.*");

my %dataset;

# local $"="****";
# print "@files\n";




for my $file (@files) {
  my ($id) = $file =~ /\.(\d+)$/;
  print "id: $id\n";

  open F, $file or die "cannot open $file";

  for (1...6) {
    <F>;
  }				# slurp 6 lines

  my $nodecount = <F>;
  chomp $nodecount;
  print "nodes: $nodecount\n";
  
  my $nodecoords = [];
  my $elements = [];

  $dataset{$id} = { nodes => $nodecoords, faces => $elements };

  my $i;
  for ($i=0; $i<$nodecount; $i++) {
    my $line = <F>;
    #print "pushing $line\n";
    push @$nodecoords, $line;
  }

  for (1...3) {
    <F>;
  }				# slurp 3 lines

  while (my $line = <F>) {
    #print "pushing $line\n";
    push @$elements, $line;
  }
  print "faces: ", scalar @$elements, "\n";

  # print "nf: ", (scalar @{$dataset{$id}->{'faces'}}), "\n";
  # print "num faces: ", (scalar @{$dataset{$id}->{'faces'}}), "\n";

  close F;
}


my @masternodes = ();

for my $id (sort { $a <=> $b } keys %dataset) {
  push @masternodes, @{$dataset{$id}->{'nodes'}};
}

for my $id (keys %dataset) {
  open F, ">merged.$file_prefix.$id"
    or die "cannot open output file";


  print F << "EOF"
FIELDVIEW_Grids 3 0
GRIDS
1
Boundary Table
0
Nodes
EOF
    ;

  print F scalar @masternodes, "\n";

  print F @masternodes;

  print F << "EOF"
Boundary Faces
0
Elements
EOF
    ;

  # print "nf: ", (scalar @{$dataset{$id}->{'faces'}}), "\n";


  print F @{$dataset{$id}->{'faces'}};

  close F;
}
