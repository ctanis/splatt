#!/usr/bin/perl -w
use strict;

my @files = sort {
  my ($n1)=$a=~/\.(\d+)/g;  my ($n2)=$b=~/\.(\d+)/g;
  return $n1 <=> $n2
} glob("stdout.*");

my %keys;
my @order;
my $first=1;


for my $f (@files) {
  open F, $f or die;
  my $i=0;

  while (<F>) {
    chomp;
    my ($str, $n, $val);
    
    if (($n, $str,$val) = /__tbl\.(.*)\|(.*)\|(.*)/) {

      if ($n == "-1") {
        $n= "";
      } else {
        $str = "$n,$str";        
      }


      if ($first) {
        push @order, "$str";
      }
      else {
        die "$str =/= $order[$i]" unless $str eq $order[$i];
        $i++;
      }

      no warnings;
      push @{$keys{$str}}, $val;

    } 
  }   

  $first = 0;
}


# destructive table dump
local $"="|";
print "|@order|\n";
while (@{$keys{$order[0]}}) {

  print "|";
  for my $o (@order) {
#    print "**$o**\n";
    my $s = shift @{$keys{$o}};
    print $s,"|";
  }
  print "\n";

}
