package splatt::single;

## perl module for a single splatt restart file
use Carp qw(croak);
use FileHandle;
use strict;
#use Data::Dumper;

sub new($)   {
  my ($class, $filename) = @_;
  my $fh = new FileHandle($filename)
    or croak "cannot open $filename -- $!\n";

    


  my $self = bless {
		    filename => $filename,
		    fh => $fh
		   };

  $self->parse_init();
  return $self;
}


sub parse_init($)    {

  my ($self) = @_;
  my $fh = $self->{'fh'};

  my $line = <$fh>;

  my ($low, $high) = $line =~ m/(\d+)\s+(\d+)/;
  $self->{'_bounds'} = [ $low, $high ];

#  print "l: $low\nh: $high\n";  

  while ($line = $fh->getline())
    {
      # variables we will extract here...
      my ($tag, $index, $count, $size_per, $pos);

      my $nameln = $line; chomp $nameln;
      my $dummy;
      ($tag, $dummy, $index) = $nameln =~ m/^(\S+)(\s+(\S+))?/;

      my $countln = $fh->getline();
      ($count, $dummy, $size_per) = $countln =~ m/^(\d+)(\s+(\d+))?/;
      my $isdata=0;
      my $isnode=1;

      # if count is 2 numbers, then we're dealing with data
      # if name is 2 words, then we're dealing with explicit index data (vs. nodes)


      if ($size_per) {
	$isdata=1;
      } else {
	$size_per=1;
      }

      if ($index) {
	$isnode = 0;
      }

      if ($isnode) {
	$index = '_node';
      }

      if (! $isdata) {
	$index = $tag;
	$tag = '_nodes';
      }


      $pos = $fh->getpos();

#      print "i: $index\nt: $tag\nc: $count\ns: $size_per\n\n";

      $self->{$index}->{$tag} = [ $count, $size_per, $pos, $fh ];


      for (my $i=0; $i<$count * $size_per; $i++)
	{
	  $fh->getline();
	}


    }


  #print Dumper($self);
  
}

1;
  
