package splatt::handle;
use Carp qw(croak);
use strict;

sub new($) {
  my ($class, $partref) = @_;
  my $self = {
	      pos => 0,
	      currsize => 0,
	      part => -1,
	      parts => $partref,
	      val => undef
	     };

  bless $self;
  $self->next();
  return $self;
}


sub next() {
  my ($self) = @_;
  my $currpart = $self->{'parts'}->[$self->{'part'}];

  if ($self->{'pos'} >= $self->{'currsize'}) {
    # load the next part
    my $nextpart = $self->{'part'} + 1;
    if ($nextpart >= scalar @{$self->{parts}}) {
      return undef;
    }
    $self->{'part'} = $nextpart;
#    print "switching to ", $self->{'part'}, "\n";

    $currpart = $self->{'parts'}->[$self->{'part'}];

    $self->{'pos'} = 0;
    $self->{'fhpos'} = $currpart->[2];
    $self->{'currsize'} = $currpart->[0];
    $self->{'sizeper'} = $currpart->[1];
    
  }

  $currpart->[3]->setpos($self->{'fhpos'});

  my $fh = $currpart->[3];
  $self->{'pos'}++;

  $self->{'val'} = '';
  for (1...$self->{'sizeper'}) {
    my $line = <$fh>;
    chomp $line;
    $self->{'val'} .= " $line";
  }

  $self->{'fhpos'} = $fh->getpos;

}


sub val() {
  my ($self) = @_;
  return $self->{'val'};
}


sub rank() {
  my ($self) = @_;
  return $self->{'part'};
}

1;
