package splatt::restart;
use splatt::single;
use splatt::handle;
use Data::Dumper;

## perl module for a single splatt restart file
use Carp qw(croak);
use strict;

sub new($) {
  my ($class, $prefix) = @_;
  my $self = [ ];
  
  # sort by rank
  @$self = sort
    {
      $a->[0] <=> $b->[0]
    }
    map
    {
      # the loaded restart files
      my ($rank) = m/(\d+$)/;
      [ $rank, new splatt::single($_) ];
    }
    grep
    /\.[0-9]+$/, 		# from the prefix.* files ending with a number
    glob $prefix. ".*";

  if (@$self > 0) {
    return bless $self;
  } else {
    return undef;
  }
}


sub validate() {
  my ($self) = @_;

  my $last = 0;
  foreach my $single (@$self) {
    my $rank = $single->[0];
    my $obj = $single->[1];

    if ($obj->{'_bounds'}->[0] != $last)
      {
	croak "node mismatch on rank $rank!";
	return undef;
      }

    $last = $obj->{'_bounds'}->[1];
  }

  return 1;
}


sub total_nodes() {
  my ($self) = @_;
  return $self->[$#$self]->[1]->{'_bounds'}->[1];
}

sub total_count($) {
  my ($self, $idx) = @_;
  my $total = 0;
  # print Dumper($self);

  foreach my $part (@$self) {
    $total += $part->[1]->{$idx}->{'_nodes'}->[0];
  }

  return $total;
}

  

sub handle($$) {
  my ($self, $idx, $tag) = @_;
  my $parts = [ ];

  @$parts = map
    {
      $_->[1]->{$idx}->{$tag}
    } @$self;
  
  return new splatt::handle($parts);
}




1;

  
