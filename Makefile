all:
	cd src && make lib

clean:
	cd src && make clean

cleanall: clean
	rm libsplatter.a
