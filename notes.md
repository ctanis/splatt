what kind of changes can we expect?

add splatt		-> new empty part
merge splatt    -> one less part

move splatt		-> redistribute / renumber
split splatt	-> add and redistribute


affect id_map (global ids?)
affect node_dist (new dimension)


new_splatt() -> new index



changing number of ranks requires a new splatt_mgr + parallel_ctx
## future work, if necessary


splatter -> splatt -> index -> data

best way to propogate changes through a hierarchy of objects?

does an index have to have a presence on every splatt?



## decomp flow
* a mesh is loaded into a part_mgr
* reorder parts so that contiguous nodes are on each processor (parmetis rqmt).
* some csr index must exist containing all dependencies relevant to the decomp
* parmetis generates new part number for each node
* note new part sizes
* renumber all nodes so that contiguous ranges fall in each future part

* options:
		* add parts and redistribute on the fly
		* write new files and reload on the right number of processes


## relationships between indices

* what assumptions am i making about nodes.. why am i treating them specially?
they *really* just identify elements.  they *tend* to be important -- "solution sites" as Daniel says.  This is node-centered thinking.

Clearly __node ids__ are needed, and special, because they identify everything in the mesh -- elements and their connectivity.  Cleary an implicit index is something special.  The phantom stuff is not special -- it's just 1-wide remote elements.

These questions must be answered:

1. how do we represent adjacency between indices

2. how do we distinguish between local and remote things (element owned by node owner-- this requires a notion of node ownership, and the contiguous local nodes arranged by rank method is really the only workable solution)

3. we ultimately need to support efficient iterators and queries in the things we use as indices.  

**every index has some local and some remote piece**


outstanding issues:
1. index dependencies / relationships
@done 2. extracting data from a data_wrapper


## 
