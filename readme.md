# Splatt

* a data framework -- not an application framework

* a *hive* of distributed arrays with a variety of index types: __range__, __tuple__, __crs__.

* indices may be modified with renumbering schemes to improve
  rank-local spatial locality or multithreaded traversal performance

* element ownership is defined by a rank-contiguous node distribution

* ghost nodes are simply out-of-range index elements

## compatible with

* fmdb
* zoltan
* parmetis
* petsc
